#pragma once

void maybe_reset_tmap(int cam_idx, uchar *tmap) noexcept;
void handle_pending_tmap_edits(int cam_idx, uchar *tmap) noexcept;
void handle_tmap_click(int cam_idx, uchar *tmap, int x, int y, bool was_left) noexcept;
bool undo_tmap_click(int cam_idx, uchar *tmap) noexcept;
