
#include <thread>
#include <iostream>
#include <memory>
#include "common/log.h"
#include "common/check.h"
#include "common/keep_going.h"
#include "common/macros.h"
#include "tune/tune.h"
#include "find/find.h"
#include "gui/user_input.h"
#include "gui/draw.h"
#include "gui/gui.h"
#include "visualize/field.h"

#include "HardwareInterface/Control.h"

static void stop(Gui *gui) noexcept {
    check(!keep_going());    
    stop_tuning();
    stop_drawing();
    stop_gui(gui);
    stop_finding();
    stop_logger();
}

static bool init_helper(int argc, char **argv, Gui **gui_ptr) noexcept {

    // Only return false if something critical fails - the robot
    // can still play without IO, visualization, etc.

    init_logger(LOG_DEBUG);
    
    if (unlikely(!init_finding(argc > 1 ? argv[1][0] : 0))) {
        #ifndef NDEBUG
            log_critical("DEBUG MODE: Ignoring camera failiure, continuing");
        #else
            return false;
        #endif
    }
    
    Gui *gui = *gui_ptr = init_gui(&argc, &argv);
    if (likely(gui)) {
        init_tuning(gui);
        init_visualization(gui);
        if (unlikely(!init_user_input(gui))) {
            log_warning("Couldn't initialize user input.");
        }
    } else {
        log_warning("Couldn't initialize gui.");
    }

    return true;
}

static bool init(int argc, char **argv, Gui **gui_ptr) noexcept {
    *gui_ptr = nullptr;
    if (unlikely(!init_helper(argc, argv, gui_ptr))) {
        set_keep_going_false();
        stop(*gui_ptr);
        return false;
    }
    return true;
}

static void run_threads(void) noexcept {
    std::thread threads[] = {std::thread{find_stuff}, std::thread{control} };
    run_gui();
    for (auto &thread : threads) thread.join();
} // Note that joining with threads ends before stopping begins.



int main(int argc, char **argv) {
    std::cout << "Main!\n";
    Gui *gui;
    if (unlikely(!init(argc, argv, &gui))) return 1;
    run_threads();
    stop(gui);
    return did_check_fail() ? 7 : 0; // 7 is arbitrary.
    return 0;
}
