#include "common/log.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/constants.h"
#include "common/undo_buf.h"
#include "sync/spinlock.h"
#include "gui/user_input.h"
#include "cmvision/threshold.h"
#include "find/cam.h"
#include "tune/tmap.h"

struct alignas(cache_line_size) AlignedAtomicBool { std::atomic<bool> b; };

void maybe_reset_tmap(int cam_idx, uchar *tmap) noexcept {

    // Here we use a cache line to store a single bit.
    static AlignedAtomicBool is_pending_reset[cam_num]; // Initialized to false.

    if (unlikely(is_pending_reset[cam_idx].b.load(std::memory_order_relaxed))) {
        CMVision::reset_tmap(tmap);
        is_pending_reset[cam_idx].b.store(false, std::memory_order_relaxed);
    } else if (unlikely(take_reset_request())) {
        CMVision::reset_tmap(tmap);
        for (int i = 0; i < cam_num; ++i) {
            if (i != cam_idx) {
                is_pending_reset[i].b.store(true, std::memory_order_relaxed);
            }
        }
    }
}

static const int wide_brush_width = 10, narrow_brush_width = 1;

// Number of values in tmap that will be changed per click given some brush width
static constexpr int valnum_from_brush_width(int w) noexcept { return 4*w*w + 4*w + 1; }

struct ClickBackup {
    yuvi loc;     // Click location in yuv space
    int width;    // Brush width
    uchar old_tmap_part[valnum_from_brush_width(wide_brush_width)];
};

static ClickBackup backup_click(const uchar *tmap, yuvi loc, int width) noexcept {
    ClickBackup b;
    const int vn = valnum_from_brush_width(width);
    const int got_vn = CMVision::get_tmap(tmap, loc.y, loc.u, loc.v,
                                          &b.old_tmap_part[0], vn, width);
    check(got_vn <= vn);
    b.loc   = loc;
    b.width = width;
    return b;
}

static void do_click(uchar *tmap, yuvi loc, int color, int brush_width) noexcept {
    const int set_valnum = CMVision::set_tmap(tmap, loc.y, loc.u, loc.v,
                                              color, brush_width);
    check(set_valnum <= valnum_from_brush_width(brush_width));
}

static void do_undo(uchar *tmap, int width,
                    const uchar *old_tmap_part, yuvi loc) noexcept {
    const int vn = valnum_from_brush_width(width);
    const int set_vn = CMVision::set_tmap(tmap, loc.y, loc.u, loc.v,
                                          old_tmap_part, vn, width);
    check(set_vn <= vn);
}

struct Comm { // Struct for communication between camera threads
    Spinlock lock;
    std::atomic<bool> is_pending_click, is_pending_undo;
    uchar old_tmap_part[valnum_from_brush_width(wide_brush_width)];
    int color, width;
    yuvi loc;
};

static Comm s_comm[cam_num];
static UndoBuf<ClickBackup, 16> s_undo_buf;
static Spinlock s_undo_buf_lock;

void handle_pending_tmap_edits(int cam_idx, uchar *tmap) noexcept {

    if (unlikely(s_comm[cam_idx].is_pending_click.load(std::memory_order_acquire))) {

        s_comm[cam_idx].lock.lock();
        do_click(tmap, s_comm[cam_idx].loc, s_comm[cam_idx].color, s_comm[cam_idx].width);
        s_comm[cam_idx].lock.unlock();

        s_comm[cam_idx].is_pending_click.store(false, std::memory_order_relaxed);

    } else if (unlikely(s_comm[cam_idx].is_pending_undo.load(std::memory_order_acquire))) {

        s_comm[cam_idx].lock.lock();
        do_undo(tmap, s_comm[cam_idx].width,
                s_comm[cam_idx].old_tmap_part, s_comm[cam_idx].loc);
        s_comm[cam_idx].lock.unlock();

        s_comm[cam_idx].is_pending_undo.store(false, std::memory_order_relaxed);
    }
}

void handle_tmap_click(int cam_idx, uchar *tmap, int x, int y, bool was_left) noexcept {

    const yuv  p   = get_frame_pixel(cam_idx, x, y);
    const yuvi loc = {CMVision::adjust_y_for_tmap(static_cast<int>(p.y)),
                      static_cast<int>(p.u), static_cast<int>(p.v)};
    const int w =  use_wide_brush() ? wide_brush_width : narrow_brush_width;

    s_undo_buf_lock.lock();
    s_undo_buf.add(backup_click(tmap, loc, w));
    s_undo_buf_lock.unlock();

    enum Colors color;
    if (was_left) {
        log_info("Adding pixel color");
        color = selected_color();
    } else {
        log_info("Erasing pixel color");
        color = color_background;
    }

    // Add pending clicks for all other cameras.
    for (int i = 0; i < cam_num; ++i) {
        if (i == cam_idx) continue;
        s_comm[i].lock.lock();
        s_comm[i].color = color;
        s_comm[i].width = w;
        s_comm[i].loc = loc;
        s_comm[i].lock.unlock();
        s_comm[i].is_pending_click.store(true, std::memory_order_release);
    }

    do_click(tmap, loc, color, w);
}

bool undo_tmap_click(int cam_idx, uchar *tmap) noexcept {

    if (unlikely(s_comm[cam_idx].is_pending_click.load(std::memory_order_relaxed))) {
        s_comm[cam_idx].is_pending_click.store(false, std::memory_order_relaxed);
        return true;
    }

    s_undo_buf_lock.lock();
    const ClickBackup *b = s_undo_buf.undo();
    if (!likely(b)) return false;

    for (int i = 0; i < cam_num; ++i) {
        s_comm[i].lock.lock();
        s_comm[i].width = b->width;
        s_comm[i].loc   = b->loc;
        for (int j = 0; j < valnum_from_brush_width(b->width); ++j) {
            s_comm[i].old_tmap_part[j] = b->old_tmap_part[j];
        }
        s_comm[i].lock.unlock();
        s_comm[i].is_pending_undo.store(true, std::memory_order_release);
    }

    do_undo(tmap, b->width, b->old_tmap_part, b->loc);
    s_undo_buf_lock.unlock();

    return true;
}
