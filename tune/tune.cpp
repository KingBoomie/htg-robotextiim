#include "pixfc/pixfc-sse.h"
#include "common/constants.h"
#include "common/macros.h"
#include "common/log.h"
#include "common/check.h"
#include "gui/user_input.h"
#include "gui/draw.h"
#include "gui/gui.h"
#include "find/ball_reg.h"
#include "stats/sample.h"
#include "tune/tmap.h"
#include "tune/tune.h"

#define order_rlx std::memory_order_relaxed

static void maybe_undo(int cam_idx, uchar *tmap, bool smpl) noexcept {
    if (likely(!take_undo_request())) return;
    if (smpl) {
        undo_sample();
    } else {
        if (undo_tmap_click(cam_idx, tmap)) {
            log_info("Undid tmap click.");
        } else {
            log_warning("Can't undo - no saved tmap clicks left.");
        }
    }
}

struct CamDrawData {
    DrawBuf *frame_db, *cmap_db;
    rgb frame_rgb[frame_size], cmap_rgb[frame_size];
    PixFcSSE *pixfc;
};

static CamDrawData s_cam_data[cam_num];

// TODO: Is there some way to assert that we initialized all the colors?
static rgb rgb_from_color_idx[color_num] = {
    {255, 128, 0},   // orange
    {0,   0,   255}, // blue
    {255, 255, 0},   // yellow
    {0,   128, 0},   // green
    {255, 255, 255}, // white
    {0,   0,   0},   // black
    {100, 100, 100}  // background
};

void tune_stuff(int cam_idx, uchar *tmap) noexcept {

    maybe_reset_tmap(cam_idx, tmap);
    const bool smpl = is_sample_mode();
    maybe_undo(cam_idx, tmap, smpl);
    handle_pending_tmap_edits(cam_idx, tmap);

    DrawBufClick c = get_last_click();
    if (likely(!c.draw_buf) ||
        likely(c.draw_buf != s_cam_data[cam_idx].frame_db &&
               c.draw_buf != s_cam_data[cam_idx].cmap_db)) return; // No new clicks

    log_info("Click");
    if (smpl)      sample_click(cam_idx, c.x, c.y, c.left);
    else      handle_tmap_click(cam_idx, tmap, c.x, c.y, c.left);
}

void draw_cmap(int cam_idx, const uchar *cmap) noexcept {
    DrawBuf *db = s_cam_data[cam_idx].cmap_db;
    if (!likely(db) || !begin_updating_draw_buf(db)) return;
    for (int i = 0; i < frame_size; i++) {
        s_cam_data[cam_idx].cmap_rgb[i] = rgb_from_color_idx[cmap[i]];
    }
    finish_updating_draw_buf(db);
}

void draw_raw_frame(int cam_idx, const uchar *frame_yuyv) noexcept {
    DrawBuf *db = s_cam_data[cam_idx].frame_db;
    if (!likely(db) || !begin_updating_draw_buf(db)) return;
    const auto p = s_cam_data[cam_idx].pixfc;
    // `(!p) == (!frame_db)`, so if we get this far `p != nullptr`.
    check_ret(!p == !db,);
    check_ret(p,);
    p->convert(p, const_cast<uchar*>(frame_yuyv), s_cam_data[cam_idx].frame_rgb);
    finish_updating_draw_buf(db);
}

bool draw_found_balls(int cam_idx, const AtomicBallRegs *b) noexcept {

    DrawBuf *db = s_cam_data[cam_idx].frame_db;
    if (!likely(db) || !is_draw_buf_visible(db)) return false;

    const int ball_num = b->num.load(order_rlx);
    for (int i = 0; i < ball_num; ++i) {
        Shape ball_border = {{}, 1.0, 0.65, 0.0, Shape::Type::circle, false};
        const int average_radius = (b->regs[i].width.load(order_rlx) +
                                    b->regs[i].height.load(order_rlx) + 2) / 4;
        ball_border.circle = {
            static_cast<int>(b->regs[i].cen_x.load(order_rlx) + 0.5),
            static_cast<int>(b->regs[i].cen_y.load(order_rlx) + 0.5),
            average_radius
        };
        if (unlikely(!draw_shape(db, ball_border))) {
            log_warning("Couldn't draw ball on frame.");
            return false;
        }
    }
    return true;
}

bool draw_found_gate(int cam_idx, Shape::Rect r) noexcept {

    DrawBuf *db = s_cam_data[cam_idx].frame_db;
    if (!likely(!db) || !is_draw_buf_visible(db)) return false;

    const Shape gate_bbox = {{r}, 0.0, 1.0, 1.0, Shape::Type::rect, false};
    if (!likely(draw_shape(db, gate_bbox))) {
        log_warning("Couldn't draw gate bbox on frame.");
        return false;
    }
    return true;
}

void stop_tuning(void) noexcept {
    for (int i = 0; i < cam_num; ++i) {
        destroy_pixfc(s_cam_data[i].pixfc);
        s_cam_data[i].pixfc = nullptr;
    }
    flush_samples_to_file();
}

void init_tuning(Gui *gui) noexcept {

    check_ret(is_gui_initialized(gui),);

    for (int i = 0; i < cam_num; ++i) {

        const uint32_t err = create_pixfc(&s_cam_data[i].pixfc,
                                      PixFcYUYV, PixFcRGB24,
                                      frame_width, frame_height,
                                      // 2 == effective number of bytes per yuyv pixel
                                      frame_width * 2,
                                      // 3 == number of bytes per rgb
                                      frame_width * 3,
                                      PixFcFlag_NNbResamplingOnly);
        if (unlikely(err != PixFc_OK)) {
            log_warning("Creating pixfc failed with error " + std::to_string(err) + ". ");
            s_cam_data[i].pixfc = nullptr;
            s_cam_data[i].frame_db = nullptr;
        } else {
            // Casting `uchar*` to `rgb*` is undefined behaviour due to
            // aliasing, but `rgb*` to `uchar*` is safe.
            s_cam_data[i].frame_db = add_buf_to_draw(gui,
                                        reinterpret_cast<uchar*>(s_cam_data[i].frame_rgb),
                                        frame_width, frame_height, "Raw", true);
            if (!likely(s_cam_data[i].frame_db)) log_warning("Can't display raw frames.");
        }

        s_cam_data[i].cmap_db = add_buf_to_draw(gui,
                                     reinterpret_cast<uchar*>(s_cam_data[i].cmap_rgb),
                                     frame_width, frame_height, "CMap", true);
        if (!likely(s_cam_data[i].cmap_db)) {
            log_warning("Can't display thresholded frames.");
        }
    }
}
