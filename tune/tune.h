#pragma once

#include "gui/draw.h"

typedef unsigned char uchar;

struct AtomicBallRegs;

void init_tuning(Gui *gui) noexcept;
void stop_tuning(void) noexcept;

// This function won't do anything without user input.
void tune_stuff(int cam_idx, uchar *tmap) noexcept;

void draw_cmap(int cam_idx, const uchar *cmap) noexcept;
void draw_raw_frame(int cam_idx, const uchar *frame_yuyv) noexcept;

bool draw_found_balls(int cam_idx, const AtomicBallRegs *b) noexcept;
bool draw_found_gate(int cam_idx, Shape::Rect r) noexcept;
