#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=clang++-3.5
CXX=clang++-3.5
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/2133502246/iofile.o \
	${OBJECTDIR}/_ext/15925952/capture.o \
	${OBJECTDIR}/_ext/15925952/region.o \
	${OBJECTDIR}/_ext/15925952/threshold.o \
	${OBJECTDIR}/_ext/15925952/v4l_helper.o \
	${OBJECTDIR}/_ext/508470658/CoilgunAndDribblerInterface.o \
	${OBJECTDIR}/_ext/508470658/HardwareInterface.o \
	${OBJECTDIR}/_ext/508470658/LEDInterface.o \
	${OBJECTDIR}/_ext/508470658/MotorInterface.o \
	${OBJECTDIR}/_ext/2079416696/AsyncSerial.o \
	${OBJECTDIR}/_ext/166231016/Gamepad_linux.o \
	${OBJECTDIR}/_ext/166231016/Gamepad_private.o \
	${OBJECTDIR}/_ext/1270477542/check.o \
	${OBJECTDIR}/_ext/1270477542/keep_going.o \
	${OBJECTDIR}/_ext/1270477542/log.o \
	${OBJECTDIR}/_ext/761014104/ball.o \
	${OBJECTDIR}/_ext/761014104/ball_reg.o \
	${OBJECTDIR}/_ext/761014104/cam.o \
	${OBJECTDIR}/_ext/761014104/find.o \
	${OBJECTDIR}/_ext/761014104/gate.o \
	${OBJECTDIR}/_ext/1360925804/draw.o \
	${OBJECTDIR}/_ext/1360925804/gui.o \
	${OBJECTDIR}/_ext/1360925804/user_input.o \
	${OBJECTDIR}/_ext/1360930417/loc.o \
	${OBJECTDIR}/_ext/1263313414/MotorLocalizer.o \
	${OBJECTDIR}/_ext/2104279152/main4.o \
	${OBJECTDIR}/_ext/2104279152/sample.o \
	${OBJECTDIR}/_ext/2104279152/stats.o \
	${OBJECTDIR}/_ext/2147261307/field.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/tmap.o \
	${OBJECTDIR}/tune.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m64 -std=c++14 -stdlib=libc++ -Wno-c++98-compat `pkg-config --cflags gtk+-3.0` 
CXXFLAGS=-m64 -std=c++14 -stdlib=libc++ -Wno-c++98-compat `pkg-config --cflags gtk+-3.0` 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread ../pixfc/libpixfc-sse.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tune

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tune: ../pixfc/libpixfc-sse.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tune: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tune ${OBJECTFILES} ${LDLIBSOPTIONS} -lc++ `pkg-config --libs gtk+-3.0 ` -lz -lrt

${OBJECTDIR}/_ext/2133502246/iofile.o: ../CMVision/cmv_shared/iofile.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/2133502246
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2133502246/iofile.o ../CMVision/cmv_shared/iofile.cc

${OBJECTDIR}/_ext/15925952/capture.o: ../CMVision/cmvision/capture.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/15925952
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/15925952/capture.o ../CMVision/cmvision/capture.cc

${OBJECTDIR}/_ext/15925952/region.o: ../CMVision/cmvision/region.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/15925952
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/15925952/region.o ../CMVision/cmvision/region.cpp

${OBJECTDIR}/_ext/15925952/threshold.o: ../CMVision/cmvision/threshold.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/15925952
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/15925952/threshold.o ../CMVision/cmvision/threshold.cpp

${OBJECTDIR}/_ext/15925952/v4l_helper.o: ../CMVision/cmvision/v4l_helper.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/15925952
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/15925952/v4l_helper.o ../CMVision/cmvision/v4l_helper.cc

${OBJECTDIR}/_ext/508470658/CoilgunAndDribblerInterface.o: ../HardwareInterface/CoilgunAndDribblerInterface.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/508470658
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/508470658/CoilgunAndDribblerInterface.o ../HardwareInterface/CoilgunAndDribblerInterface.cpp

${OBJECTDIR}/_ext/508470658/HardwareInterface.o: ../HardwareInterface/HardwareInterface.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/508470658
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/508470658/HardwareInterface.o ../HardwareInterface/HardwareInterface.cpp

${OBJECTDIR}/_ext/508470658/LEDInterface.o: ../HardwareInterface/LEDInterface.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/508470658
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/508470658/LEDInterface.o ../HardwareInterface/LEDInterface.cpp

${OBJECTDIR}/_ext/508470658/MotorInterface.o: ../HardwareInterface/MotorInterface.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/508470658
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/508470658/MotorInterface.o ../HardwareInterface/MotorInterface.cpp

${OBJECTDIR}/_ext/2079416696/AsyncSerial.o: ../HardwareInterface/asio-1.11.0/asyncSerial/AsyncSerial.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2079416696
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2079416696/AsyncSerial.o ../HardwareInterface/asio-1.11.0/asyncSerial/AsyncSerial.cpp

${OBJECTDIR}/_ext/166231016/Gamepad_linux.o: ../Lib/gamepad/Gamepad_linux.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/166231016
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/166231016/Gamepad_linux.o ../Lib/gamepad/Gamepad_linux.c

${OBJECTDIR}/_ext/166231016/Gamepad_private.o: ../Lib/gamepad/Gamepad_private.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/166231016
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/166231016/Gamepad_private.o ../Lib/gamepad/Gamepad_private.c

${OBJECTDIR}/_ext/1270477542/check.o: ../common/check.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1270477542
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1270477542/check.o ../common/check.cpp

${OBJECTDIR}/_ext/1270477542/keep_going.o: ../common/keep_going.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1270477542
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1270477542/keep_going.o ../common/keep_going.cpp

${OBJECTDIR}/_ext/1270477542/log.o: ../common/log.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1270477542
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1270477542/log.o ../common/log.cpp

${OBJECTDIR}/_ext/761014104/ball.o: ../find/ball.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/761014104
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/761014104/ball.o ../find/ball.cpp

${OBJECTDIR}/_ext/761014104/ball_reg.o: ../find/ball_reg.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/761014104
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/761014104/ball_reg.o ../find/ball_reg.cpp

${OBJECTDIR}/_ext/761014104/cam.o: ../find/cam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/761014104
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/761014104/cam.o ../find/cam.cpp

${OBJECTDIR}/_ext/761014104/find.o: ../find/find.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/761014104
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/761014104/find.o ../find/find.cpp

${OBJECTDIR}/_ext/761014104/gate.o: ../find/gate.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/761014104
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/761014104/gate.o ../find/gate.cpp

${OBJECTDIR}/_ext/1360925804/draw.o: ../gui/draw.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360925804
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360925804/draw.o ../gui/draw.cpp

${OBJECTDIR}/_ext/1360925804/gui.o: ../gui/gui.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360925804
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360925804/gui.o ../gui/gui.cpp

${OBJECTDIR}/_ext/1360925804/user_input.o: ../gui/user_input.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360925804
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360925804/user_input.o ../gui/user_input.cpp

${OBJECTDIR}/_ext/1360930417/loc.o: ../loc/loc.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360930417
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360930417/loc.o ../loc/loc.cpp

${OBJECTDIR}/_ext/1263313414/MotorLocalizer.o: ../localizer/MotorLocalizer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1263313414
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1263313414/MotorLocalizer.o ../localizer/MotorLocalizer.cpp

${OBJECTDIR}/_ext/2104279152/main4.o: ../stats/main4.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2104279152
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2104279152/main4.o ../stats/main4.cpp

${OBJECTDIR}/_ext/2104279152/sample.o: ../stats/sample.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2104279152
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2104279152/sample.o ../stats/sample.cpp

${OBJECTDIR}/_ext/2104279152/stats.o: ../stats/stats.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2104279152
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2104279152/stats.o ../stats/stats.cpp

${OBJECTDIR}/_ext/2147261307/field.o: ../visualize/field.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2147261307
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2147261307/field.o ../visualize/field.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/tmap.o: tmap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tmap.o tmap.cpp

${OBJECTDIR}/tune.o: tune.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/include/gtk-3.0/gtk/ -I.. -I../CMVision -I../Lib -I../HardwareInterface/asio-1.11.0/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tune.o tune.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tune

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
