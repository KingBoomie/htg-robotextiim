#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=tune
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/tune
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=tune.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/tune.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=tune
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/tune
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=tune.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/tune.tar
# gcc_debug configuration
CND_PLATFORM_gcc_debug=GNU-Linux-x86
CND_ARTIFACT_DIR_gcc_debug=dist/gcc_debug/GNU-Linux-x86
CND_ARTIFACT_NAME_gcc_debug=tune
CND_ARTIFACT_PATH_gcc_debug=dist/gcc_debug/GNU-Linux-x86/tune
CND_PACKAGE_DIR_gcc_debug=dist/gcc_debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_gcc_debug=tune.tar
CND_PACKAGE_PATH_gcc_debug=dist/gcc_debug/GNU-Linux-x86/package/tune.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
