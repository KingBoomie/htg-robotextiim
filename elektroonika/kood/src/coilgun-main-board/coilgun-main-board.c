/*
 * coilgun_main_board.c
 *
 * Created: 17-Oct-15 16:01:02
 * Author: J�rgen Laks
 * 
 * TODO:
 *   [ ]	implement DISCHARGE
 *   [ ]	implement CHARGE
 *   [ ]	implement AUTOCHARGE
 *   [ ]	implement KICK
 *   [ ]	implement FALILSAFE
 *   [ ]	implement BALL DETECTION
 *   [ ]	implement BUTTON DETECTION
 *   [ ]	implement DRIBBLER SPEED
 *   [?]	implement SET ID
 *   [?]	implement GET ID
 *   [?]	implement PING
 *   [?]	implement LEDs
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include "usb_serial.h"

//defines
#define USB_ENABLED /*Uncomment this to use RS485 interface*/
#define BOARD_ID (uint8_t*)0

//pin definitions
#define PIN_LED_1_R 1
#define PIN_LED_1_G 0
#define PIN_LED_1_B 4
#define PIN_LED_2_R 6
#define PIN_LED_2_G 5
#define PIN_LED_2_B 7
#define PIN_MOTOR_1 5
#define PIN_MOTOR_2 6


//function prototypes
int atoi(const char * str);
void usb_write(const char *str);
uint8_t recv_str(char *buf, uint8_t size);
void parse_and_execute_command(char *buf);
void init_ports_and_pins();
void init_timers();
void setLED(_Bool led, _Bool r, _Bool g, _Bool b);
void setLEDs(unsigned char color);

//global variables
char response[16];
uint8_t fail_counter = 0;
uint8_t failsafe = 1;
uint8_t autocharge = 1;

/*********************************
*   __  __           _           *
*  |  \/  |   __ _  (_)  _ __    *
*  | |\/| |  / _` | | | | '_ \   *
*  | |  | | | (_| | | | | | | |  *
*  |_|  |_|  \__,_| |_| |_| |_|  *
*                                *
*********************************/

int main(void){
	CLKPR = 0x80;
	CLKPR = 0x00;
	
	init_ports_and_pins();
	init_timers();
	
	#ifdef USB_ENABLED
		//init USB
		usb_init();
		while (!usb_configured());
		_delay_ms(500);
	#endif
	
	sei();
	uint8_t n;
	char buf[16];
	
	//send welcome message
	sprintf(response, "<connected_id:%d>\n", eeprom_read_byte(BOARD_ID));
	usb_write(response);
	
    while(1){
        if(usb_serial_available()) {
	        n = recv_str(buf, sizeof(buf));
	        if (n == sizeof(buf)) {
		        parse_and_execute_command(buf);
	        }
        }
        if ((fail_counter == 100) && failsafe) {
	        //discharge(); //TODO implement discharge on fail
	        fail_counter = 0;
        }
		
    }
}

/*******************************************************************
*   _____                          _     _                         *
*  |  ___|  _   _   _ __     ___  | |_  (_)   ___    _ __    ___   *
*  | |_    | | | | | '_ \   / __| | __| | |  / _ \  | '_ \  / __|  *
*  |  _|   | |_| | | | | | | (__  | |_  | | | (_) | | | | | \__ \  *
*  |_|      \__,_| |_| |_|  \___|  \__| |_|  \___/  |_| |_| |___/  *
*                                                                  *
*******************************************************************/

//Related to initialization
void init_ports_and_pins(){
	//disable JTAG, needed to use some of the pins on the MCU
	MCUCR|= (1<<JTD); //in order to change this value, it is needed to 
	MCUCR|= (1<<JTD); //overwrite this value twice during 4 clock cycles
	   
	DDRF = 0b11110011; //LEDs
	PORTF = 255; //swich LEDs off by default
	
	DDRB |= (1 << PIN_MOTOR_1) | (1 << PIN_MOTOR_2);
	
}

void init_timers(){
	//init timer 0
	TCCR0A = 0b00000010;
	TCCR0B = 0b00000101; //prescale 1024
	OCR0A = 250;
	TIMSK0 = 0b00000010;
	TCNT0 = 0;
	
	//Setup PWM
	//Configure TIMER1
	TCCR1A|=(1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);        //NON Inverted PWM
	TCCR1B|=(1<<WGM13)|(1<<WGM12)|(1<<CS11)|(1<<CS10); //PRESCALER=64 MODE 14(FAST PWM)

	ICR1=4999;  //fPWM=50Hz (Period = 20ms Standard).

}

//LED control
void setLED(_Bool led, _Bool r, _Bool g, _Bool b){
	if(led){
		if(!r) PORTF |= (1 << PIN_LED_1_R); else PORTF &= ~(1 << PIN_LED_1_R);
		if(!g) PORTF |= (1 << PIN_LED_1_G); else PORTF &= ~(1 << PIN_LED_1_G);
		if(!b) PORTF |= (1 << PIN_LED_1_B); else PORTF &= ~(1 << PIN_LED_1_B);
	}else{
		if(!r) PORTF |= (1 << PIN_LED_2_R); else PORTF &= ~(1 << PIN_LED_2_R);
		if(!g) PORTF |= (1 << PIN_LED_2_G); else PORTF &= ~(1 << PIN_LED_2_G);
		if(!b) PORTF |= (1 << PIN_LED_2_B); else PORTF &= ~(1 << PIN_LED_2_B);
	}
}

void setLEDs(unsigned char color){
	setLED(0, color&1, color&2, color&4);
	setLED(1, color&8, color&16, color&32);
}

//USB related
void usb_write(const char *str) {
	while (*str) {
		usb_serial_putchar(*str);
		str++;
	}
}

uint8_t recv_str(char *buf, uint8_t size) {
	char data;
	uint8_t count = 0;
	
	while (count < size) {
		data = usb_serial_getchar();
		if (data == '\r' || data == '\n') {
			*buf = '\0';
			return size;
		}
		if (data >= ' ' && data <= '~') {
			*buf++ = data;
			count++;
		}
	}
	return count;
}

/************************************************************************************************************************
*    ____                                                       _       ____                         _                  *
*   / ___|   ___    _ __ ___    _ __ ___     __ _   _ __     __| |     |  _ \    __ _   _ __   ___  (_)  _ __     __ _  *
*  | |      / _ \  | '_ ` _ \  | '_ ` _ \   / _` | | '_ \   / _` |     | |_) |  / _` | | '__| / __| | | | '_ \   / _` | *
*  | |___  | (_) | | | | | | | | | | | | | | (_| | | | | | | (_| |     |  __/  | (_| | | |    \__ \ | | | | | | | (_| | *
*   \____|  \___/  |_| |_| |_| |_| |_| |_|  \__,_| |_| |_|  \__,_|     |_|      \__,_| |_|    |___/ |_| |_| |_|  \__, | *
*                                                                                                                |___/  *
************************************************************************************************************************/

void parse_and_execute_command(char *buf) {
	uint8_t id = eeprom_read_byte(BOARD_ID);
	char *command;
	int16_t par1;
	command = buf;
	
	//Vaatan �le globaalsed k�sud (ID pole tarvis)
	if (   (command[0] == 'i') && (command[1] == 'd')   ){
		//peaks s�ttima plaadi ID
		par1 = atoi(command+2); //id
		eeprom_update_byte(BOARD_ID, par1);
		sprintf(response, "<id_changed_from_%d_to_%d>\n", id, par1);
		usb_write(response);
		return;
	} else if (command[0] == '?') {
		//tagastan ID
		sprintf(response, "<connected:%d>\n", id);
		usb_write(response);
		return;
	}
	
	//Parsin v�lja p�ris k�su (eemaldan ID)
	par1 = atoi(command);
	if(par1 != id){
		return;
	}else{
		while (*command != ':') command++;
		command++;
	}
	
	//uurin ja t�idan k�ske
	if (command[0] == 'l'  &&  command[1] == 'e'  &&  command[2] == 'd') {
		//set LED state
		par1 = atoi(command+3);
		setLEDs(par1);
		usb_write(response);
	}else if (command[0] == 'p'  &&  command[1] == 'i'  &&  command[2] == 'n'  &&  command[3] == 'g') {
		sprintf(response, "<%d:pong>\n", id);
		usb_write(response);
	}else if (command[0] == 'm'  &&  command[1] == '1') {
		par1 = atoi(command+2);
		OCR1A = par1;
	}else if (command[0] == 'm'  &&  command[1] == '2') {
		par1 = atoi(command+2);
		OCR1B = par1;
	}else{
		sprintf(response, "%d:unknown:%s\n", id, command);
		usb_write(response);
	}
}

/************************************************************************
*   ___           _                                          _          *
*  |_ _|  _ __   | |_    ___   _ __   _ __   _   _   _ __   | |_   ___  *
*   | |  | '_ \  | __|  / _ \ | '__| | '__| | | | | | '_ \  | __| / __| *
*   | |  | | | | | |_  |  __/ | |    | |    | |_| | | |_) | | |_  \__ \ *
*  |___| |_| |_|  \__|  \___| |_|    |_|     \__,_| | .__/   \__| |___/ *
*                                                   |_|                 *
************************************************************************/

ISR(TIMER0_COMPA_vect) {
	fail_counter++;
}