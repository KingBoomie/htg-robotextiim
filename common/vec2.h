#pragma once

#include <math.h>
#include "common/macros.h"

template <typename T>
struct Vec2 {
	T x, y;

	Vec2()           noexcept : x(0),  y(0)  {}
	Vec2(T nx, T ny) noexcept : x(nx), y(ny) {}
	void set(T nx, T ny) noexcept {
		x = nx;
		y = ny;
	}

    template <typename U>
    operator Vec2<U>() const noexcept {
        return Vec2<U>(static_cast<U>(x), static_cast<U>(y));
    }

	Vec2 operator+(Vec2 v) const noexcept { return Vec2(x + v.x, y + v.y); }
	Vec2 operator-(Vec2 v) const noexcept { return Vec2(x - v.x, y - v.y); }

	Vec2& operator+=(Vec2 v) noexcept {
		x += v.x;
		y += v.y;
		return *this;
	}
	Vec2& operator-=(Vec2 v) noexcept {
		x -= v.x;
		y -= v.y;
		return *this;
	}

	Vec2 operator+(T s) const noexcept { return Vec2(x + s, y + s); }
	Vec2 operator-(T s) const noexcept { return Vec2(x - s, y - s); }
	Vec2 operator*(T s) const noexcept { return Vec2(x * s, y * s); }
	Vec2 operator/(T s) const noexcept { return Vec2(x / s, y / s); }


	Vec2& operator+=(T s) noexcept {
		x += s;
		y += s;
		return *this;
	}
	Vec2& operator-=(T s) noexcept {
		x -= s;
		y -= s;
		return *this;
	}
	Vec2& operator*=(T s) noexcept {
		x *= s;
		y *= s;
		return *this;
	}
	Vec2& operator/=(T s) noexcept {
		x /= s;
		y /= s;
		return *this;
	}

    // `s` and `c` are the sine and cosine of the angle to rotate by.
    void rot(double c, double s) noexcept {
        const double tx = x * c - y * s;
        y = static_cast<T>(x * s + y * c);
        x = static_cast<T>(tx);
    }
    void rot(double angle_in_radians) noexcept {
        rot(cos(angle_in_radians), sin(angle_in_radians));
    }

    T sqr_dist(Vec2 v) const noexcept {
		const T dx = v.x - x, dy = v.y - y;
		return dx * dx + dy * dy;
	}

	double dist(Vec2 v) const noexcept {
		const T dx = v.x - x, dy = v.y - y;
		return sqrt(dx * dx + dy * dy);
	}

    T  sqr_len() const noexcept { return x * x + y * y; }
	double len() const noexcept { return sqrt(x * x + y * y); }

	void truncate(double length) noexcept {
		// const double angle = atan2(y, x);
		// x = length * cos(angle);
		// y = length * sin(angle);

		// Isn't this faster?
        const double s = length / sqrt(x * x + y * y);
        x *= s;
        y *= s;
	}

	Vec2 unit() const noexcept {
        const T s = x * x + y * y;
        if (unlikely(s == static_cast<T>(0))) return *this;
        const double n = sqrt(s);
        return {x / n, y / n};
    }
	void normalize() noexcept { *this = this->unit(); }

	Vec2 ortho_cw()  const noexcept { return Vec2(y, -x); }
	Vec2 ortho_ccw() const noexcept { return Vec2(-y, x); }

	T   dot(Vec2 v) const noexcept { return (v.x * x) + (v.y * y); }
	T cross(Vec2 v) const noexcept { return (v.y * x) - (v.x * y); }
};

#define GEN_OP(o) \
template <typename T> \
Vec2<T> operator o (T x, Vec2<T> v) noexcept { return v o x; }

GEN_OP(*)
GEN_OP(+)
GEN_OP(-)
GEN_OP(/)

#undef GEN_OP // Operator overloading in C++ is a real pain in the neck.

typedef Vec2<double> Vec2d;
typedef Vec2<int> Vec2i;
