#pragma once

#define bitnum(x)   (sizeof(x) * 8) // Assuming CHAR_BIT == 8; doesn't evaluate `x`.
#define msb_mask(x) ((one << (bitnum(x) - 1))) // Doesn't evaluate `x`.
#define msb(x)      ((x) & (msb_mask(x)))      // Evaluates `x` once.

#undef likely
#undef unlikely
#if defined(clang) || (defined(__GNUC__) && (__GNUC__ >= 3))
# define   likely(x) __builtin_expect(!!(x), 1)
# define unlikely(x) __builtin_expect(!!(x), 0)
#else
# define   likely(x) (x)
# define unlikely(x) (x)
#endif
