#pragma once

#include "common/vec2.h"

struct RelPos {  // Position relative to the robot
    double dist; // Distance in meters
    double dir;  // Direction in radians (negative -> left, positive -> right)
};

inline RelPos to_rel(Vec2d v) noexcept {
    RelPos r;
    r.dir = atan2(v.y, v.x);
    r.dist = v.len();
    return r;
}

inline Vec2d to_vec2d(RelPos r) noexcept {
    return Vec2d(cos(r.dir), sin(r.dir)) * r.dist;
}
