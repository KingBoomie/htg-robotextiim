#include <mutex> // lock_guard
#include <time.h>
#include <stdio.h>
#include "common/macros.h"
#include "common/log.h"

#define USE_SPINLOCK 0
#if USE_SPINLOCK && defined(NDEBUG) // Spinlock uses logger in debug mode.
  #include "sync/spinlock.h"
  typedef Spinlock Lock;
#else
  typedef std::mutex Lock;
#endif

#ifdef __linux__

// The following are UBUNTU/LINUX ONLY terminal color codes.
#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#else

#define RESET   ""
#define BLACK   ""				/* Black */
#define RED     ""				/* Red */
#define GREEN   ""				/* Green */
#define YELLOW  ""				/* Yellow */
#define BLUE    ""				/* Blue */
#define MAGENTA ""				/* Magenta */
#define CYAN    ""				/* Cyan */
#define WHITE   ""				/* White */
#define BOLDBLACK   ""			/* Bold Black */
#define BOLDRED     ""			/* Bold Red */
#define BOLDGREEN   ""			/* Bold Green */
#define BOLDYELLOW  ""			/* Bold Yellow */
#define BOLDBLUE    ""			/* Bold Blue */
#define BOLDMAGENTA ""			/* Bold Magenta */
#define BOLDCYAN    ""			/* Bold Cyan */
#define BOLDWHITE   ""			/* Bold White */

#endif // __linux__

const std::string get_cur_date_time_str() noexcept {
	const time_t now = time(0);
#ifdef __linux__
    char a[26]; // `sizeof(a)` must be >= 26 for `ctime_r`.
    ctime_r(&now, a);
    return std::string(a);
#else
	return ctime(&now); // `ctime` is threadsafe on Windows.
#endif
}

const std::string get_log_level() noexcept {
    
}

static void replace_all(std::string &input_string, const std::string &find,
                        const std::string &replace) noexcept {
	size_t index = 0;
	while (true) {
		index = input_string.find(find, index);
		if (index == std::string::npos) break;
		input_string.replace(index++, find.length(), replace);
	}
}

// I'm not sure whether this should be prefixed
// with an `s_`, since it's not really a variable.
static std::hash<std::string> hash_fn;

static Lock s_log_lock;
static LogLevel s_log_level;
static FILE *s_log_file;
static std::string s_last_text;
static size_t s_last_hash = 0;
static int s_relog_count = 0;

static void plain_log(const std::string &text) noexcept {
    if (likely(s_log_file)) fputs(text.c_str(), s_log_file);
    fputs(text.c_str(), stdout);
}

static void log(const std::string &color, const std::string &prefix,
                std::string text, bool raw, LogLevel level) noexcept {
    
    if (level > s_log_level) return;

    const size_t h = hash_fn(text);
    {
      //  std::lock_guard<Lock> lg(s_log_lock);
        if (h == s_last_hash && text == s_last_text) { // TODO: Is this unlikely?
            ++s_relog_count;
            plain_log("~");
            fflush(stdout); // Print immediately instead of waiting for '\n'.
            return;
        } else {
            s_last_hash = h;
            s_last_text = text;
            if (s_relog_count > 0) { // TODO: Is this unlikely?
                if (s_relog_count > 2) {
                    plain_log("\n[[" + std::to_string(s_relog_count) + "]]\n");
                } else {
                    plain_log("\n");
                }
                s_relog_count = 0;
            }
        }
    }

    if (raw) {
        // Replace all occurrences of \r and \n with
        // readable \r or \n values respectively.
        replace_all(text, "\r", "\\r");
        replace_all(text, "\n", "\\n");
    }
    text = prefix + text;

   // s_log_lock.lock();
	// All \r's and \n's should now be replaced - now print it out.
    if (likely(s_log_file)) fputs((text + '\n').c_str(), s_log_file);
	puts((color + text + RESET).c_str());
   // s_log_lock.unlock();
}

void log_debug(const std::string &text, bool raw) noexcept {
	log(WHITE, "", text, raw, LOG_DEBUG);
}

void log_info(const std::string &text, bool raw) noexcept {
	log(GREEN, "LOG: ", text, raw, LOG_INFO);
}

void log_warning(const std::string &text, bool raw) noexcept {
	log(YELLOW, "WARNING: ", text, raw, LOG_WARNING);
}

void log_critical(const std::string &text, bool raw) noexcept {
	log(BOLDRED, "CRITICAL ERROR: ", text, raw, LOG_CRITICAL);
}

bool init_logger(LogLevel level) noexcept {
    const std::string filename = "../logs/" + get_cur_date_time_str() + ".log";
    s_log_file = fopen(filename.c_str(), "w");
    s_log_level = level;
    if (unlikely(!s_log_file)) {
        log_critical("Creating logfile failed."); // Print to terminal.
        return false;
    }
    return true;
}

void stop_logger(void) noexcept {

    // Return if the logger was already stopped or couldn't be initialized.
    if (unlikely(!s_log_file)) return;

    // If a string was repeatedly logged right before the program started exiting,
    // the number of times it was logged hasn't been logged yet. Do that by logging
    // an empty string.
    if (likely(s_log_level != LOG_NONE)) log("", "", "", false, LOG_NONE);

    fclose(s_log_file);
    s_log_file = nullptr;
}
