#pragma once

enum Colors {
    color_orange,
    color_blue,
    color_yellow,
    color_green,
    color_white,
    color_black,
    color_background,
    color_num
};
