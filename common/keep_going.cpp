#include <atomic>
#include "common/log.h"
#include "common/keep_going.h"

static std::atomic<bool> s_keep_going{true};

bool keep_going(void) noexcept { return s_keep_going.load(std::memory_order_relaxed); }

void set_keep_going_false(void) noexcept {
    log_info("Exiting...");
    s_keep_going.store(false, std::memory_order_relaxed);
}
