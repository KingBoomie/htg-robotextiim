#pragma once

template <typename T, size_t N>
class UndoBuf {
private:
    size_t filled, last; // Number of valid elements, index of last one
    T buf[N];
public:
    UndoBuf() noexcept {
        filled = last = 0;
        --last; // `last` >= any other `size_t` variable now.
    }
    const T * undo(void) noexcept {
        if (filled == 0) return nullptr; // Nothing left to undo.
        --filled;
        size_t t = last;
        last =  (last == 0) ? N - 1 : last - 1;
        return &buf[t];
    }
    // TODO: Redo?
    void add(const T &element) noexcept {
        if (filled != N) ++filled;
        last = (last + 1) % N;
        buf[last] = element;
    }
};
