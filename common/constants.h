#pragma once

#define angle_per_circle 6.283185307179586476925 // 2 * pi

#define cache_line_size  64

#define cam_num          2

#define cam_fov_deg      75.0 // Probably diagonal field of view
#define cam_fov          (angle_per_circle * cam_fov_deg / 360.0)
#define cam_fps          60

#define frame_width      640
#define frame_height     480
#define frame_size       (frame_width * frame_height)

#define gate_width       0.7   // In meters
#define gate_height      0.2
#define gate_depth       0.25

#define field_width      2.4  // field = the part of the field within the borders
#define field_len        4.8

#define robot_rad        0.1  // Approximate robot radius; TODO: measure this.

#define ball_diam        0.0427

#define   max_field_ball_num  11
#define max_tracked_ball_num  20
