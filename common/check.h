#pragma once

#ifdef NDEBUG

// `c` always evaluates to true if there aren't any bugs.
# define check(c)        true
# define check_ret(c, v) do{}while(0)

#else

# include "common/macros.h"

// Always returns false.
bool fail_check_with(const char *msg, unsigned line, const char *file) noexcept;

# define fail_check(c) fail_check_with(#c, __LINE__, __FILE__)

# define check(c) likely((c) || fail_check(c)) // Evaluates to `c`.

# define check_ret(c, v) /* Return `v` on failure. */ \
    do { \
        if (!likely(c)) { \
            fail_check(c); \
            return v; \
        } \
    } while(0)

#endif

bool did_check_fail(void) noexcept; // Always returns false in release mode.
