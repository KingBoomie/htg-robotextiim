#include <chrono>
#include "common/log.h"

using Clock = std::chrono::high_resolution_clock;
using TimePoint = std::chrono::time_point<Clock, std::chrono::nanoseconds>;
using namespace std::chrono_literals; // std::chrono::milliseconds(42) == 42ms

inline void log_delta_ms(const std::string &prefix, TimePoint a, TimePoint b) noexcept {
    const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(b - a);
    log_debug(prefix + std::to_string(ms.count()));
}
