#include <atomic>
#include "common/log.h"
#include "common/keep_going.h"
#include "common/check.h"

static std::atomic<bool> s_did_check_fail{false};

bool fail_check_with(const char *msg, unsigned line, const char *file) noexcept {
    log_critical("Check `" + std::string(msg) + "` failed on line " +
                 std::to_string(line) + " in file " + std::string(file) + ".");
    s_did_check_fail.store(true);
    set_keep_going_false();
    return false;
}

bool did_check_fail(void) noexcept { return s_did_check_fail.load(); }
