#pragma once

#include <string>

#define tostr(s)  std::to_string(s)

enum LogLevel { LOG_NONE, LOG_CRITICAL, LOG_WARNING, LOG_INFO, LOG_DEBUG };

bool init_logger(LogLevel level) noexcept;
void stop_logger() noexcept;

void log_critical(const std::string &text, bool raw = false) noexcept;
void log_warning( const std::string &text, bool raw = false) noexcept;
void log_info(    const std::string &text, bool raw = false) noexcept;
void log_debug(   const std::string &text, bool raw = false) noexcept;


const std::string get_cur_date_time_str() noexcept;
const std::string get_log_level() noexcept;