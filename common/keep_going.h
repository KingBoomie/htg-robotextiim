#pragma once

bool keep_going(void) noexcept;
void set_keep_going_false(void) noexcept;
// Note that there is no `set_keep_going_true`.
