#include <string.h> // strchr
#include "common/log.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/color_enum.h"
#include "cmvision/region.h"

namespace CMVision {

// sum of integers over range [x,x+w)
static inline int range_sum(int x, int w) noexcept { return(w*(2*x + w-1) / 2); }

static inline size_t find_run_end(uchar *cmap, uchar c,
                                  size_t run_start, size_t max_end) noexcept {
    size_t i = run_start;
    while (i < max_end && cmap[i] == c) ++i;
    return i;
}

// TODO: Optimize finding the end of a run.
void EncodeRuns(Run *runs, int *num_runs, int max_runs, int color,
                const uchar *cmap, int cmap_width, int cmap_height) noexcept
// Changes the flat array version of the thresholded image into a run
// length encoded version, which speeds up later processing since we
// only have to look at the points where values change.
{
    const size_t sz = static_cast<size_t>(cmap_width * cmap_height);
    --max_runs; // The last run will be used as a terminator in `ConnectComponents`.

    int n = 0;
    size_t j = 0;

    while (true) {

        const uchar *p = static_cast<const uchar*>(memchr(&cmap[j], color, sz - j));
        if (!likely(p)) break;

        check_ret(*p == color,);
        j = static_cast<size_t>(p - cmap);
        check_ret(j < sz,);

        Run r;

        // A division and a modulus should be optimized to one instruction.
        const size_t cmap_width_u = static_cast<size_t>(cmap_width);
        r.y  = static_cast<int>(j / cmap_width_u);
        r.x1 = static_cast<int>(j % cmap_width_u); // Inclusive
        check_ret(r.y  < cmap_height,);
        check_ret(r.x1 < cmap_width,);

        const size_t row_start = j - static_cast<size_t>(r.x1);
        const size_t next_row_start = row_start + cmap_width_u;

        // TODO: Optimize this loop.
        while (likely(cmap[++j] == color && j < next_row_start)) {}

        const int div_y = static_cast<int>(j / cmap_width_u);
        check_ret((j  < next_row_start && div_y == r.y) ||
                  (j == next_row_start && div_y == r.y + 1),);
        check_ret(j <= sz,);

        r.x2 = static_cast<int>(j - row_start); // Exclusive
        check_ret(r.x2 > 0,);
        check_ret(r.x2 <= cmap_width,);

        r.width = r.x2 - r.x1;
        check_ret(r.width > 0,);
        check_ret(r.width <= cmap_width,);

        r.parent = n;
        runs[n] = r;
        if (unlikely(++n >= max_runs)) {
            log_warning("Exceeded maximum number of runs for color " +
                        tostr(color) + " in CMVision::EncodeRuns.");
            break;
        }
    }

    *num_runs = n;
}

#if 1
void ConnectComponents(Run *runs, int num, int height) noexcept
// Connect components using four-connectedness so that the runs each
// identify the global parent of the connected region they are a part
// of.  It does this by scanning adjacent rows and merging where
// similar colors overlap.
{
    if (unlikely(num == 0)) return; // TODO: Should this be `num < 2` instead?

    // Add terminator to speed up / simplify loops with comparisons.
    runs[num].y = height; // Valid y coordinates are in [0; height-1].

    int a, b = 0;
    do {
        a = b;
        // Increment b until the runs runs[a] and runs[b] are on different rows.
        while (runs[++b].y == runs[a].y) {}
        if (unlikely(b == num)) return; // There aren't any runs on adjacent rows.
    } while (runs[b].y != runs[a].y + 1); // while the runs aren't on adjacent rows

    while (true) { // For each pair of adjacent rows
        const int b_start = b, b_start_y = runs[b].y;
        int a_start = a;
        while (true) { // For each b
            while (runs[a].x2 <= runs[b].x1) {
                if (unlikely(++a == b_start)) {
                    while (runs[++b].y == b_start_y) {}
                    goto skip_row; // break twice
                }
            }
            a_start = a;
            do {  // For each a
                if (runs[a].x1 < runs[b].x2) { // TODO: Why can't we break if this is false?
                    const int new_parent = std::min(runs[a].parent, runs[b].parent);
                    runs[a].parent = runs[b].parent = new_parent;
                }
            } while (++a < b_start); // while a is on the current row

            if (unlikely(runs[++b].y != b_start_y)) break; // Finished this row.
            a = a_start; // Reset a for the do-while loop.
        };
skip_row:
        if (unlikely(b == num)) return; // All runs have been processed.

        // Now a == b_start and b is equal to the index of
        // the first run on the next row that has any runs.
        // That row might not be adjacent to the previous one.

        while (runs[b].y != runs[a].y + 1) { // while not adjacent
            a = b;
            const int old_y = runs[b].y;
            while (runs[++b].y == old_y) {}
            if (unlikely(b == num)) return;
            // Now b is the index of the first run on the next lower row.
        }
    }
}
#else
void ConnectComponents(Run *runs, int num, int height) noexcept
// Connect components using four-connectedness so that the runs each
// identify the global parent of the connected region they are a part
// of.  It does this by scanning adjacent rows and merging where
// similar colors overlap.
{
    if (unlikely(num == 0)) return; // TODO: Should this be `num < 2` instead?

    // Add terminator to speed up / simplify loops with comparisons.
    runs[num].y = height; // Valid y coordinates are in [0; height-1].

    int a, b = 0;
    do {
        a = b++;
        // Increment b until the runs runs[a] and runs[b] are on different rows.
        while (runs[b].y == runs[a].y) ++b;
        if (unlikely(b == num)) return; // There aren't any runs on adjacent rows.
    } while (runs[b].y != runs[a].y + 1); // while the runs aren't on adjacent rows

    while (true) {
        const int b_start = b, b_start_y = runs[b].y;
        //while (true) {
            //if (runs[a].x2 <= runs[b].x1 && ++a == b_start) goto finished_row;
            //else if (runs[b].x2 <= runs[a].x1) if (++b == num) return;
        //}
        const int a_start = a;
        // a_start and b_start are the indices of the first
        // runs on adjacent rows with 0 <= a_start < b_start < num.
        while (true) {
            do {
                // case 1:    a.x1 <= b.x1 < a.x2
                // case 2:    b.x1 <= a.x1 < b.x2
                // otherwise: a and b aren't overlapping.
                // TODO: Profile different ways to compute this.
                const int d = runs[b].x1 - runs[a].x1;
                const unsigned da = d, db = -d;
                const bool are_overlapping = da < runs[a].width || db < runs[b].width;

#if 0 // Branchless implementation

                // (`min` will hopefully compile to something branchless.)
                const int new_parent = min(runs[a].parent, runs[b].parent);

#if 1 // Use mask.
                const int mask = -are_overlapping;
                const int not_mask = ~mask, maybe_new = new_parent & mask;
                runs[a].parent = maybe_new | (runs[a].parent & not_mask);
                runs[b].parent = maybe_new | (runs[b].parent & not_mask);
#else // Multiply by `are_overlapping.`
                const int maybe_new = new_parent * are_overlapping,
                          not_overlapping = !are_overlapping;
                runs[a].parent = maybe_new | (runs[a].parent * not_overlapping);
                runs[b].parent = maybe_new | (runs[b].parent * not_overlapping);
#endif

#else
                if (are_overlapping) {
                    const int new_parent = std::min(runs[a].parent, runs[b].parent);
                    runs[a].parent = runs[b].parent = new_parent;
                }
#endif

            } while (++a < b_start); // while a is on the same row as a_start

            if (unlikely(runs[++b].y != b_start_y)) break; // Finished this row.
            a = a_start; // Reset a for the do-while loop.
        };

        if (unlikely(b == num)) return; // All runs have been processed.

        // Now a == b_start and b is equal to the index of
        // the first run on the next row that has any runs.
        // That row might not be adjacent to the previous one.

        while (runs[b].y != runs[a].y + 1) { // while not adjacent
            a = b;
            const int old_y = runs[b].y;
            while (runs[b].y == old_y) ++b;
            if (unlikely(b == num)) return;
            // Now b is the index of the first run on the next lower row.
        }
    }
}
#endif

int ExtractRegions(Region *reg, int max_reg, Run *runs, int num_runs) noexcept
// Takes the list of runs and formats them into a region table,
// gathering the various statistics along the way.  The number of
// unique regions in reg[] (bounded by max_reg) is returned.
{
    int num_regs = 0;

    for (int j = 0; j < num_runs; ++j) {
        const Run r = runs[j];
        if (unlikely(r.parent == j)) {
            // Add new region if this run is a root (i.e. self parented).
            const int b = num_regs;
            runs[j].parent = b;
            reg[b].area = r.width;
            reg[b].x1 = r.x1;
            reg[b].x2 = r.x2;
            reg[b].y1 = reg[b].y2 = r.y;
            reg[b].cen_x = range_sum(r.x1, r.width);
            reg[b].cen_y = r.y * r.width;
            if (unlikely(++num_regs == max_reg)) {
                log_warning("Exceeded maximum number of regions "
                            "in CMVision::ExtractRegions.");
                break;
            }
        } else {
            // Otherwise update region stats incrementally.
            // r.parent < i, so runs[r.parent].parent is a region id that
            // was set in this loop.
            const int b = runs[r.parent].parent;
            runs[j].parent = b;
            reg[b].area += r.width;
            reg[b].x2 = std::max(r.x2, reg[b].x2);
            reg[b].x1 = std::min(r.x1, reg[b].x1);
            reg[b].y2 = r.y; // last set by lowest run
            reg[b].cen_x += range_sum(r.x1, r.width);
            reg[b].cen_y += r.y * r.width;
        }
    }

    // Calculate centroids from stored sums.
    for (int i = 0; i < num_regs; ++i) {
        const double a = 1.0 / static_cast<double>(reg[i].area);
        reg[i].cen_x *= a;
        reg[i].cen_y *= a;
        reg[i].width  = reg[i].x2 - reg[i].x1;
        reg[i].x2--; // Change to inclusive range.
        reg[i].height = reg[i].y2 - reg[i].y1 + 1; // `y2` is already inclusive.
    }
    return num_regs;
}

int FindRun(Run *runs, int first, int last, int x, int y) noexcept
// This function uses binary search to find the leftmost run whose
// interval either contains (x,y), or returns -1 if no such run exists
// (for example when (x,y) is background color)
{
#if 0 // Brute force implementation for comparison

  for (int i = first; i <= last; ++i) {
    if (runs[i].x1 <= x && runs[i].x2 > x && runs[i].y == y) return i;
  }
  return -1;

#else

  int m = 0;

  while (last > first) {
    m = (first + last) / 2;
    if (y > runs[m].y || (y == runs[m].y && x > runs[m].x2)) {
      first = m + 1;
    } else if (y < runs[m].y || (y == runs[m].y && x < runs[m].x1)) {
      last = m;
    } else {
      return m;
    }
  }

  if (y==runs[m].y && x>=runs[m].x1 && x<runs[m].x2) {
    return m;
  } else {
    return -1;
  }

#endif
}

void RunsToRgbImage(rgb *img, int width, Run *runs, int num_runs, rgb color) noexcept
{
  for (int i = 0; i < num_runs; ++i) {
    const Run run = runs[i];
    const int s = run.y * width + run.x1;
    for (int j = 0; j < run.width; ++j) img[s + j] = color;
  }
}

} // namespace
