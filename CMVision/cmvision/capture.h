/*========================================================================
    capture.h : Video4Linux2 raw video capture class for CMVision2
  ------------------------------------------------------------------------
    Copyright (C) 1999-2005  James R. Bruce
    School of Computer Science, Carnegie Mellon University
  ------------------------------------------------------------------------
    This software is distributed under the GNU General Public License,
    version 2.  If you do not have a copy of this licence, visit
    www.gnu.org, or write: Free Software Foundation, 59 Temple Place,
    Suite 330 Boston, MA 02111-1307 USA.  This program is distributed
    in the hope that it will be useful, but WITHOUT ANY WARRANTY,
    including MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  ========================================================================*/

#ifndef INCLUDED_CAPTURE_H
#define INCLUDED_CAPTURE_H

#include <stdlib.h>
#include "cmvision/v4l_helper.h"

#define VIDEO_STANDARD        "NTSC"

// If you get a message like "DQBUF returned error", "DQBUF error: invalid"
// then you need to use a higher value for STREAMBUFS or process frames faster.
#define STREAMBUFS            3

class Capture {
public:
  class Frame {
    friend class Capture;
    unsigned char *data;  // image data
    size_t length;        // data buffer size
    int width, height;    // image dimensions
    int bytesperline;     // image pitch, usually width*sizeof(pixel)
    timeval timestamp;    // timestamp for when the frame was captured
    int index;
    unsigned char field;  // which field of interlaced video this is (0,1)
  public:
    Frame() { data = nullptr; length = 0; }
    const unsigned char * get_raw_data(void) const { return data; }
    int size(void) const { return bytesperline * height; }
    long get_timestamp_usec(void) const {
        return timestamp.tv_sec * 1000000L + timestamp.tv_usec;
    }
    bool copy(const Frame &frame) noexcept;
  };

  enum ImageType { ImageTypeUnknown, ImageTypeRawYUV };

  struct RawImageFileHdr{
    uint16_t type;         // from ImageType enum
    uint8_t  reserved;     // reserved, default to zero
    uint8_t  field;        // which field of video this is from {0,1}
    uint16_t width,height; // image dimensions
    timeval  timestamp;
  };

private:
  VideoDevice vid;           // video device
  Frame frames[STREAMBUFS];  // buffers for images
  int pixelformat;           // format of image data
  struct v4l2_buffer tempbuf;

public:
  ~Capture() { close(); }

  bool init(const char *device, int nwidth, int nheight,
            int nfmt, unsigned fps) noexcept;
  void close(void) noexcept;

  const Frame *capture_frame(void) noexcept;
  bool release_frame(const Frame *_frame) noexcept;

  int get_fd() noexcept {return(vid.fd);}
};

#endif // INCLUDED_CAPTURE_H
