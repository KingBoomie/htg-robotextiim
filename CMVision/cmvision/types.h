#pragma once

/*========================================================================
    types.h : Base types for CMVision2
  ------------------------------------------------------------------------
    Copyright (C) 1999-2005 James R. Bruce
    School of Computer Science, Carnegie Mellon University
  ------------------------------------------------------------------------
    This software is distributed under the GNU General Public License,
    version 2.  If you do not have a copy of this licence, visit
    www.gnu.org, or write: Free Software Foundation, 59 Temple Place,
    Suite 330 Boston, MA 02111-1307 USA.  This program is distributed
    in the hope that it will be useful, but WITHOUT ANY WARRANTY,
    including MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  ========================================================================*/

typedef unsigned char uchar;

struct yuv  { uchar y,u,v; };
struct yuvi { int y,u,v; };
struct yuvf { float y,u,v; };
struct yuyv { uchar y1,u,y2,v; };
struct uyvy { uchar u,y1,v,y2; };

struct rgb {
  uchar r, g, b;
  bool operator ==(const rgb &c) const noexcept {return(r==c.r && g==c.g && b==c.b);}
  bool operator !=(const rgb &c) const noexcept {return(r!=c.r || g!=c.g || b!=c.b);}
};
struct rgbf  { float r,g,b;   };
struct rgba  { uchar r,g,b,a; };
struct rgbaf { float r,g,b,a; };

namespace CMVision {

// Uncomment if your compiler supports the "restrict" keyword.
#define restrict __restrict__

struct run {
  int parent;           // parent run in run list
  int x1, x2, y, width; // location and width of run
};

struct region {
  double cen_x,cen_y; // centroid
  int x1,y1,x2,y2;    // bounding box (x1,y1) - (x2,y2) (inclusive coordinates)
  int width, height;  // width = x2 - x1 + 1, height = y2 - y1 + 1
  int area;           // occupied area in pixels
};

}

typedef CMVision::run    Run;
typedef CMVision::region Region;
