#pragma once

/*========================================================================
    cmv_threshold.h : Color threshold support for CMVision2
  ------------------------------------------------------------------------
    Copyright (C) 1999-2005 James R. Bruce
    School of Computer Science, Carnegie Mellon University
  ------------------------------------------------------------------------
    This software is distributed under the GNU General Public License,
    version 2.  If you do not have a copy of this licence, visit
    www.gnu.org, or write: Free Software Foundation, 59 Temple Place,
    Suite 330 Boston, MA 02111-1307 USA.  This program is distributed
    in the hope that it will be useful, but WITHOUT ANY WARRANTY,
    including MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  ========================================================================*/

#include "common/check.h"
#include "cmvision/types.h"

#define tmap_bits_y   4
#define tmap_bits_u   8
#define tmap_bits_v   8

#define tmap_y_size  (1 << tmap_bits_y)
#define tmap_u_size  (1 << tmap_bits_u)
#define tmap_v_size  (1 << tmap_bits_v)

#define tmap_size    (tmap_y_size * tmap_u_size * tmap_v_size)

namespace CMVision {

inline int adjust_y_for_tmap(int y) noexcept {
    // The y components of a regular yuyv pixel use all 8 bits of a uchar
    // and can have values 0 to 255, but the tmap selects a color class
    // according to only the `bits_y` most significant bits, so we need to
    // bitshift `yi` right by `8 - bits_y` to truncate the rest. This isn't
    // necessary for u and v, because bits_u == bits_v == bitnum(uchar) == 8,
    // so `u >> (8 - bits_u) == u >> 0 == u`.
    return y >> (8 - tmap_bits_y);
}

inline int calc_tmap_index(int yi, int ui, int vi) noexcept {
    return (yi << (tmap_bits_u + tmap_bits_v)) + (ui << tmap_bits_v) + vi;
}

inline uchar get_tmap_fast(const uchar *tmap, int yi, int ui, int vi) noexcept {
    const int a = calc_tmap_index(yi, ui, vi);
    check_ret(a < tmap_size, 0);
    return tmap[a];
}

inline uchar get_tmap(const uchar *tmap, int yi, int ui, int vi) noexcept {
    if (yi>=0 && yi<tmap_y_size && ui>=0 && ui<tmap_u_size && vi>=0 && vi<tmap_v_size) {
        return get_tmap_fast(tmap, yi, ui, vi);
    }
    return 0;
}

inline void set_tmap_fast(uchar *tmap, int yi, int ui, int vi, uchar val) noexcept {
    const int a = calc_tmap_index(yi, ui, vi);
    check_ret(a < tmap_size,);
    tmap[a] = val;
}

inline bool set_tmap(uchar *tmap, int yi, int ui, int vi, uchar color_to_set) noexcept
{
    if(yi>=0 && yi<tmap_y_size &&
       ui>=0 && ui<tmap_u_size &&
       vi>=0 && vi<tmap_v_size) {
        const int l = calc_tmap_index(yi, ui, vi);
        if ((tmap[l] | 128) == (color_to_set | 128)) return(false); // Why ??
        tmap[l] = color_to_set;
        return true;
    }
    return false;
}

int get_tmap(const uchar *tmap, int yi, int ui, int vi,
             uchar *vals, int max_valnum, int uv_radius) noexcept;

int set_tmap(uchar *tmap, int yi, int ui, int vi, int color, int uv_radius) noexcept;

int set_tmap(uchar *tmap, int yi, int ui, int vi,
             const uchar *vals, int valnum, int uv_radius) noexcept;

void reset_tmap(uchar *tmap) noexcept;

void ThresholdImage(uchar *cmap, const uchar *tmap, const uchar *img_yuyv, int img_size) noexcept;

// Count the number of each color index in a box from (x1,y1) to
// (x2,y2) (inclusive).  Counts are added to hist_count, and the total
// area of the box is returned.
int AddToHistogram(const uchar *cmap, int cmap_width, int cmap_height,
                   int x1, int y1, int x2, int y2, int *hist_count) noexcept;

void RgbToIndex(uchar *cmap, int cmap_size, rgb *img, rgb *colors, int color_num) noexcept;

int RemapTMapColor(uchar *tmap, int src_color, int dest_color) noexcept;

int CheckTMapColors(uchar *tmap, int num_colors, int default_color) noexcept;

bool LoadThresholdFile(uchar *tmap, const char *filename) noexcept;

bool SaveThresholdFile(const uchar *tmap, const char *filename) noexcept;

};
