// (C) 2004-2006 James R. Bruce, Carnegie Mellon University
// Licenced under the GNU General Public License (GPL) version 2,
//   or alternately by a specific written agreement.

#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>

#include "common/check.h"
#include "common/macros.h"
#include "cmv_shared/ansicolor.h"
#include "cmvision/v4l_helper.h"

int VideoDevice::xioctl(int request,void *data, const char *error_str) noexcept
{
  // try the ioctl, which should succeed in the common case
  int ret = ioctl(fd,request,data);
  if (likely(ret >= 0)) return(ret);

  // retry if we were interrupted (up to a few times)
  int n=0;
  while(ret!=0 && errno==EINTR && n<8){
    ret = ioctl(fd,request,data);
    n++;
  }

  // report error
  if (ret != 0) {
    if (!error_str) error_str = "";
    AnsiColor::SetFgColor(stderr,AnsiColor::Red);
    AnsiColor::Bold(stderr);
    fprintf(stderr,"VideoDevice: %s returned %d (%s)\n",
            error_str,ret,strerror(errno));
    AnsiColor::Reset(stderr);
  }

  return(ret);
}

bool VideoDevice::open(const char *device) noexcept
{
  // open device
  close();
  fd = ::open(device, O_RDWR|O_NONBLOCK);
  if(fd < 0) return(false);

  pollset.fd = fd;
  pollset.events = POLLIN;

  // test capture capabilities
  v4l2_capability cap;
  if(xioctl(VIDIOC_QUERYCAP, &cap, "VideoQueryCap") == 0){
    if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) ||
       !(cap.capabilities & V4L2_CAP_STREAMING)){
      return(false);
    }
  }else{
    return(false);
  }

  return(true);
}

bool VideoDevice::close() noexcept
{
  if(fd >= 0) ::close(fd);
  fd = -1;
  pollset.fd = -1;
  return(true);
}

bool VideoDevice::set_input(int idx) noexcept
{
  return(xioctl(VIDIOC_S_INPUT, &idx, "SetInput") == 0);
}

bool VideoDevice::set_input(const char *str) noexcept
{
  check(false && "VideoDevice::set_input(const char *str) hasn't been implemented.");
  return(false);
}

void VideoDevice::list_inputs(FILE *out) noexcept
{
  v4l2_input input;
  int current = -1;
  if(xioctl(VIDIOC_G_INPUT, &current) < 0) return;

  int index = 0;
  fprintf(out,"Inputs:\n");
  while(true){
    memset(&input, 0, sizeof (input));
    input.index = index;
    int ret = xioctl(VIDIOC_ENUMINPUT, &input);
    if(ret < 0) break;
    char ch = (index == current)? '#' : ' ';
    fprintf(out,"  %d: (%c) \"%s\"\n",index,ch,input.name);
    index++;
  }
}

bool VideoDevice::set_standard(v4l2_std_id id) noexcept
{
  return(xioctl(VIDIOC_S_STD,&id,"SetStandard") == 0);
}

bool VideoDevice::start_streaming() noexcept
{
  v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return(xioctl(VIDIOC_STREAMON, &type, "StreamOn") == 0);
}

bool VideoDevice::stop_streaming() noexcept
{
  v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return(xioctl(VIDIOC_STREAMOFF, &type, "StreamOff") == 0);
}

/*
  V4L2_CID_BRIGHTNESS
  V4L2_CID_CONTRAST
  V4L2_CID_SATURATION
  V4L2_CID_HUE
*/

bool VideoDevice::get_control(int ctrl_id,float &s) noexcept
{
  v4l2_control ctrl;
  mzero(ctrl);
  ctrl.id = ctrl_id;

  if(xioctl(VIDIOC_G_CTRL, &ctrl, "GetControl") == 0){
    s = static_cast<float>(ctrl.value / MaxCtrlVal);
    return(true);
  }else{
    return(false);
  }
}

bool VideoDevice::set_control(int ctrl_id,float s) noexcept
{
  v4l2_control ctrl;
  mzero(ctrl);
  ctrl.id = ctrl_id;
  ctrl.value = bound(static_cast<int>(rint(s * MaxCtrlVal)), 0, MaxCtrlVal-1);

  if(xioctl(VIDIOC_S_CTRL, &ctrl, "SetControl") == 0){
    return(true);
  }else{
    return(false);
  }
}

bool VideoDevice::get_format(v4l2_format &fmt) noexcept
{
  memset(&fmt,0,sizeof(fmt));
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return(xioctl(VIDIOC_G_FMT, &fmt, "GetFormat") == 0);
}

bool VideoDevice::set_format(v4l2_format &fmt) noexcept
{
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return(xioctl(VIDIOC_S_FMT, &fmt, "SetFormat") == 0);
}

bool VideoDevice::set_fps(unsigned fps) noexcept
{
    v4l2_streamparm parm;
    parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    parm.parm.capture.timeperframe.numerator   = 1;
    parm.parm.capture.timeperframe.denominator = fps;
    parm.parm.capture.capability   = parm.parm.capture.capturemode = 0;
    parm.parm.capture.extendedmode = parm.parm.capture.readbuffers = 0;
    for (int i = 0; i < 4; ++i) parm.parm.capture.reserved[i] = 0;
    return xioctl(VIDIOC_S_PARM, &parm) == 0;
}

bool VideoDevice::init_capture() noexcept
{
  // mmap initialization
  check(false && "VideoDevice::init_capture hasn't been implemented.");
  return(false);
}

bool VideoDevice::stop_capture() noexcept
{
  check(false && "VideoDevice::stop_capture hasn't been implemented.");
  return(false);
}

bool VideoDevice::wait_for_frame(int max_msec) noexcept
{
  int n = poll(&pollset,1,max_msec);
  return(n==1 && (pollset.revents & POLLIN)!=0);
}

bool VideoDevice::enqueue_buffer(v4l2_buffer &buf) noexcept
{
  int ret = xioctl(VIDIOC_QBUF, &buf, "EnqueueBuffer");
  return(ret == 0);
}

bool VideoDevice::dequeue_buffer(v4l2_buffer &buf) noexcept
{
  int ret = xioctl(VIDIOC_DQBUF, &buf, "DequeueBuffer");
  return(ret == 0);
}
