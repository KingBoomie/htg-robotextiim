#pragma once

#include "cmvision/types.h"

namespace CMVision {

void EncodeRuns(Run *runs, int *num_runs, int max_runs, int color,
                const uchar *cmap, int cmap_width, int cmap_height) noexcept;

void ConnectComponents(Run *runs, int num, int height) noexcept;

int ExtractRegions(Region *reg, int max_reg, Run *runs, int num_runs) noexcept;

int FindRun(Run *runs, int left, int right, int x, int y) noexcept;

void RunsToRgbImage(rgb *img, int width, Run *runs, int num_runs, rgb color) noexcept;

}
