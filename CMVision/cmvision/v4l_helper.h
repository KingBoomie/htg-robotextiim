#pragma once

// (C) 2004-2006 James R. Bruce, Carnegie Mellon University
// Licenced under the GNU General Public License (GPL) version 2,
//   or alternately by a specific written agreement.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/poll.h>

#include <asm/types.h>
#include <linux/videodev2.h>

#include "cmv_shared/util.h"

class VideoDevice{
public:
  int fd;
  pollfd pollset;
protected:
  int xioctl(int request, void *data, const char *error_str = nullptr) noexcept;
public:
  VideoDevice() noexcept
    {fd = -1;}
  ~VideoDevice() noexcept
    {close();}

  bool open(const char *device) noexcept;
  bool close() noexcept;
  bool is_open() noexcept
   {return(fd >= 0);}

  // Video input
  bool set_input(int idx) noexcept;
  bool set_input(const char *str) noexcept;
  void list_inputs(FILE *out = stdout) noexcept;

  // Video standard (i.e. NTSC/PAL)
  bool set_standard(v4l2_std_id id) noexcept;

  // Streaming capture control
  bool start_streaming() noexcept;
  bool stop_streaming() noexcept;

  // Controls
  struct CtrlVal{
    const char *name;
    int value;
  };
  static const int MaxCtrlVal = (1 << 16);

  bool get_control(int ctrl_id,float &val) noexcept;
  bool set_control(int ctrl_id,float  val) noexcept;
  // bool set_controls(CtrlVal *arr,int num);
  // bool list_controls();

  // Capture formats
  bool get_format(v4l2_format &fmt) noexcept;
  bool set_format(v4l2_format &fmt) noexcept;

  bool set_fps(unsigned fps) noexcept;

  // Video capture
  bool init_capture() noexcept;
  bool stop_capture() noexcept;
  bool wait_for_frame(int max_msec) noexcept;
  bool is_frame_ready() noexcept
    {return(wait_for_frame(0));}

  bool enqueue_buffer(v4l2_buffer &buf) noexcept;
  bool dequeue_buffer(v4l2_buffer &buf) noexcept;
};
