/*========================================================================
    capture.cc : Video4Linux2 raw video capture class for CMVision2
  ------------------------------------------------------------------------
    Copyright (C) 1999-2005  James R. Bruce, Anna Helena Reali Costa
    School of Computer Science, Carnegie Mellon University
  ------------------------------------------------------------------------
    This software is distributed under the GNU General Public License,
    version 2.  If you do not have a copy of this licence, visit
    www.gnu.org, or write: Free Software Foundation, 59 Temple Place,
    Suite 330 Boston, MA 02111-1307 USA.  This program is distributed
    in the hope that it will be useful, but WITHOUT ANY WARRANTY,
    including MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  ========================================================================*/

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <memory>
#include <new>

#include "common/log.h"
#include "common/macros.h"
#include "cmvision/capture.h"

bool Capture::Frame::copy(const Frame &frame) noexcept
{
  if(length < frame.length) { // Allocate memory only if needed.
    delete[](data);
    length = 0;
    data = new /*(std::nothrow)*/ unsigned char[frame.length];
    if (unlikely(!data)) return(false);
  }

  length = frame.length;
  width  = frame.width;
  height = frame.height;
  bytesperline = frame.bytesperline;
  timestamp = frame.timestamp;
  field = frame.field;
  index = -1;

  memcpy(data, frame.data, length);

  return(true);
}

//==== Capture Class Implementation =======================================//

static_assert(STREAMBUFS <= 256, "");

bool Capture::init(const char *device, int nwidth, int nheight,
                   int nfmt, unsigned fps) noexcept
{
  if (!likely(device && nfmt && nwidth && nheight && fps)) return false;

  if (unlikely(!vid.open(device))) {
    log_critical("Couldn't open video device " + std::string(device));
    return false;
  }

  v4l2_format fmt;
  mzero(fmt);
  fmt.fmt.pix.width       = static_cast<__u32>(nwidth);
  fmt.fmt.pix.height      = static_cast<__u32>(nheight);
  fmt.fmt.pix.pixelformat = static_cast<__u32>(nfmt);
  fmt.fmt.pix.field       = V4L2_FIELD_ALTERNATE;
  if(unlikely(!vid.set_format(fmt))){
    log_critical("Couldn't set video format");
    return(false);
  }
  pixelformat = static_cast<int>(fmt.fmt.pix.pixelformat);
  if (unlikely(!vid.set_fps(fps))) log_warning("Couldn't set fps.");

  int err;

  struct v4l2_requestbuffers req; // Request mmap-able capture buffers
  mzero(req);
  req.count  = STREAMBUFS;
  req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;
  err = ioctl(vid.fd, VIDIOC_REQBUFS, &req);
  if(unlikely(err < 0 || req.count != STREAMBUFS)){
    log_critical("REQBUFS returned error " + tostr(errno) +
                 ", count " + tostr(req.count));
    return(false);
  }

  // set up individual buffers
  mzero(frames, STREAMBUFS);
  mzero(tempbuf);
  tempbuf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  tempbuf.memory = V4L2_MEMORY_MMAP;

  for(unsigned i=0; i<req.count; i++){
    tempbuf.index = i;
    err = ioctl(vid.fd, VIDIOC_QUERYBUF, &tempbuf);
    if(unlikely(err < 0)){
      log_critical("QUERYBUF returned error " + tostr(errno));
      return(false);
    }

    frames[i].length = tempbuf.length;
    frames[i].data = static_cast<unsigned char*>(mmap(NULL, tempbuf.length,
                              PROT_READ | PROT_WRITE, MAP_SHARED,
                              vid.fd, static_cast<__off_t>(tempbuf.m.offset)));
    if (unlikely(frames[i].data == MAP_FAILED)) {
      log_critical("mmap returned error " + tostr(errno) +
                   " (" + strerror(errno) + ").");
      return(false);
    }

    // fill out other fields
    frames[i].width        = nwidth;
    frames[i].height       = nheight;
    frames[i].bytesperline = static_cast<int>(fmt.fmt.pix.bytesperline);
    frames[i].index = static_cast<unsigned char>(i); // Assume STREAMBUFS <= 256.
  }

  for(unsigned i=0; i<req.count; i++){
    tempbuf.index = i;
    if(unlikely(!vid.enqueue_buffer(tempbuf))){
      log_critical("Couldn't enqueue initial video buffers.");
      return(false);
    }
  }

  return vid.start_streaming();
}

void Capture::close() noexcept
{
  if(vid.is_open()){
    vid.stop_streaming();
    for(int i=0; i<STREAMBUFS; i++){
      if(frames[i].data){
        munmap(frames[i].data,frames[i].length);
      }
    }
    vid.close();
  }
}

const Capture::Frame *Capture::capture_frame(void) noexcept
{
  if(unlikely(!vid.wait_for_frame(1000))){
    log_warning("Couldn't get a video frame within one second.");
    return nullptr;
  }

  while (true) {
    vid.dequeue_buffer(tempbuf); // Get the frame.
    // Poll to see if another frame is already available.
    // If not, break out now.
    if (!vid.is_frame_ready()) break;
    vid.enqueue_buffer(tempbuf); // Otherwise, drop this frame.
  }

  int i = static_cast<int>(tempbuf.index);
  frames[i].timestamp = tempbuf.timestamp;
  frames[i].field = (tempbuf.field == V4L2_FIELD_BOTTOM);

  return(&(frames[i]));
}

bool Capture::release_frame(const Capture::Frame *_frame) noexcept
{
  if(unlikely(!_frame)) return(false);
  tempbuf.index = static_cast<__u32>(_frame->index);
  return(vid.enqueue_buffer(tempbuf));
}
