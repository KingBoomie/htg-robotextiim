#include "common/log.h"
#include "common/macros.h"
#include "common/color_enum.h"
#include "cmv_shared/iofile.h"
#include "cmv_shared/util.h"
#include "cmvision/threshold.h"

namespace CMVision {

// TODO: Reduce code duplication between this `get_tmap` function and the
// two `set_tmap` functions that take a `uv_radius`.
int get_tmap(const uchar *tmap, int yi, int ui, int vi,
             uchar *vals, int max_valnum, int uv_radius) noexcept
{
    int n = 0;
    for (int u = ui - uv_radius; u <= ui + uv_radius; ++u) {
        if      (u < 0)            continue;
        else if (u >= tmap_u_size) break;

        for (int v = vi - uv_radius; v <= vi + uv_radius; ++v) {
            if      (v < 0)            continue;
            else if (v >= tmap_v_size) break;

            if (n >= max_valnum) return -1; // Ran out of space.
            vals[n] = get_tmap_fast(tmap, yi, u, v);
            ++n;
        }
    }
    return n;
}

int set_tmap(uchar *tmap, int yi, int ui, int vi, int color, int uv_radius) noexcept
{
    int n = 0;
    for (int u = ui - uv_radius; u <= ui + uv_radius; ++u){
        if      (u < 0)            continue;
        else if (u >= tmap_u_size) break;

        for (int v = vi - uv_radius; v <= vi + uv_radius; ++v) {
            if      (v < 0)            continue;
            else if (v >= tmap_v_size) break;

            set_tmap_fast(tmap, yi, u, v, static_cast<uchar>(color));
            ++n;
        }
    }
    return n;
}

int set_tmap(uchar *tmap, int yi, int ui, int vi,
             const uchar *vals, int valnum, int uv_radius) noexcept
{
    int n = 0;
    for (int u = ui - uv_radius; u <= ui + uv_radius; ++u){
        if      (u < 0)            continue;
        else if (u >= tmap_u_size) break;

        for (int v = vi - uv_radius; v <= vi + uv_radius; ++v) {
            if      (v < 0)            continue;
            else if (v >= tmap_v_size) break;

            if (n >= valnum) return -1; // Ran out of space.
            set_tmap_fast(tmap, yi, u, v, vals[n]);
            ++n;
        }
    }
    return n;
}

void reset_tmap(uchar *tmap) noexcept
{
    const int n = calc_tmap_index(tmap_y_size - 1, tmap_u_size - 1, tmap_v_size - 1);
    for (int i = 0; i < n; ++i) tmap[i] = color_background;
}

void ThresholdImage(uchar *cmap, const uchar *tmap, const uchar *img_yuyv, int img_size) noexcept
{
  const int rshift_y = 8 - tmap_bits_y,           rshift_u = 8 - tmap_bits_u, rshift_v = 8 - tmap_bits_v,
            lshift_y = tmap_bits_u + tmap_bits_v, lshift_u = tmap_bits_v,     lshift_v = 0;
  for (int i = 0; i < img_size; i += 2) { // i == pixel being processed (two at a time)
    const uchar *p = &img_yuyv[i * 2];  // Four chars per two pixels
    const int y1 = p[0], u = p[1], y2 = p[2], v = p[3];
    const int m = ((u >> rshift_u) << lshift_u) + ((v >> rshift_v) << lshift_v);
    cmap[i + 0] = tmap[m + ((y1 >> rshift_y) << lshift_y)];
    cmap[i + 1] = tmap[m + ((y2 >> rshift_y) << lshift_y)];
  }
}

int AddToHistogram(const uchar *cmap, int cmap_width, int cmap_height,
                   int x1, int y1, int x2, int y2, int *hist_count) noexcept
{
  x1 = bound(x1, 0, cmap_width  - 1);
  y1 = bound(y1, 0, cmap_height - 1);
  x2 = bound(x2, 0, cmap_width  - 1);
  y2 = bound(y2, 0, cmap_height - 1);

  for(int y=y1; y<=y2; y++){
    for(int x=x1; x<=x2; x++){
      const uchar color_index = cmap[y*cmap_width + x];
      ++hist_count[color_index];
    }
  }

  return((x2 - x1 + 1) * (y2 - y1 + 1));
}

void RgbToIndex(uchar *cmap, int cmap_size, rgb *img, rgb *colors, int color_num) noexcept
{
  int j = 0;
  for (int i = 0; i < cmap_size; ++i) {
    if (img[i] != colors[j]) {
      j = 0;
      while (j < color_num && img[i] != colors[j]) ++j;
    }
    cmap[i] = static_cast<uchar>(j);
  }
}

int RemapTMapColor(uchar *tmap, int src_color, int dest_color) noexcept
{
  int n = 0;
  for (int i = 0; i < tmap_size; ++i) {
    if (tmap[i] == src_color) {
      tmap[i] = static_cast<uchar>(dest_color);
      ++n;
    }
  }
  return n;
}

int CheckTMapColors(uchar *tmap, int num_colors, int default_color) noexcept
{
  int err_num = 0;

  for (int i = 0; i < tmap_size; ++i) {
    if (tmap[i] >= num_colors) {
      tmap[i] = static_cast<uchar>(default_color);
      ++err_num;
    }
  }

  if (err_num > 0) log_warning("Warning: " + tostr(err_num) + " tmap errors found.");
  return err_num;
}

bool LoadThresholdFile(uchar *tmap, const char *filename) noexcept
{
  if (!likely(tmap)) return false;

  static const int MaxBuf = 128;
  char buf[MaxBuf];
  int ny, nu, nv, num_read;

  IOFile *in;
  if(strstr(filename,".gz")){
    in = new GZipFile();
  }else{
    in = new NormalFile();
  }
  if(!in->open(filename,"r")) goto error;

  // read magic
  if(!in->gets(buf,MaxBuf)) goto error;
  buf[4] = 0;
  if(strcmp(buf,"TMAP")) goto error;

  // read type (ignore for now)
  if(!in->gets(buf,MaxBuf)) goto error;

  // read size
  if(!in->gets(buf,MaxBuf)) goto error;
  ny = nu = nv = 0;
  sscanf(buf,"%d %d %d",&ny,&nu,&nv);
  if (tmap_y_size!=ny || tmap_u_size!=nu || tmap_v_size!=nv) goto error;

  num_read = in->read(tmap, tmap_size);

  delete(in);
  return(num_read == tmap_size);

error:
  delete(in);
  return(false);
}

bool SaveThresholdFile(const uchar *tmap, const char *filename) noexcept
{
  static const int MaxBuf = 128;
  char buf[MaxBuf];
  int slen, wrote;

  IOFile *out;
  if(strstr(filename,".gz")){
    out = new GZipFile();
  }else{
    out = new NormalFile();
  }

  if (!likely(out->open(filename,"w"))) goto error;

  // write header
  snprintf(buf,MaxBuf,"TMAP\n_y_u_v%zu\n%d %d %d\n",
           sizeof(uchar), tmap_y_size, tmap_u_size, tmap_v_size);
  slen = static_cast<int>(strlen(buf));
  if(out->write(buf,slen) != slen) goto error;

  // write body
  wrote = out->write(tmap, tmap_size);

  delete(out);
  return(wrote == tmap_size);

error:
  delete(out);
  return(false);
}

} // namespace
