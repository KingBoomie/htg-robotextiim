#pragma once

void sample_click(int cam_idx, int x, int y, bool is_left_click) noexcept;
bool undo_sample(void) noexcept;
void flush_samples_to_file(void) noexcept;
