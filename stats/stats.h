#pragma once

#include "cmvision/types.h"

// Calculates the probability that an orange region is a
// ball. Just use this unless you know what you're doing.
double calc_pr_is_ball(const Region &reg) noexcept;

// Data based on which similarity to sample balls is determined:
double calc_side_ratio(const Region &reg) noexcept;
double calc_area_ratio(const Region &reg) noexcept;

// Statistical "distance" from sample data (which is taken from regions
// that we know are balls for sure (if the data was gathered correctly))
double calc_ball_stat_dist(double side, double area) noexcept;
double calc_ball_stat_dist(const Region &reg) noexcept;

double ball_pr_from_stat_dist(double dist) noexcept; // Convert distance to probability.
