#include <math.h>
#include "common/macros.h"
#include "stats/stats.h"

// The following gamma functions were copy-pasted from the SAMtools source code
// at https://github.com/samtools/htslib/blob/develop/kfunc.c in June 2015.

/* The MIT License

   Copyright (C) 2010, 2013 Genome Research Ltd.
   Copyright (C) 2011 Attractive Chaos <attractor@live.co.uk>

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

// Inlining helps the compiler optimize away calculations with constants.

/* Log gamma function
 * \log{\Gamma(z)}
 * AS245, 2nd algorithm, http://lib.stat.cmu.edu/apstat/245
 */
static inline double kf_lgamma(double z) noexcept {
	double x = 0.;
	x += 0.1659470187408462e-06 / (z+7.);
	x += 0.9934937113930748e-05 / (z+6.);
	x -= 0.1385710331296526     / (z+5.);
	x += 12.50734324009056      / (z+4.);
	x -= 176.6150291498386      / (z+3.);
	x += 771.3234287757674      / (z+2.);
	x -= 1259.139216722289      / (z+1.);
	x += 676.5203681218835      / z;
	x += 0.9999999999995183;
	return log(x) - 5.58106146679532777 - z + (z-0.5) * log(z+6.5);
}

/* The following computes regularized incomplete gamma functions.
 * Formulas are taken from Wiki, with additional input from Numerical
 * Recipes in C (for modified Lentz's algorithm) and AS245
 * (http://lib.stat.cmu.edu/apstat/245).
 *
 * A good online calculator is available at:
 *
 *   http://www.danielsoper.com/statcalc/calc23.aspx
 *
 * It calculates upper incomplete gamma function, which equals
 * kf_gammaq(s,z)*tgamma(s).
 */

#define KF_GAMMA_EPS 1e-14
#define KF_TINY      1e-290 // Avoid denormalized doubles ??

// regularized lower incomplete gamma function, by series expansion
static inline double _kf_gammap(double s, double z) noexcept {
	double sum, x;
	int k;
	for (k = 1, sum = x = 1.; k < 100; ++k) { // Is 100 an arbitrary number ??
		sum += (x *= z / (s + k)); // TODO: Test how often the value of k reaches 100.
		if (x / sum < KF_GAMMA_EPS) break; // TODO: Profile with / without the branch.
	}
	return exp(s * log(z) - z - kf_lgamma(s + 1.) + log(sum));
}

// regularized upper incomplete gamma function, by continued fraction
static inline double _kf_gammaq(double s, double z) noexcept {
	int j;
	double C, D, f;
	f = 1. + z - s; C = f; D = 0.;
	// Modified Lentz's algorithm for computing continued fraction
	// See Numerical Recipes in C, 2nd edition, section 5.2
	for (j = 1; j < 100; ++j) {
		double a = j * (s - j), b = (j<<1) + 1 + z - s, d;
		D = b + a * D;
		if (D < KF_TINY) D = KF_TINY;
		C = b + a / C;
		if (C < KF_TINY) C = KF_TINY;
		D = 1. / D;
		d = C * D;
		f *= d;
		if (fabs(d - 1.) < KF_GAMMA_EPS) break;
	}
	return exp(s * log(z) - z - kf_lgamma(s) - log(f));
}

static inline double kf_gammap(double s, double z) noexcept {
	return unlikely(z <= 1. || z < s) ? _kf_gammap(s, z) : (1. - _kf_gammaq(s, z));
}

// All data is in the order side_ratio, area_ratio.
static const int test_num = 2; // aka degrees of freedom

// cov - covariance, pr - probability
static const double means[test_num] = {1.000214, 0.752764},
    inv_cov_mat[test_num][test_num] = {{100.007, -0.820055}, {-0.820055, 100.007}};

// Without Tikhonov bias:
// inv_cov_mat[test_num][test_num] = {{1104.1, -125.05}, {-125.05, 1395.38}};
// cov mat:
// 0.000915	0.000082
// 0.000082 0.000724

double calc_side_ratio(const Region &reg) noexcept {
    // The sides of a bounding box of a circle are the same length.
    return static_cast<double>(reg.height) / static_cast<double>(reg.width);
}

double calc_area_ratio(const Region &reg) noexcept {
    // The ratio of the area of a circle and its bounding box is pi/4 = ~0.78539816339.
    const double bbox_area = static_cast<double>(reg.height * reg.width);
    return static_cast<double>(reg.area) / bbox_area;
}

// TODO: dist for determining whether two regions represent
// the same ball (on different frames).

static double mahalanobis_dist(const double *test_results) noexcept {

    double diff[test_num];
    for (int j = 0; j < test_num; ++j) diff[j] = test_results[j] - means[j];

    double v[test_num];
    for (int j = 0; j < test_num; ++j) {
        double dot = 0.0;
        for (int k = 0; k < test_num; ++k) {
            dot += inv_cov_mat[j][k] * diff[k];
        }
        v[j] = dot;
    }

    double dist = 0.0;
    for (int j = 0; j < test_num; ++j) dist += v[j] * diff[j];
    return dist;
}

double calc_ball_stat_dist(const Region &reg) noexcept {
    const double test_results[test_num] = {calc_side_ratio(reg), calc_area_ratio(reg)};
    return mahalanobis_dist(test_results);
}

double calc_ball_stat_dist(double side_ratio, double area_ratio) noexcept {
    const double test_results[test_num] = {side_ratio, area_ratio};
    return mahalanobis_dist(test_results);
}

double ball_pr_from_stat_dist(double dist) noexcept {
    // `dist` is e.g. sqr mahalanobis dist.
    // return not(chi_sqr_cdf(dist))
    return 1.0 - kf_gammap(static_cast<double>(test_num) * 0.5, dist * 0.5);
}

double calc_pr_is_ball(const Region &reg) noexcept {
    return ball_pr_from_stat_dist(calc_ball_stat_dist(reg));
}
