#include <vector>
#include <stdio.h>  // fprintf, remove
#include "common/log.h"
#include "common/macros.h"
#include "common/undo_buf.h"
#include "common/color_enum.h"
#include "sync/spinlock.h"
#include "find/cam.h"
#include "stats/stats.h"
#include "stats/internal.h"
#include "stats/sample.h"

using std::vector;

struct BallSample {
    // TODO: RelPos rel;
    double side, area, stat_dist, pr;
    bool moving;
};

struct GateSample { double side, area; };

static BallSample make_ball_sample(const Region &reg, bool is_moving) noexcept {
    BallSample s = {calc_side_ratio(reg), calc_area_ratio(reg)};
    s.stat_dist = calc_ball_stat_dist(s.side, s.area);
    s.pr = ball_pr_from_stat_dist(s.stat_dist);
    s.moving = is_moving;
    return s;
}

static GateSample make_gate_sample(const Region &reg) noexcept {
    return {calc_side_ratio(reg), calc_area_ratio(reg)};
}

static void log_sample(BallSample s) noexcept {
    log_info(
        "\nBall sample:"
        "\nSide ratio:\t\t\t\t"                    + std::to_string(s.side)      +
        "\nArea ratio:\t\t\t\t"                    + std::to_string(s.area)      +
        "\nStatistical distance:\t\t\t"            + std::to_string(s.stat_dist) +
        "\nProbability of being a ball:\t\t"       + std::to_string(s.pr)        +
        "\nYou indicated this ball is " + (s.moving ? "moving.\n" : "not moving.\n")
    );
}

static void log_sample(GateSample s) noexcept {
    log_info("\nGate sample:");
    log_info("Side ratio:\t\t\t\t"                    + std::to_string(s.side));
    log_info("Area ratio:\t\t\t\t"                    + std::to_string(s.area));
    log_info(""); // Print a newline.
}

template <typename Sample>
static bool collect_sample(vector<Sample> &samples, Sample s) noexcept {
    log_info("Sampling...");
    log_sample(s);
    try {
        samples.push_back(s);
    } catch (std::bad_alloc) {
        log_warning("Couldn't allocate memory for sample.");
        return false;
    }
    return true;
}

static void rem_file(FILE *file, const std::string filename) noexcept {
    fclose(file);
    remove(filename.c_str());
}

static bool write_sample_to_file(FILE *file, BallSample s) noexcept {
    return fprintf(file, "%lf\t%lf\t%lf\t%lf\t%d\n", s.side, s.area,
                   s.stat_dist, s.pr, static_cast<int>(s.moving)) > 0;
}

static bool write_sample_to_file(FILE *file, GateSample s) noexcept {
    return fprintf(file, "%lf\t%lf\n", s.side, s.area) > 0;
}

template <typename Sample>
static void save_samples(const vector<Sample> &samples, const std::string obj_name,
                         const std::string filename) noexcept {
    FILE *file = fopen(filename.c_str(), "w");
    if (unlikely(!file)) {
        log_warning("Couldn't open " + obj_name + " sample file for writing. "
                    "[" + filename + "]");
        return;
    }
    if (unlikely(fprintf(file, "%d\n", samples.size()) < 0)) {
        log_warning("Couldn't write " + obj_name + " sample num.");
        rem_file(file, filename);
        return;
    }
    for (size_t i = 0; i < samples.size(); ++i) {
        if (unlikely(!write_sample_to_file(file, samples[i]))) {
            log_warning("Couldn't write " + obj_name + " sample #" + std::to_string(i));
            // Don't `rem_file` here - it's better to keep the samples that
            // have been saved so far and fix the number of samples written
            // to the beginning of the file manually.
            break;
        }
    }
    fclose(file);
}

template <typename Sample>
static void flush_samples_helper(vector<Sample> &samples, const std::string obj_name,
                                 const std::string cur_time_str) noexcept {
    if (samples.size() > 0) {
        save_samples(samples, obj_name, get_sample_filename(obj_name, cur_time_str));
    }
    samples.clear();
}

static UndoBuf<bool, 32> s_was_ball_sample;

static vector<BallSample> s_ball_samples;
static vector<GateSample> s_gate_samples;

// This lock will likely have no contention at all.
static Spinlock s_lock;

void flush_samples_to_file(void) noexcept {
    const std::string c = get_cur_date_time_str();
    s_lock.lock();
    flush_samples_helper(s_ball_samples, "ball", c);
    flush_samples_helper(s_gate_samples, "gate", c);
    s_lock.unlock();
}

bool undo_sample(void) noexcept {
    s_lock.lock();
    const bool *p = s_was_ball_sample.undo();
    if (!p) {
        s_lock.unlock();
        log_warning("Can't undo - order of added samples unknown.");
        return false;
    }
    if (*p) {
        s_ball_samples.pop_back();
    } else {
        s_gate_samples.pop_back();
    }
    s_lock.unlock();
    log_info("Undid sample (removed latest sample).");
    return true;
}

void sample_click(int cam_idx, int x, int y, bool is_left_click) noexcept {

    int c;

    const Region *reg = find_region(cam_idx, x, y, c);
    if (!likely(reg)) {
        log_warning("Couldn't find region to sample.");
        return;
    }
    if (unlikely(c != color_orange && c != color_blue && c != color_yellow)) {
        log_warning("Sample pixel isn't ball or gate-colored.");
        return;
    }

    s_lock.lock();
    if (c == color_orange) {
        if (likely(collect_sample(s_ball_samples,
                                  make_ball_sample(*reg, is_left_click)))) {
            s_was_ball_sample.add(true);
        }
    } else {
        if (likely(collect_sample(s_gate_samples, make_gate_sample(*reg)))) {
            s_was_ball_sample.add(false);
        }
    }
    s_lock.unlock();
}
