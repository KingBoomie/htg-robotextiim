#pragma once

#include <string>

inline std::string get_sample_filename(const std::string obj_name,
                                       const std::string sample_date_time) noexcept {
    return "../../../samples/" + obj_name + "/" + sample_date_time + ".smpl";
}
