#include <vector>
#include <stdio.h>
#include <math.h>
#include "common/log.h"
#include "common/macros.h"
#include "stats/internal.h"

using std::vector;

struct BallData { // TODO: Better name
    static const int test_num{2};
    double a[test_num];
};

struct GateData {
    static const int test_num{2};
    double a[test_num];
};

static bool get_data(FILE *file, vector<BallData> &data) noexcept {
    BallData d;
    if (unlikely(fscanf(file, "%lf\t%lf\t%*lf\t%*lf\t%*d\n",
                        &d.a[0], &d.a[1]) < 0)) return false;
    try { data.push_back(d); } catch (std::bad_alloc) { return false; }
    return true;
}

static bool get_data(FILE *file, vector<GateData> &data) noexcept {
    GateData d;
    if (unlikely(fscanf(file, "%lf\t%lf\t\n",
                        &d.a[0], &d.a[1]) < 0)) return false;
    try { data.push_back(d); } catch (std::bad_alloc) { return false; }
    return true;
}

template <typename Data>
static void read_data(const std::string filename, vector<Data> &data) noexcept {

    FILE *file = fopen(filename.c_str(), "r");
    if (unlikely(!file)) {
        log_critical("Couldn't open sample file with filename `" +
                     filename + "`.");
        return;
    }

    int num;
    if (unlikely(fscanf(file, "%d\n", &num) < 0)) {
        log_critical("Couldn't read how many samples there are.");
        goto cleanup;
    }

    for (int i = 0; i < num; ++i) {
        if (!get_data(file, data)) {
            log_warning("Could only read " + tostr(i) +
                        " samples of " + tostr(num) + ".");
            goto cleanup;
        }
    }

cleanup:
    fclose(file);
}

template <typename T, typename F>
static double kahan_sum(const vector<T> &v, F f) noexcept {
    double sum = 0.0, c = 0.0;
    for (size_t i = 0; i < v.size(); ++i) {
        const double y = f(v[i]) - c;
        const double t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }
    return sum;
}

template <typename Data>
static void log_means_and_vars(double *means, double *vars,
                               const vector<Data> &data) noexcept {
    const double n = static_cast<double>(data.size());
    std::string mean_str = "Means: ", var_str = "Variances: ";
    for (int j = 0; j < Data::test_num; ++j) {
        const double mean = means[j] = kahan_sum(data, [j](Data d){return d.a[j];}) / n;
        mean_str += tostr(mean) + ' ';
        const double sum_sqr = kahan_sum(data, [j, mean](Data d) {
            const double x = d.a[j] - mean;
            return x * x;
        });
        const double sum = kahan_sum(data, [j, mean](Data d) { return d.a[j] - mean; });
        var_str += tostr(vars[j] = (sum_sqr - (sum * sum) / n) / (n - 1.0)) + ' ';
    }
    log_info(mean_str);
    log_info(var_str);
}

template <typename Data>
static void log_cov_mat(double cov_mat[Data::test_num][Data::test_num],
                        double *means, const vector<Data> &data) noexcept {
    std::string cov_mat_str = "Covariance matrix:\n";
    for (int j = 0; j < Data::test_num; ++j) {
        const double mean_j = means[j];
        for (int k = 0; k < Data::test_num; ++k) {
            const double mean_k = means[k];
            cov_mat[j][k] = kahan_sum(data, [j, k, mean_j, mean_k](Data d) {
                    return (d.a[j] - mean_j) * (d.a[k] - mean_k);
            }) / (static_cast<double>(data.size()) - 1.0);
            cov_mat_str += tostr(cov_mat[k][j] = cov_mat[j][k]) + '\t';
        }
        cov_mat_str += '\n';
    }
    log_info(cov_mat_str);
}

template <int test_num>
static bool get_cov_eigenvecs_and_vals(double  cov_eigenvecs[test_num][test_num],
                                       double *cov_eigenvals) noexcept {
    FILE *file;

    log_info("Enter 'f' and a filename to read covariance matrix eigenvalues "
             "and eigenvectors from a file or any other character for manual input.");
    const int c = getchar();
    if (c < 0) {
        log_critical("Couldn't get character.");
        return false;
    }
log_debug("char");

    const bool read_from_file = (c == static_cast<int>('f'));
    if (read_from_file) {
        char filename[200];
        if (unlikely(scanf(" %199s ", filename) < 0)) {
            log_critical("Couldn't get filename.");
            return false;
        }
        file = fopen(filename, "r");
        if (unlikely(!file)) {
            log_critical("Couldn't open file with eigenvalues and eigenvectors.");
            return false;
        }
    } else {
        file = stdin;
    }
log_debug("filename");
    //const char *float_fmt = "%*[^-+012356789.e]%f%*[^-+012356789.e]";
    const char *float_fmt = "%lf ";
    if (!read_from_file) log_info("Enter covariance matrix eigenvalues.");
    for (int j = 0; j < test_num; ++j) {
        if (unlikely(fscanf(file, float_fmt, &cov_eigenvals[j]) <= 0)) {
            log_critical("Couldn't read eigenvalue " + tostr(j) + ".");
            goto error;
        }
    }

    if (!read_from_file) log_info("Enter covariance matrix eigenvectors.");
    for (int j = 0; j < test_num; ++j) {
        for (int k = 0; k < test_num; ++k) {
            if (unlikely(fscanf(file, float_fmt, &cov_eigenvecs[j][k]) <= 0)) {
                log_critical("Couldn't read eigenvector " + tostr(j) +
                             " component " + tostr(k) + ".");
                goto error;
            }
        }
    }

    if (read_from_file) fclose(file);
    return true;
error:
    if (read_from_file) fclose(file);
    return false;
}

template <typename Data>
static void log_stats(const vector<Data> &data) noexcept {

    double means[Data::test_num], vars[Data::test_num];
    log_means_and_vars(means, vars, data);

    double cov_mat[Data::test_num][Data::test_num];
    log_cov_mat(cov_mat, means, data);

    double cov_eigenvecs[Data::test_num][Data::test_num], cov_eigenvals[Data::test_num];
    if (!get_cov_eigenvecs_and_vals<Data::test_num>(cov_eigenvecs, cov_eigenvals)) {
        return;
    }

    // I don't know what this is, but we'll need it later.
    vector<double> u[Data::test_num];
    for (int j = 0; j < Data::test_num; ++j) {
        try {
            u[j].resize(data.size());
        } catch (std::bad_alloc) {
            log_critical("Couldn't allocate u vector " + tostr(j) + '.');
            return;
        }
        for (size_t i = 0; i < data.size(); ++i) {
            for (int k = 0; k < Data::test_num; ++k) {
                u[j][i] += (data[i].a[j] - means[j]) * cov_eigenvecs[j][k];
            }
        }
    }

    double smoothing_factors[Data::test_num]; // TODO: Tune this.
    for (int j = 0; j < Data::test_num; ++j) {
        smoothing_factors[j] = 100.0 * sqrt(cov_eigenvals[j]);
    }

    vector<double> smooth_data[Data::test_num];
    for (int j = 0; j < Data::test_num; ++j) {
        try {
            smooth_data[j].reserve(data.size());
        } catch (std::bad_alloc) {
            log_critical("Couldn't allocate smooth_data vector " + tostr(j) + '.');
            return;
        }
        for (size_t i = 0; i < data.size(); ++i) {
            if (fabs(u[j][i]) < smoothing_factors[j]) {
                smooth_data[j].push_back(u[j][i]);
            }
        }
    }

    double quasi_means[Data::test_num];
    std::string quasi_mean_str = "Quasi means:\n";
    auto sqr = [](double x) { return x*x; };
    for (int j = 0; j < Data::test_num; ++j) {
        double s = 0.0;
        for (int k = 0; k < Data::test_num; ++k) s += means[k] * cov_eigenvecs[j][k];
        quasi_means[j] = kahan_sum(smooth_data[j], [s](double x) { return x + s; }) /
                         static_cast<double>(smooth_data[j].size());
        quasi_mean_str += tostr(quasi_means[j]) + "\t";
    }
    log_info(quasi_mean_str);

    double adjusted_means[Data::test_num];
    std::string adjusted_mean_str = "Adjusted means:\n";
    for (int j = 0; j < Data::test_num; ++j) adjusted_means[j] = 0.0;
    for (int j = 0; j < Data::test_num; ++j) {
        for (int k = 0; k < Data::test_num; ++k) {
            adjusted_means[k] += quasi_means[j] * cov_eigenvecs[j][k];
        }
        adjusted_mean_str += tostr(adjusted_means[j]) + "\t";
    }
    log_info(adjusted_mean_str);

    vector<double> quasi_u[Data::test_num];
    for (int j = 0; j < Data::test_num; ++j) {
        try {
            quasi_u[j].resize(data.size());
        } catch (std::bad_alloc) {
            log_critical("Couldn't allocate quasi_u vector " + tostr(j) + '.');
            return;
        }
        for (size_t i = 0; i < data.size(); ++i) {
            double dot = 0.0;
            for (int k = 0; k < Data::test_num; ++k) {
                dot += (data[i].a[k] - adjusted_means[k]) * cov_eigenvecs[j][k];
            }
            quasi_u[j][i] = dot;
        }
    }

    vector<double> positive_data[Data::test_num], neg_data[Data::test_num];
    for (int j = 0; j < Data::test_num; ++j) {
        try {
            positive_data[j].reserve(data.size());
            neg_data[j].reserve(data.size());
        } catch (std::bad_alloc) {
            log_critical("Couldn't allocate positive_data or neg_data vector " +
                         tostr(j) + '.');
            return;
        }
        for (size_t i = 0; i < data.size(); ++i) {
            const double qu = quasi_u[j][i];
            if (qu < 0.0) {
                if (-qu < smoothing_factors[j]) neg_data[j].push_back(qu);
            } else {
                if (+qu < smoothing_factors[j]) positive_data[j].push_back(qu);
            }
        }
    }

    double neg_quasi_vars[Data::test_num], positive_quasi_vars[Data::test_num];
    std::string quasi_var_str = "Quasi-variances:\n";
    for (int j = 0; j < Data::test_num; ++j) {
        positive_quasi_vars[j] = kahan_sum(positive_data[j], sqr) /
                           static_cast<double>(positive_data[j].size());
             neg_quasi_vars[j] = kahan_sum(neg_data[j], sqr) /
                           static_cast<double>(neg_data[j].size());
        quasi_var_str += ("Positive:\t" + tostr(positive_quasi_vars[j]) +
                          "\tNeg:\t"    + tostr(neg_quasi_vars[j]) + '\n');
    }
    log_info(quasi_var_str);
}

template <typename Data>
static void process_data(vector<Data> &data, const std::string obj_name,
                         const std::string sample_date_time) noexcept {
    read_data(get_sample_filename(obj_name, sample_date_time), data);
    log_stats(data);
}

int main_old4(int argc, char **argv) noexcept {

    if (!init_logger(LOG_DEBUG)) return 1;

    if (argc < 2) {
        log_critical("Not enough command line arguments: expected the "
                     "time a set of samples were taken.");
        stop_logger();
        return 0;
    }

    bool do_ball = true, do_gate = true;
    if (argc >= 3) {
        if (argv[2][0] == 'b') do_gate = false;
        if (argv[2][0] == 'g') do_ball = false;
    }
    const std::string sample_date_time = argv[1];

    if (do_ball) {
        vector<BallData> ball_data;
        process_data(ball_data, "ball", sample_date_time);
    }
    if (do_gate) {
        vector<GateData> gate_data;
        process_data(gate_data, "gate", sample_date_time);
    }

    stop_logger();
    return 0;
}
