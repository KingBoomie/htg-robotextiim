double angle_to_rot_around_point(RelPos move_to, RelPos rot_around) {
    // FIXME: Make (move_to, rot_around, (0, 0)) a right triangle first.
    return arcsin(move_to.dist / rot_around.dist) / (2.0 * M_PI);
}

void line_up_ball_gate(RelPos ball, RelPos gate) {
    const AbsPos tangent_a = to_abs({.dist = robot_rad, .dir = gate.dir}),
                 tangent_b = to_abs(ball) - (unit_vec(gate.dir) * ball_rad);
    const double tangent_slope = (tangent_b.y - tangent_a.y) / (tangent_b.x - tangent_a.x);
    const AbsPos abs_gate = to_abs(gate);
    const double eps = 0.08; // 8 cm
    const RelPos move_here = to_rel({gate.x, tangent_slope * gate.x - robot_rad - eps});
    move_and_rot(move_here, angle_to_rot_around_point(move_here, gate));
}