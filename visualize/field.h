#pragma once

#include "common/vec2.h"

struct Gui;

bool init_visualization(Gui *gui) noexcept;
void visualize_field(const Vec2d *ball_positions, const double *ball_prs, int ball_num,
                     Vec2d robot_pos, double robot_dir) noexcept;
