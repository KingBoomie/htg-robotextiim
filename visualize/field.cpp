#include <utility> // swap
#include "common/color_enum.h"
#include "common/log.h"
#include "common/check.h"
#include "common/constants.h"
#include "common/rel_pos.h"
#include "find/ball.h"
#include "find/gate.h"
#include "loc/loc.h"
#include "gui/gui.h"   // is_gui_initialized
#include "gui/draw.h"
#include "visualize/field.h"

static constexpr double offside_width = 0.2, // TODO: Measure this in meters.
                        carpet_len   = field_len   + 2.0 * gate_depth,
                        carpet_width = field_width + 2.0 * offside_width;
static constexpr int border_width = 2, carpet_pix_len = frame_height;
static constexpr double pix_scale = static_cast<double>(carpet_pix_len) / carpet_len;
static constexpr int carpet_pix_width = static_cast<int>(pix_scale * carpet_width);

static bool is_in_bounds(int x, int y) noexcept {
    return x >= 0 && x < carpet_pix_width && y >= 0 && y < carpet_pix_len;
}

static Vec2d carpet_coord_from_abs_pos(Vec2d abs) noexcept {
    return {abs.x + offside_width, field_len + gate_depth - abs.y};
}

static Vec2i pix_from_rel(RelPos rel, Vec2d robot_pos, double robot_dir) noexcept {
    rel.dir = robot_dir - rel.dir;
    const Vec2d c = carpet_coord_from_abs_pos(robot_pos + to_vec2d(rel)) * pix_scale;
    return c + Vec2d(0.5, 0.5); // Add 0.5 for rounding to nearest instead of toward 0.
}

static void visualize_robot(Vec2d robot_pos, double robot_dir, DrawBuf *db) {

    const Vec2i robot_pix = carpet_coord_from_abs_pos(robot_pos) * pix_scale;
    if (unlikely(!is_in_bounds(robot_pix.x, robot_pix.y))) {
        log_warning("Attempted to draw robot at coordinates (" +
                    tostr(robot_pix.x) + "; " + tostr(robot_pix.y) + ").");
        return;
    }

    Shape robot_shape = {{}, 0, 0, 0, Shape::Type::circle, true};
    robot_shape.circle = {robot_pix.x, robot_pix.y, static_cast<int>(robot_rad * pix_scale)};
    if (unlikely(!draw_shape(db, robot_shape))) {
        log_warning("Couldn't draw robot on field.");
        return;
    }

    RelPos rel;
    rel.dir  = 0.0;
    rel.dist = robot_rad;
    Vec2i pix = pix_from_rel(rel, robot_pos, robot_dir);
    if (unlikely(!is_in_bounds(pix.x, pix.y))) {
        log_warning("Attempted to draw robot direction indicator at coordinates (" +
                    tostr(pix.x) + "; " + tostr(pix.y) + ").");
        return;
    }

    Shape s;
    if (get_opnt_gate_color() == color_blue) {
        s.r = s.g = 1.0;
        s.b = 0.0;
    } else {
        s.r = s.g = 0.0;
        s.b = 0.8;
    }
    s.type = Shape::Type::circle;
    s.do_fill = true;
    s.circle = {pix.x, pix.y, 3};
    if (unlikely(!draw_shape(db, s))) {
        log_warning("Couldn't draw robot direction indicator.");
    }
}

static void visualize_balls(const Vec2d *positions, const double *prs, int num,
                            DrawBuf *db) noexcept {
    for (int i = 0; i < num; ++i) {
        const Vec2i pix = pix_scale * carpet_coord_from_abs_pos(positions[i]);
        if (unlikely(!is_in_bounds(pix.x, pix.y))) {
            log_warning("Attempted to draw ball at coordinates (" +
                        tostr(pix.x) + "; " + tostr(pix.y) + ").");
            continue;
        }
        Shape s = {{}, prs[i], prs[i] * 0.65, 0.0, Shape::Type::circle, true};
        s.circle = {pix.x, pix.y, 3};
        if (unlikely(!draw_shape(db, s))) log_warning("Couldn't draw ball on field.");
    }
}

static void visualize_aim(Vec2d robot_pos, double robot_dir, DrawBuf *db) noexcept {
    const Vec2<int> pix = pix_from_rel(get_aim(nullptr), robot_pos, robot_dir);
    if (unlikely(!is_in_bounds(pix.x, pix.y))) {
        log_warning("Attempted to draw aim at coordinates (" +
                    tostr(pix.x) + "; " + tostr(pix.y) + ").");
        return;
    }
    Shape s = {{}, 255, 0, 100, Shape::Type::circle, true};
    s.circle = {pix.x, pix.y, 2};
    if (unlikely(!draw_shape(db, s))) log_warning("Couldn't draw aim on field.");
}

static void init_field_rgb(rgb field_rgb[carpet_pix_len][carpet_pix_width]) noexcept {

#define check_bounds() check_ret(is_in_bounds(x, y),)

    for (int x = 0; x < carpet_pix_width; ++x) {
        for (int y = 0; y < carpet_pix_len; ++y) field_rgb[y][x] = {0, 200, 0};
    }

    const rgb white_rgb = {255, 255, 255}, black_rgb = {0, 0, 0};
    const int gate_start_x = (carpet_width - gate_width) * 0.5 * pix_scale,
              gate_end_x   = (carpet_width + gate_width) * 0.5 * pix_scale,
              gate_pix_depth = gate_depth * pix_scale;
    const int our_gate_start = carpet_pix_len - gate_pix_depth;

    // Draw vertical borders.
    const int pix_offside = offside_width * pix_scale;
    const int right_offside = carpet_pix_width - pix_offside;
    for (int y = gate_pix_depth; y < our_gate_start; ++y) {
        for (int x = pix_offside - border_width; x < pix_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = black_rgb;
        }
        for (int x = pix_offside; x < pix_offside + border_width; ++x) {
            check_bounds();
            field_rgb[y][x] = white_rgb;
        }
        for (int x = right_offside - border_width; x < right_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = white_rgb;
        }
        for (int x = right_offside; x < right_offside + border_width; ++x) {
            check_bounds();
            field_rgb[y][x] = black_rgb;
        }
    }

    // Draw horizontal borders.
    for (int y = gate_pix_depth - border_width; y < gate_pix_depth; ++y) {
        for (int x = pix_offside; x < right_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = black_rgb;
        }
    }
    for (int y = gate_pix_depth; y < gate_pix_depth + border_width; ++y) {
        for (int x = pix_offside; x < right_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = white_rgb;
        }
    }
    for (int y = our_gate_start - border_width; y < our_gate_start; ++y) {
        for (int x = pix_offside; x < right_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = white_rgb;
        }
    }
    for (int y = our_gate_start; y < our_gate_start + border_width; ++y) {
        for (int x = pix_offside; x < right_offside; ++x) {
            check_bounds();
            field_rgb[y][x] = black_rgb;
        }
    }

    // Draw gates.

    rgb opnt_rgb = {0, 0, 200}, our_rgb = {255, 255, 0};
    const int c = get_opnt_gate_color();
    check(c != 0); // If c == 0, the gate's color hasn't been set.
    if (c == color_yellow) std::swap(opnt_rgb, our_rgb);

    for (int y = 0; y < gate_pix_depth; ++y) {
        for (int x = gate_start_x; x < gate_end_x; ++x) {
            check_bounds();
            field_rgb[y][x] = opnt_rgb;
        }
    }
    for (int y = our_gate_start; y < carpet_pix_len; ++y) {
        for (int x = gate_start_x; x < gate_end_x; ++x) {
            check_bounds();
            field_rgb[y][x] = our_rgb;
        }
    }

#undef check_bounds
}

static DrawBuf *s_field_out;
static rgb s_field_rgb[carpet_pix_len][carpet_pix_width];

void visualize_field(const Vec2d *ball_positions, const double *ball_prs, int ball_num,
                     Vec2d robot_pos, double robot_dir) noexcept {
    // We don't need to update the DrawBuf itself, but this function tells
    // us whether it's visible and `finish_updating_draw_buf` will get
    // the buf redrawn. TODO: Remove this hack.
    if (!begin_updating_draw_buf(s_field_out)) return;
    visualize_robot(robot_pos, robot_dir, s_field_out);
    visualize_balls(ball_positions, ball_prs, ball_num, s_field_out);
    visualize_aim(  robot_pos, robot_dir, s_field_out);
    finish_updating_draw_buf(s_field_out);
}

bool init_visualization(Gui *gui) noexcept {

    check_ret(is_gui_initialized(gui), false);
    s_field_out = add_buf_to_draw(gui, reinterpret_cast<uchar*>(s_field_rgb),
                                  carpet_pix_width, carpet_pix_len, "Field", false);
    if (!likely(s_field_out)) return false;

    init_field_rgb(s_field_rgb);
    log_info("Visualized carpet width in pixels: " + tostr(carpet_pix_width));
    return true;
}
