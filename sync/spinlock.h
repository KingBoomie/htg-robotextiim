#pragma once

#include "sync/internal.h"

#ifndef NDEBUG
#include "common/log.h"
#endif

class alignas(cache_line_size) Spinlock {

    std::atomic<bool> locked{false};

public:

    bool is_locked() const noexcept { return locked.load(std::memory_order_acquire); }
    bool try_lock() noexcept { return !locked.exchange(true, std::memory_order_acquire); }
    void unlock() noexcept { locked.store(false, std::memory_order_release); }

    void lock() noexcept {
        while (locked.exchange(true, std::memory_order_acquire)) {
#ifndef NDEBUG
            log_warning("Spinning");
#endif
            while (locked.load(std::memory_order_relaxed)) {
                _mm_pause();
                std::atomic_signal_fence(std::memory_order_acquire);
            }
        }
    }
};
