#pragma once

#ifdef __linux__

class TimedSemaphore { // Adapted from https://github.com/preshing/cpp11-on-multicore

    sem_t m_sema;

    TimedSemaphore(const TimedSemaphore&) = delete;
    TimedSemaphore& operator=(const TimedSemaphore&) = delete;

public:

    explicit TimedSemaphore(unsigned initial_count = 0) noexcept {
        sem_init(&m_sema, 0, initial_count);
    }
    ~TimedSemaphore() noexcept { sem_destroy(&m_sema); }

    void wait() noexcept {
        // stackoverflow.com/questions/2013181/gdb-causes-sem-wait-to-fail-with-eintr-error
        int rc;
        do rc = sem_wait(&m_sema); while (rc == -1 && errno == EINTR);
    }

    template<class Rep, class Period>
    bool wait_for(const std::chrono::duration<Rep, Period>& d) noexcept {
        struct timespec t;
        t.tv_sec  = std::duration_cast<std::chrono::seconds>(d).count();
        t.tv_nsec = std::duration_cast<std::chrono::nanoseconds>(d).count();
        return sem_timedwait(&m_sema, &t) == 0;
    }

    template<class Clock, class Duration>
    bool wait_until(const std::chrono::time_point<Clock, Duration>& t) noexcept {
        return wait_for(t - Clock::now());
    }

    void notify() noexcept { sem_post(&m_sema); }
    void notify(int count) noexcept { while (--count >= 0) sem_post(&m_sema); }
};

#else

class TimedSemaphore {

    unsigned count;
    std::mutex mutex;
    std::condition_variable cv;

    TimedSemaphore(const TimedSemaphore&) = delete;
    TimedSemaphore& operator=(const TimedSemaphore&) = delete;

public:
    explicit TimedSemaphore(unsigned initial_count = 0) noexcept : count{initial_count} {}

    void notify() noexcept {
        mutex.lock();
        ++count;
        mutex.unlock();
        cv.notify_one();
    }

    void wait() noexcept {
        std::unique_lock<std::mutex> lock{mutex};
        cv.wait(lock, [&]{ return count > 0; });
        --count;
    }

    template<class Rep, class Period>
    bool wait_for(const std::chrono::duration<Rep, Period>& d) noexcept {
        std::unique_lock<std::mutex> lock{mutex};
        const auto finished = cv.wait_for(lock, d, [&]{ return count > 0; });
        if (finished) --count;
        return finished;
    }

    template<class Clock, class Duration>
    bool wait_until(const std::chrono::time_point<Clock, Duration>& t) noexcept {
        std::unique_lock<std::mutex> lock{mutex};
        const auto finished = cv.wait_until(lock, t, [&]{ return count > 0; });
        if (finished) --count;
        return finished;
    }
};

#endif // __linux__
