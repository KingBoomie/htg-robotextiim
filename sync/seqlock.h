#pragma once

#include "sync/internal.h"
#include "common/check.h"
#include "common/macros.h"

class alignas(cache_line_size) Seqlock {

    std::atomic<unsigned> seq{0};

public:

    // Pass the return value to `finish_read`.
    unsigned begin_read() const noexcept {
        unsigned s;
        while (unlikely((s = seq.load(std::memory_order_relaxed)) & 1)) _mm_pause();
        std::atomic_thread_fence(std::memory_order_acquire);
        return s;
    }

    bool finish_read(unsigned old_seq) const noexcept {
        // TODO: Should false (try again) or true (use read data)
        // be returned if `old_seq` is odd? (It should never
        // actually be odd.)
        check_ret((old_seq & 1) == 0, false);
        std::atomic_thread_fence(std::memory_order_acquire); // load-load
        const unsigned s = seq.load(std::memory_order_relaxed);
        return s == old_seq; // If they aren't equal, the data has been
                             // written to while we were reading it or
                             // is been written right now (iff s is odd).
    }

    // This method assumes that there is either only one writer
    // or that writers only call the method one at a time due to
    // some other mechanism, such as a mutex. Also, loads done
    // after `begin_write` won't be moved before `begin_write`,
    // but `begin_unsync_write` doesn't prevent that.
    void begin_unsync_write() noexcept {
        check_ret((seq.load() & 1) == 0,); // Check that there isn't anyone writing.
        seq.store(seq.load(std::memory_order_relaxed) + 1, // Increment `seq`.
                  std::memory_order_relaxed);
        std::atomic_thread_fence(std::memory_order_release); // store-store
    }

    bool try_write() noexcept {
        unsigned s = seq.load(std::memory_order_acquire);
        return ((s & 1) == 0 &&
                seq.compare_exchange_weak(s, s + 1,
                                          std::memory_order_acquire,
                                          std::memory_order_relaxed));
    }

    // This method synchronizes writers using a CAS loop.
    void begin_write() noexcept {
        while (!likely(try_write())) {
            while (seq.load(std::memory_order_relaxed) & 1) _mm_pause();
        }
    }

    void finish_write() noexcept {
        check_ret(seq.load() & 1,);
        seq.store(seq.load(std::memory_order_relaxed) + 1, // Increment `seq`.
                  std::memory_order_release);
    }
};

template <typename T, unsigned buf_len>
class alignas(cache_line_size) SeqlockBuf {
    // Note: this class doesn't initialize the data in the buffer
    // unless it has a default constructor that does so. If it
    // doesn't, anything using this class must ensure that the first
    // read occurs after the first write.

    static_assert(buf_len > 0, "");

    struct Elem {
        T data; // sizeof(T) >= cache_line_size for all current uses, so
                // Elem doesn't need to be aligned to a cache line.
        std::atomic<unsigned> seq;
    };
    Elem buf[buf_len];
    alignas(cache_line_size) std::atomic<unsigned> cur{0}; // Index of current data

	
public:

	SeqlockBuf() {}
	
    inline unsigned get_cur_idx() const noexcept {
        return cur.load(std::memory_order_relaxed);
    }

    // Use this to read if there are definitely no writers or to read
    // the current data while writing new data (on the same thread).
    // DON'T `check(!is_write())` here.
    inline const T * raw_read() const noexcept { return &buf[get_cur_idx()].data; }

    // Use this to write if there are definitely no readers.
    inline T * raw_write() noexcept { return &buf[get_cur_idx()].data; }

    const T * begin_read(unsigned &read_idx, unsigned &read_seq) const noexcept {
        read_idx = cur.load(std::memory_order_consume);
        unsigned s;
        while (unlikely((s = buf[read_idx].seq.load(std::memory_order_relaxed)) & 1)) {
            _mm_pause();
        }
        std::atomic_thread_fence(std::memory_order_acquire);
        read_seq = s;
        return &buf[read_idx].data;
    }

    bool finish_read(unsigned read_idx, unsigned read_seq) const noexcept {
        check_ret(read_idx < buf_len, false); // TODO: Should we return true or false here?
        check_ret((read_seq & 1) == 0, false);
        std::atomic_thread_fence(std::memory_order_acquire); // load-load
        // If C++ allowed `load(release)`, that could be used
        // here instead of the load-load fence.
        return buf[read_idx].seq.load(std::memory_order_relaxed) == read_seq;
    }

    template <typename Func>
    void read(Func f) const noexcept {
        unsigned read_idx, read_seq;
        do {
            const T *p = begin_read(read_idx, read_seq);
            f(p);
        } while (!likely(finish_read(read_idx, read_seq)));
    }

    T * begin_unsync_write(unsigned &write_idx) noexcept {

        write_idx = (cur.load(std::memory_order_consume) + 1) % buf_len;

        // Check that no other threads are writing.
        check_ret((buf[write_idx].seq.load() & 1) == 0,);

        // Increment `seq`.
        buf[write_idx].seq.store(buf[write_idx].seq.load(std::memory_order_relaxed) + 1,
                                 std::memory_order_relaxed);
        std::atomic_thread_fence(std::memory_order_release);

        return &buf[write_idx].data;
    }

private:
    bool try_inc_seq(unsigned i) noexcept {
        unsigned s = buf[i].seq.load(std::memory_order_relaxed);
        return (s & 1) == 0 &&
               buf[i].seq.compare_exchange_strong(s, s + 1,
                                                  std::memory_order_acquire,
                                                  std::memory_order_relaxed);
    }
public:

    T * begin_write(unsigned &write_idx) noexcept {
        while (true) {
            // Can `cur.load(consume)` be moved outside this loop?
            unsigned i = cur.load(std::memory_order_consume);
            i = (i + 1) % buf_len;
            if (likely(try_inc_seq(i))) return &buf[write_idx = i].data;
            while (buf[i].seq.load(std::memory_order_relaxed) & 1) _mm_pause();
        }
    }

    T * try_write(unsigned &write_idx) noexcept {
        const unsigned i = (cur.load(std::memory_order_consume) + 1) % buf_len;
        return likely(try_inc_seq(i)) ? &buf[write_idx = i].data : nullptr;
    }

    void finish_write(unsigned write_idx) noexcept {
        check_ret(buf[write_idx].seq.load() & 1,);
        // If a thread gets to this point (and is using the SeqlockBuf correctly),
        // it means that it was the last to change `buf[write_idx].seq`, either using
        // a plain store or a `compare_exchange`, so `seq.load()` must return
        // the newest value regardless of the memory order passed to it.
        // This is intuitive from a hardware viewpoint: if a processor core writes
        // a value to a variable and then reads the variable before any other cores
        // write a different value, then it would see the value it wrote regardless
        // of synchronization with other cores.
        buf[write_idx].seq.store(buf[write_idx].seq.load(std::memory_order_relaxed) + 1,
                                 std::memory_order_release);
        cur.store(write_idx, std::memory_order_release);
    }

    void cancel_write(unsigned write_idx) noexcept {
        check_ret(buf[write_idx].seq.load() & 1,);
        buf[write_idx].seq.store(buf[write_idx].seq.load(std::memory_order_relaxed) + 1,
                                 std::memory_order_relaxed);
        // This store to `buf[write_idx].seq` can be relaxed because after calling
        // `cancel_write`, `buf[write_idx].data` either wasn't written to or is
        // invalid and won't be read from (`cur` won't be changed) until another
        // write (that isn't cancelled) takes place by calling either `try_write`,
        // `begin_write` or `begin_unsync_write`.
        // If `begin_unsync_write` is called, there is either only one writer or
        // some external synchronization, e.g. a mutex is used, in which case any
        // memory order will work. `begin_write` and `try_write` test whether `seq`
        // is even before beginning writing with a release operation, and how
        // quickly a store becomes visible to other threads isn't affected by its
        // memory order, so `release` is unnecessary in all cases.
    }
};
