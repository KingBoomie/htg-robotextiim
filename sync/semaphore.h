#pragma once

#ifdef __linux__

#include <semaphore.h>
#include <errno.h>

class Semaphore { // Adapted from https://github.com/preshing/cpp11-on-multicore

    sem_t m_sema;

    Semaphore(const Semaphore& other) = delete;
    Semaphore& operator=(const Semaphore& other) = delete;

public:

    Semaphore(unsigned initial_count = 0) noexcept { sem_init(&m_sema, 0, initial_count); }
    ~Semaphore() noexcept { sem_destroy(&m_sema); }

    void wait() noexcept {
        // stackoverflow.com/questions/2013181/gdb-causes-sem-wait-to-fail-with-eintr-error
        int rc;
        do rc = sem_wait(&m_sema); while (rc == -1 && errno == EINTR);
    }

    void notify() noexcept { sem_post(&m_sema); }
    void notify(int count) noexcept { while (--count >= 0) sem_post(&m_sema); }
};

#else

class Semaphore {

    unsigned count;
    std::mutex mutex;
    std::condition_variable cv;

    Semaphore(const Semaphore&) = delete;
    Semaphore& operator=(const Semaphore&) = delete;

public:

    explicit Semaphore(unsigned initial_count = 0) noexcept : count{initial_count} {}

    void notify() noexcept {
        mutex.lock();
        ++count;
        mutex.unlock();
        cv.notify_one();
    }

    void wait() noexcept {
        std::unique_lock<std::mutex> lock{mutex};
        cv.wait(lock, [&]{ return count > 0; });
        --count;
    }
};

#endif // __linux__

class LightweightSemaphore {

    std::atomic<int> m_count;
    Semaphore m_sema;

    void wait_with_partial_spinning() noexcept {
        int old_count;
        int spin = 10000;
        while (--spin) {
            old_count = m_count.load(std::memory_order_relaxed);
            if (old_count > 0 &&
                m_count.compare_exchange_strong(old_count, old_count - 1,
                                                std::memory_order_acquire)) return;
            // Prevent the compiler from collapsing the loop.
            std::atomic_signal_fence(std::memory_order_acquire);
        }
        old_count = m_count.fetch_sub(1, std::memory_order_acquire);
        if (old_count <= 0) m_sema.wait();
    }

public:

    LightweightSemaphore(unsigned initial_count = 0) noexcept :
        m_count(static_cast<int>(initial_count)) {}

    bool try_wait() noexcept {
        int old_count = m_count.load(std::memory_order_relaxed);
        return (old_count > 0 &&
                m_count.compare_exchange_strong(old_count, old_count - 1,
                                                std::memory_order_acquire));
    }

    void wait() noexcept { if (!try_wait()) wait_with_partial_spinning(); }

    void notify(int count = 1) noexcept {
        int old_count = m_count.fetch_add(count, std::memory_order_release);
        int to_release = -old_count < count ? -old_count : count;
        if (to_release > 0) m_sema.notify(to_release);
    }
};
