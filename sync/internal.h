#pragma once

#include <atomic>
#include <emmintrin.h> // TODO: Do we need to check that the platform
                       // is x86 before including this header?
// #if (emmintrin.h isn't available)
// # define _mm_pause()
// #endif

#include "common/constants.h" // cache_line_size

// Some things to keep in mind when reading concurrent lockfree code:

// Memory orders do NOT affect how QUICKLY writes become visible,
// only in what ORDER they become visible (hence the name). However,
// to enforce the memory ordering, a processor may have to wait for a
// later store before using the results of earlier stores, which has
// an effect similar to them actually becoming visible more slowly.

// The memory order of a load or store only affects OTHER loads
// and stores in relation to it, not that load or store itself.

// We can't directly control what order instructions are executed in
// by other threads - that's up to the scheduler - only in what order
// instructions are executed in this thread. To order several threads,
// we test a condition known to occur after some other operations with
// an instruction in this thread known to occur before others.

// Writes immediately become visible due to cache coherence protocols
// (e.g. MESI) once they reach any core's cache (after the instruction
// doing the write has finished and the store buffer has been flushed),
// so a write is visible to either only one core or all cores.
