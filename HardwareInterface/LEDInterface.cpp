/* 
 * File:   LEDInterface.cpp
 * Author: terminaator
 * 
 * Created on October 26, 2015, 6:10 PM
 */

#include "LEDInterface.h"


void LEDInterface::update_LED(uint8_t led_values_to_set) {
    hardware->write_command("0:led" + std::to_string(led_values_to_set));
}

LEDInterface::LEDInterface (std::shared_ptr<HardwareInterface> hardwareInterface) : hardware(hardwareInterface) {

    set_LED(0, Color_mask::GREEN);
    set_LED(1, Color_mask::GREEN);

}

void LEDInterface::set_LED(int led, Color_mask mask) {
    //					 
    //	led_values&~(0b111<<led*3)) - reset only currently changed values
    //	|(mask << led * 3) - set new values for them
    led_values = (led_values&~(0b111 << led * 3)) | (mask << led * 3); // each led 
    update_LED(led_values);
}
