#pragma once
#include <iostream>
#include <memory>
#include <thread>
#include <mutex>
#include <functional>
#include <iostream>
#include <chrono>
#include <vector>
#include <cstdio>
#include <string>
#include <cmath>
#include <thread>

#include "config.h"
#ifdef _WIN32
#define _WIN32_WINNT 0x0602
#endif //_WIN32

#include "HardwareInterface.h"
#include "gamepad/Gamepad.h"

// Interfaces for controlling hardware
#include "MotorInterface.h"
#include "CoilgunAndDribblerInterface.h"
#include "LEDInterface.h"
#include "config.h"

// Includes from common headers
#include "common/keep_going.h"
#include "common/log.h"

template <typename T1, typename T2, typename T3>
bool value_close_to (T1 value, T2 check, T3 possible_error){
    auto delta = std::abs(value - check);
    if (delta < possible_error){
        return true;
    }
    return false;
}

void onButtonDown(struct Gamepad_device * device, unsigned int buttonID, double timestamp, void * context) {
	log_info("Button " + std::to_string(buttonID) + " down");
}

void onAxisMoved(struct Gamepad_device * device, unsigned int axisID, float value, float lastValue, double timestamp, void * context) {
	log_info("Axis " + tostr(axisID) + " moved to " + tostr(lastValue));
}

static void init_gamepad(const bool debug = false){
	if (debug){
		Gamepad_buttonDownFunc(onButtonDown, (void *) 0x3);
		Gamepad_axisMoveFunc(onAxisMoved, (void*) 0x4);
	}
	Gamepad_init();
}

static bool kick_force_fix = 1;

// Number is originally in range [-1, 1], we convert it to range [0, 1]
template <typename T>
inline T normalized_to_positive(T num){
	return (num + 1.0) / 2.0;
}

inline auto kick_ball(float strength,const std::shared_ptr<CoilgunAndDribblerInterface> & coilgunAndDribblerIO){
		// Strength is originally in range [-1, 1], we convert it to range [0, 1]
        strength = normalized_to_positive(strength);
        //When the program is started, all input values will be the default ones.
        //These will not change until a certain paddle is moved. The default value
        //used for kicking defaults to half power. To save the coilgun from
        //destroying itself, this value will be reverted to minimum power.
        if(kick_force_fix  &&  strength == 0.5){
            log_warning("Changing force from 0.5 to 0");
            strength = 0;
        }else if(kick_force_fix){
            kick_force_fix = 0;
        }
        strength = std::pow(strength, 3) * (1-0.05) + 0.05;
        coilgunAndDribblerIO->kick(strength);
}
inline auto set_dribbler_speed(float speed, const std::shared_ptr<CoilgunAndDribblerInterface>& coilgunAndDribblerIO){
		// Speed is originally in range [-1, 1], we convert it to range [0, 1]
        speed = normalized_to_positive(speed);
        //When the program is started, all input values will be the default ones.
        //These will not change until a certain paddle is moved. The default value
        //used for kicking defaults to half power. To save the coilgun from
        //destroying itself, this value will be reverted to minimum power.
        if(kick_force_fix  &&  speed == 0.5){
            log_warning("Changing force from 0.5 to 0");
            speed = 0;
        }else if(kick_force_fix){
            kick_force_fix = 0;
        }
        
        coilgunAndDribblerIO->set_dribbler_speed(speed);
}

inline auto apply_deadzones(std::vector<float>& axis_values){
    
}

//TODO maybe this shouldn't be global
static double motor_speed_multiplier = 1;

bool update_gamepad(const std::shared_ptr<MotorInterface>& motorIO, const std::shared_ptr<CoilgunAndDribblerInterface>& coilgunIO) {
    unsigned int gamepadIndex;
    struct Gamepad_device * device;
	const unsigned iterations_to_next_poll = 30;
    unsigned int iterationsToNextPoll = iterations_to_next_poll;

    iterationsToNextPoll--;
    if (iterationsToNextPoll == 0) {
        Gamepad_detectDevices();
        iterationsToNextPoll = iterations_to_next_poll;
    }
	// Update data from controller
    Gamepad_processEvents();

	// Check if we have any gamepads before doing anything
    if (Gamepad_numDevices() < 1) {
		log_warning("No gamepad found");
        return false;
    }
	
    // Look for xbox360 gamepad
    for (gamepadIndex = 0; gamepadIndex < Gamepad_numDevices(); gamepadIndex++) {
        device = Gamepad_deviceAtIndex(gamepadIndex);

        if (device->numAxes != 6 ||
                device->numButtons != 15) {
            continue;
        }
    }
    
    // BUTTONS
    // Y button will shoot with strength from right bumper axis
    if (device->buttonStates[config::button_Y]){
		kick_ball(device->axisStates[4], coilgunIO);
    }
    // BUTTONS
    // B button will set new dribbler speed
    if (device->buttonStates[config::button_B]){
        set_dribbler_speed(device->axisStates[4], coilgunIO);
    }
    // BUTTONS
    // Right shoulder button will set new motor speed multiplier
    if(device->buttonStates[config::button_RB]){
        motor_speed_multiplier = 1.0 - normalized_to_positive(device->axisStates[5]);
		log_info("Motor speed multiplier: " + std::to_string(motor_speed_multiplier));
    }
    // AXES
    float* axes = device->axisStates;
    // the first 4 axes are x and y of analogsticks, the last 2 are bumper axes
    std::vector<float> axis_values(axes, axes + 4);
	
	// Make sure that axis values are not inside deadzones
	//apply_deadzones(axis_values);
	const float deadzone = 0.2;
    for (auto& axis : axis_values) {
        // apply deadzones to each axis
        if (value_close_to(axis, 0, deadzone)) {
            axis = 0;
        } else {
            // Make axis respond in the full range [-1, 1], instead of [-1, -0.2] U [0.2, 1] 
            axis = (axis + (axis > 0 ? -deadzone : deadzone)) / (1.0-deadzone);
            // Make axis speed depend on left bumper axis
            axis *= 1.0 - normalized_to_positive(axes[5]);
            // ... and motor speed multiplier also set by the left bumper
            axis *= motor_speed_multiplier;
            // ... and left shoulder button, just to add a little bit more precision to the controls
            if(device->buttonStates[config::button_LB]){
                axis *= 0.1;
            }
        }
    }
	using Robot_vector = MotorInterface::Robot_vector;
	Robot_vector speeds (axis_values[0], axis_values[1], axis_values[2]);
	log_info ("X: " + tostr(axis_values[0]) + " Y:\t" + tostr(axis_values[1]));
    motorIO->set_euclidean_speeds(speeds);
    return true;
}


void control(void) {
    
	init_gamepad();
	
    auto p_hardwareIO = std::make_shared<HardwareInterface>();

    if (p_hardwareIO->is_connection_open()){
        log_info("Have connection to hardware");
    }else {
        log_critical("Coudn't connect to hardware /dev/USB0");
        return;
    }
    
    auto p_motorIO = std::make_shared<MotorInterface> (p_hardwareIO);
    auto p_coilgunIO = std::make_shared<CoilgunAndDribblerInterface> (p_hardwareIO);
	
    while(keep_going()){
        update_gamepad(p_motorIO, p_coilgunIO);
		using namespace std::literals;
        std::this_thread::sleep_for(100ms);
		
	}
    return;
   
}
