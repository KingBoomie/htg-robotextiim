 #pragma once
#include <array>

namespace config {
	
	// Connection data
	static const unsigned int baudrate = 19200;
	/** All others are default and unnecessary
	const int parity = 0;	  // none
	const int csize = 8;
	const int flowcontrol = 0; //none
	const int stopbits = 1;
	*/
	static const unsigned int button_A = 0;
	static const unsigned int button_B = 1;
	static const unsigned int button_X = 2;
	static const unsigned int button_Y = 3;
	static const unsigned int button_LB = 4;
	static const unsigned int button_RB = 5;
	static const unsigned int button_back = 6;
	static const unsigned int button_start = 7;
	static const unsigned int button_center = 8;
	
	
	static const double motor_diameter = 0.11;
	static const double PI = 3.14159265359;
};



