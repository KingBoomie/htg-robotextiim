#pragma once
#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <system_error>
#include <cmath>
#include <atomic>

#include "asio-1.11.0/asyncSerial/AsyncSerial.h"

//TODO make it a singleton
class HardwareInterface {

public:

	HardwareInterface (std::string serialname = "/dev/ttyUSB0");
	~HardwareInterface ();
	
	void write_command (std::string cmd);
	bool is_connection_open ();

private:
	CallbackAsyncSerial hardware_connection;
	
	void connect (std::string serialname);

};
