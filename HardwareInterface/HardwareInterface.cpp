#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <system_error>
#include <cmath>
#include <atomic>

#include "HardwareInterface.h"
#include "config.h"

#include "common/log.h"

HardwareInterface::HardwareInterface(std::string serialname) {
    connect(serialname);
}

HardwareInterface::~HardwareInterface() {
    hardware_connection.close();
}

void HardwareInterface::connect(std::string serialname) {
    while (!hardware_connection.isOpen()) {
        std::string response;
        hardware_connection.setCallback([](const char * str, size_t size) ->void {
            std::cout << "Received a message: " << std::string(str, size) << "\n";
        });
        try {
            hardware_connection.open(serialname, config::baudrate);
        } catch (std::exception& e) {
            log_warning("Exception opening serial: " + std::string(e.what()));
            return;
        }
    }
}

void HardwareInterface::write_command(std::string cmd) {
    auto write_string = cmd + "\n"; 
    hardware_connection.writeString(write_string);
}

bool HardwareInterface::is_connection_open() {
    return hardware_connection.isOpen();
}



