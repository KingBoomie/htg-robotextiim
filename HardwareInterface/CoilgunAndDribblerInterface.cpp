/* 
 * File:   CoilgunInterface.cpp
 * Author: terminaator
 * 
 * Created on October 26, 2015, 6:34 PM
 */

#include "CoilgunAndDribblerInterface.h"
#include "common/log.h"
#include <chrono>

static std::string dribbler_speed_str = "0";

void CoilgunAndDribblerInterface::keep_alive() {
    hardware->write_command("0:p");
    hardware->write_command("5:s" + dribbler_speed_str);
}

CoilgunAndDribblerInterface::CoilgunAndDribblerInterface(std::shared_ptr<HardwareInterface> hardwareInterface) : hardware(hardwareInterface) {

    // Start charging coilgun
    charge();

    keep_alive_thread = std::thread([&]() {
        while (local_keep_going.load() /* && keep_going()*/) {
            keep_alive();
            // Keep alive signal needs to be sent every 780ms. Since we are
            // using Async IO the message might be sent a bit later then the
            // function call, so we need to call it a bit before that. 
            // The call to keep_alive resets the counter on the hardware,
            // which passed starts discharging the coilgun. 

            //using namespace std::literals;
            std::this_thread::sleep_for(std::chrono::duration<int, std::milli> (100));
        }
    });
}

/*
    COILGUN
 */
CoilgunAndDribblerInterface::~CoilgunAndDribblerInterface() {
    local_keep_going.store(false);
    keep_alive_thread.join();
}

void CoilgunAndDribblerInterface::charge() {
    log_info("Coilgun charge");
    hardware->write_command("0:c");
}

void CoilgunAndDribblerInterface::discharge() {
    log_info("Coilgun discharge");
    hardware->write_command("0:d");
}

void CoilgunAndDribblerInterface::kick(double force) {
    log_info("Coilgun kick: " + std::to_string(force));
    
    assert(force >= 0.0);
    assert(force <= 1.0);
    int real_force = (int) (force * 2000.0);
    hardware->write_command("0:k" + std::to_string(real_force));
}

void CoilgunAndDribblerInterface::set_failsafe(bool on_or_off) {
    if (on_or_off){
        log_info("Coilgun failsafe: true");
    }else{
        log_warning("Coilgun failsafe: false");
    }
    
    hardware->write_command("0:fs" + std::to_string(on_or_off));
}

void CoilgunAndDribblerInterface::set_autocharge(bool on_or_off) {
    log_info("Coilgun autocharge: " + std::to_string(on_or_off));
    hardware->write_command("0:ac" + std::to_string(on_or_off));
}

/*
   DRIBBLER
 */


void CoilgunAndDribblerInterface::set_dribbler_speed(float speed) {
    log_info("Dribbler speed: " + std::to_string(speed));
    
    if (speed > 1.0 && speed < 1.1) {
        speed = 1.0;
    } else if (speed < -1.0 && speed > -1.1) {
        speed = -1.0;
    }

    assert(!(speed > 1.0));
    assert(!(speed < -1.0));

    dribbler_speed = speed*16.0;
    dribbler_speed_str = std::to_string(dribbler_speed);
    dribbler_speed_str.replace(dribbler_speed_str.find(','), 1, ".");
    
    
}