/* 
 * File:   CoilgunInterface.h
 * Author: terminaator
 *
 * Created on October 26, 2015, 6:34 PM
 */

#ifndef COILGUNINTERFACE_H
#define	COILGUNINTERFACE_H

#include <thread>
#include <atomic>
#include <memory>

#include "HardwareInterface.h"

class CoilgunAndDribblerInterface{

public:
	CoilgunAndDribblerInterface (const CoilgunAndDribblerInterface& orig) = delete;
	CoilgunAndDribblerInterface(std::shared_ptr<HardwareInterface> hardwareInterface);
	~CoilgunAndDribblerInterface();
	void charge ();
	
	void discharge ();	
	void kick (double force);

	void set_failsafe (bool on_or_off);
	
	void set_autocharge (bool on_or_off);
	void set_dribbler_speed(float speed);
private:
	void keep_alive();
	
	std::thread keep_alive_thread;
	std::atomic<bool> local_keep_going {true};
	std::shared_ptr<HardwareInterface> hardware;
	double dribbler_speed = 0.0;
};


#endif	/* COILGUNINTERFACE_H */

