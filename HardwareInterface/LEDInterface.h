/* 
 * File:   LEDInterface.h
 * Author: terminaator
 *
 * Created on October 26, 2015, 6:10 PM
 */

#ifndef LEDINTERFACE_H
#define	LEDINTERFACE_H

#include <memory>
#include "HardwareInterface.h"


class LEDInterface {
	
	uint16_t led_values;
	std::shared_ptr<HardwareInterface> hardware;
	
	void update_LED (uint8_t led_values_to_set);
	
public:
	
	enum Color_mask {

		RED = 1, GREEN = 2, BLUE = 4
	};
	
	LEDInterface(std::shared_ptr<HardwareInterface> hardwareInterface);
	
	LEDInterface (const LEDInterface& orig) = delete;
	// Usage:
	//		set_LED(0, Color_mask::RED || Color_mask::BLUE)
	void set_LED (int led, Color_mask mask);
};

#endif	/* LEDINTERFACE_H */

