#include "MotorInterface.h"


void MotorInterface::set_speed(int motor, double speed) {
    if (speed > 1.0 && speed < 1.1) {
        speed = 1.0;
    } else if (speed < -1.0 && speed > -1.1) {
        speed = -1.0;
    }

    assert(!(speed > 1.0));
    assert(!(speed < -1.0));
    assert(!(motor < 1));
    assert(!(motor > 4));

    int realSpeed = (int) (speed * 190.0);
    
    hardware->write_command(std::to_string(motor) + ":sd" + std::to_string(realSpeed));
}

void MotorInterface::set_speeds(double m1, double m2, double m3, double m4) {
    set_speed(1, m1);
    set_speed(2, m2);
    set_speed(3, m3);
    set_speed(4, m4);
}


void MotorInterface::set_euclidean_speeds(Robot_vector euclidean_speeds) {
    Motor_speeds ms = localizer.to_motor_speeds(euclidean_speeds);
    
    // Normalize motor speeds
    auto max = ms[0];
    for (int i = 0; i < 4; ++i) {
        if (max < std::abs(ms[i])) {
            max = std::abs(ms[i]);
        }
    }

    if (max > 1.0) {
        double scale = std::abs(1 / max);
        for (int i = 0; i < 4; ++i) {
            ms[i] *= scale;
        }
    } // Finish normalization

    set_speeds(ms[0], ms[1], ms[2], ms[3]);
}

