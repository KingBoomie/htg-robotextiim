/* 
 * File:   MotorInterface.h
 * Author: Kristjan Laht
 *
 * Created on October 25, 2015, 4:49 AM
 */
#pragma once

#include "HardwareInterface.h"
#include "../localizer/MotorLocalizer.h"
#include "../Lib/Eigen/Dense"

#include <memory> // std::shared_ptr

class MotorInterface {
	std::shared_ptr<HardwareInterface> hardware;
	void set_speed(int motor, double speed);
	void set_speeds(double m1, double m2, double m3, double m4);
	MotorLocalizer localizer;
public:
	typedef Eigen::Vector3d Robot_vector;
	typedef Eigen::Vector4d Motor_speeds;
	MotorInterface(std::shared_ptr<HardwareInterface> hardwareInterface) : hardware(hardwareInterface) {};
			   
	MotorInterface (const MotorInterface& orig) = delete;
	
	void set_euclidean_speeds (Robot_vector euclidean_speeds);

};

