#include <iostream>
#include "../Lib/Eigen/Dense"

#include "MotorLocalizer.h"

int main_old3 () {
	// motor angles
	const double m_a[] = { 45, 135, 225, 315 };

	MotorLocalizer motor_localizer(m_a);

	Eigen::IOFormat RowFormat (4, Eigen::DontAlignCols, ", ", "", "","" ,"[", "]");

	Eigen::Vector4d motor_speeds = { 1, 1, 1, 1 };
	Eigen::Vector3d eucleidean_speeds = { 1, 0, 1 };

	std::cout << "Motor speeds " << motor_speeds.format(RowFormat) <<
		" translates to: " << motor_localizer.to_euclidean_speeds (motor_speeds).format(RowFormat) << std::endl;
	std::cout << "Eucleidean speeds " << eucleidean_speeds.format (RowFormat) <<
		" translates to: " << motor_localizer.to_motor_speeds (eucleidean_speeds).format (RowFormat) << std::endl;

	return 0;
}