#pragma once

#include "../Lib/Eigen/Dense"

class MotorLocalizer
{
	double motor_angles[4];

public:
	// * Returns Vector3d (x_speed, y_speed, omega)
	// * Parameter motor_speeds is a Vector4d (motor1_speed, motor2_speed, motor3_speed, motor4_speed)
	// * all speeds are in m/s
	// TODO: maybe it is better to do speed calculations in some other unit
	Eigen::Vector3d to_euclidean_speeds (const Eigen::VectorXd motor_speeds);

	// * Returns Vector4d (motor1_speed, motor2_speed, motor3_speed, motor4_speed)
	// * Parameter euclidean_speeds aka real world speeds and angle
	//   Vector3d (x_speed, y_speed, omega)
	// * all speeds are in m/s, even omega
	Eigen::Vector4d to_motor_speeds (const Eigen::VectorXd euclidean_speeds);

	MotorLocalizer();
};
