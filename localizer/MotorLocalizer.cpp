#include "MotorLocalizer.h"
#include "../HardwareInterface/config.h"

// TODO: move this into some common header
inline double to_rad (int deg) {
	return double(deg) * 0.01745329;
}

// this is fix for bug 257 in Eigen
//  * http://eigen.tuxfamily.org/bz/show_bug.cgi?id=257#c14
//  * This algorithm calculates Moore-Penrose pseudoinverse of a matrix
//  * if someone reading this has a really good understanding , feel free to
//    explain it to Kristjan
template<typename _Matrix_Type_>
_Matrix_Type_ pseudo_inverse (const _Matrix_Type_ &a, double epsilon = std::numeric_limits<double>::epsilon ()) {
	Eigen::JacobiSVD< _Matrix_Type_ > svd (a, Eigen::ComputeThinU | Eigen::ComputeThinV);
	double tolerance = epsilon * std::max (a.cols (), a.rows ()) *svd.singularValues ().array ().abs ()(0);
	return svd.matrixV () *  (svd.singularValues ().array ().abs () > tolerance).select (svd.singularValues ().array ().inverse (), 0).matrix ().asDiagonal () * svd.matrixU ().adjoint ();
}

Eigen::MatrixXd inverse_velocity_coup(3, 4);

Eigen::Vector3d MotorLocalizer::to_euclidean_speeds(const Eigen::VectorXd motor_speeds) {
	return inverse_velocity_coup * motor_speeds;
}

Eigen::MatrixXd velocity_coup (4, 3);

Eigen::Vector4d MotorLocalizer::to_motor_speeds(const Eigen::VectorXd euclidean_speeds) {
	return velocity_coup * euclidean_speeds;
}

MotorLocalizer::MotorLocalizer() {
    const int motor_angles_in_deg[] = {45, 135, 225, 315};
    // convert angles to radians
    for (int i = 0; i < 4; ++i) {
        motor_angles[i] = to_rad(motor_angles_in_deg[i]);
    }
    
    // coefficient is for converting between motorspeeds and real world speeds, it's equal to perimeter * rotations per second
    double motor_coefficient = config::motor_diameter * config::PI * 62.5 / 18.75 / 64;
    
    // initialize velocity coupling
    velocity_coup << cos(motor_angles[0]), sin(motor_angles[0]), 1,
                     cos(motor_angles[1]), sin(motor_angles[1]), 1,
                     cos(motor_angles[2]), sin(motor_angles[2]), 1,
                     cos(motor_angles[3]), sin(motor_angles[3]), 1;

    velocity_coup *= 1 / motor_coefficient;
    // initialize pseudoinverse velocity coupling
    inverse_velocity_coup = pseudo_inverse(velocity_coup);
}
