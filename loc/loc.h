#pragma once

#include "common/vec2.h"

Vec2d  get_robot_pos(void) noexcept;
double get_robot_dir(void) noexcept;

// This returns the unit vector (cos(robot_dir), sin(robot_dir)), but
// calling this function might be more efficient than calculating the
// cosine and sine yourself.
Vec2d get_robot_dir_vec(void) noexcept;

bool update_robot_loc(void) noexcept;