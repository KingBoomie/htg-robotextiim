#include "common/constants.h"
#include "common/vec2.h"
#include "common/rel_pos.h"

#include "find/gate.h"
#include "loc/loc.h"

#include <cmath>

template<typename A, typename B, typename C, typename Res>
static bool solve_quadratic_equation(A a, B b, C c, Res& result1, Res& result2, Res eps = 0.000001) {
    //https://people.csail.mit.edu/bkph/articles/Quadratics.pdf
    check_ret(a != 0, false);
    auto determinant = b * b - 4.0 * a*c;
    if (determinant < 0) {
        if (determinant > -eps) {
            determinant = 0;
        } else {
            return false;
        }
    }
    const auto s = sqrt(determinant);
    if (b >= 0) {
        const auto numerator1 = -b - s;
        if (numerator1 == 0) {
            result1 = result2 = 0;
            return true;
        }
        result1 = numerator1 / (2.0 * a);
        result2 = (2.0 * c) / numerator1;
    } else {
        const auto numerator1 = -b + s;
        if (numerator1 == 0) {
            result1 = result2 = 0;
            return true;
        }
        result1 = numerator1 / (2.0 * a);
        result2 = (2.0 * c) / numerator1;
    }
    return true;
}

static constexpr double sqrt_2 = 1.414213562373095048801;

static const Vec2d start_pos = {field_width - robot_rad, robot_rad};
static const double start_dir = angle_per_circle * 135.0 / 360.0;
static const Vec2d start_dir_vec = {-sqrt_2, sqrt_2};

const Vec2d opnt_gate_abs_pos{field_width * 0.5, field_len - gate_depth * 0.5},
our_gate_abs_pos{field_width * 0.5, gate_depth * 0.5};

static bool calc_robot_pos(RelPos opnt_gate_pos,
        RelPos our_gate_pos,
        double &robot_x1,
        double &robot_x2,
        double &robot_y) noexcept {
    // (Rx - Ux)**2 + (Ry - Uy)**2 = u**2
    // (Rx - Px)**2 + (Ry - Py)**2 = p**2

    auto Px = opnt_gate_abs_pos.x;
    auto Py = opnt_gate_abs_pos.y;
    auto Ux = our_gate_abs_pos.x;
    auto Uy = our_gate_abs_pos.y;

    auto u = our_gate_pos.dist;
    auto p = opnt_gate_pos.dist;

    auto sqr_U = Ux * Ux + Uy * Uy;
    auto sqr_P = Px * Px + Py * Py;

    auto c2 = (sqr_P - sqr_U + u * u - p * p) * 0.5;
    auto ry = c2 / (Py - Uy);

    auto xb = -2 * Ux;
    auto xc = sqr_U - u * u + ry * ry - 2 * Uy*ry;
    double rx1, rx2;
    if (!likely(solve_quadratic_equation(1.0, xb, xc, rx1, rx2))) return false;

    robot_x1 = rx1;
    robot_x2 = rx2;
    robot_y = ry;
    return true;
}

static double robot_dir_by_gate(double rx, double ry,
        Vec2d gate_abs_pos, double gate_dir) {
    const Vec2d to_gate{gate_abs_pos - Vec2d(rx, ry)};
    return to_rel(to_gate).dir - gate_dir;
}

static double calc_robot_dir(double opnt_gate_dir, double our_gate_dir,
        long opnt_ts, long our_ts,
        double rx, double ry) noexcept {
    const long eps = 50000; // 50ms
    const long diff = opnt_ts - our_ts;
    if (diff > eps || diff < -eps) {
        if (diff < 0) {
            return robot_dir_by_gate(rx, ry, opnt_gate_abs_pos, opnt_gate_dir);
        } else {
            return robot_dir_by_gate(rx, ry, our_gate_abs_pos, our_gate_dir);
        }
    }
    const double pos_by_opnt = robot_dir_by_gate(rx, ry,
            opnt_gate_abs_pos,
            opnt_gate_dir);
    const double pos_by_our = robot_dir_by_gate(rx, ry,
            our_gate_abs_pos,
            our_gate_dir);
    return (pos_by_opnt + pos_by_our) * 0.5;
}

static bool calc_robot_loc(RelPos opnt_gate, RelPos our_gate,
        long opnt_ts, long our_ts,
        double &robot_x, double &robot_y, double &robot_dir
        ) noexcept {
    // Calc x1, x2, y.
    double robot_x1, robot_x2;
    if (!likely(calc_robot_pos(opnt_gate, our_gate,
            robot_x1, robot_x2, robot_y))) {
        return false;
    }

    const double d = opnt_gate.dir - our_gate.dir;
    if (d < 0.0 || d > angle_per_circle * 0.5) {
        robot_x = std::min(robot_x1, robot_x2);
    } else {
        robot_x = std::max(robot_x1, robot_x2);
    }

    // Calc dir.
    robot_dir = calc_robot_dir(opnt_gate.dir, our_gate.dir,
            opnt_ts, our_ts,
            robot_x, robot_y);
    return true;
}

struct AtomicLoc {
    std::atomic<double> pos_x, pos_y, dir;
    AtomicLoc() noexcept {
        pos_x.store(start_pos.x, m_o_relaxed);
        pos_y.store(start_pos.y, m_o_relaxed);
          dir.store(start_dir,   m_o_relaxed);
    }
};

static SeqlockBuf<AtomicLoc, 2> s_loc_buf;

Vec2d get_robot_pos(void) noexcept {
    unsigned read_idx, read_seq;
    Vec2d robot_pos;
    do {
        const AtomicLoc *p = s_loc_buf.begin_read(read_idx, read_seq);
        robot_pos.x = p->pos_x;
        robot_pos.y = p->pos_y;
    } while (!likely(s_loc_buf.finish_read(read_idx, read_seq)));
    return robot_pos;
}

double get_robot_dir(void) noexcept {
    unsigned read_idx, read_seq;
    Vec2d robot_dir;
    do {
        const AtomicLoc *p = s_loc_buf.begin_read(read_idx, read_seq);
        robot_dir = p->dir;
    } while (!likely(s_loc_buf.finish_read(read_idx, read_seq)));
    return robot_dir;
}

Vec2d get_robot_dir_vec(void) noexcept {
    const double robot_dir = get_robot_dir();
    return Vec2d(cos(robot_dir), sin(robot_dir));
}

bool update_robot_loc(void) noexcept {

    long our_ts, opnt_ts;
    const RelPos our_gate_pos =  get_our_gate_pos(&our_ts),
                opnt_gate_pos = get_opnt_gate_pos(&opnt_ts);

    double robot_x, robot_y, robot_dir;
    if (!likely(calc_robot_loc(opnt_gate_dir, our_gate_dir,
            opnt_ts, our_ts,
            robot_x, robot_y, robot_dir))) {
        return false;
    }

    unsigned write_idx;
    AtomicLoc *p = s_loc_buf.begin_write(write_idx);
    p->pos_x = robot_x;
    p->pos_y = robot_y;
    p->dir   = robot_dir;
    s_loc_buf.finish_write(write_idx);

    return true;
}