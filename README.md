# Häälestamine #

...ehk värvide threshold'ide valimine. Programmi käivitamisel saab valida palli värvi threshold'e, klikkides oranžikatele pikslitele (saab klikkida ka töödeldud kaadrile). Parema nuppuga klõpsates saab näidata, et piksel ei ole seda värvi. Teisi värve saab valida nuppudega või vajutades vastavalt

 * `s` (sinine) või `b` (blue)
 * `k` (kollane) või `y` (yellow)
 * `j` (valgete joonte värv) või `l` (line)
 * `v` (väljaku värv) või `f` (field)
 * `o` (oranž)

`z` või "Undo" nupp võtab tagasi viimase klikki (seda saab teha kuni 16 korda järjest).
`p` või "Pause" nupp teeb pausi - threshold'e saab muuta, aga kaader mida töödeldatakse jääb samaks. `w` vajutamine muudab threshold'ide valimise "laiust" - kas valitakse
ainult täpselt selle piksli värvi, mille peale vajutati või ka teisi sarnaseid värve.

Kui vajutada mitu korda järjest `r`-i või "Reset" nuppu teisi nuppe vahepeal vajutamata, siis tühistatakse kõik threshold'id (töödeldud kaader muutub täiesti mustaks). Seda ei saa tagasi võtta!

Programmi sulgemiseks vajuta `ESC` või "Exit" nuppu.  Sulgemisel salvestatakse threshold'id tmap faili, mida robot saab pärast kasutada.

# Käivitamine #

Programmi tuleb `sudo`ga lahti teha; muidu see ei saa mootoritega ühendust ja teeb segfault'i. Käivitamiseks:

1. `Ctrl-Alt-t` (tee lahti terminal)
2. `cd Code/Greedy/bin/Release` (liigu programmi kausta)
3. `sudo ./Greedy` [vastase värava värv]
4. Sisesta parool.

[vastase värava värv]-i asemel võib olla `kollane`, `sinine`, `yellow` või `blue`, või lihtsalt `k`, `s`, `y` või `b`, või isegi
`kfajlkj`, `sjfkld` jne - värv valitakse esimese tähe järgi, nii et nt

    sudo ./Greedy yelwo

valib kollase värvi. Kui värvi ei ole antud, valitakse see suvaliselt.

Programmi võib ohutult `Ctrl-c`ga kinni panna - seda muudetakse tavaliseks `exit`iks.
