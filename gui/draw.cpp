#include <atomic>
#include "common/log.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/constants.h"
#include "queue/readerwriterqueue.h"
#include "sync/seqlock.h"
#include "gui/internal.h"
#include "gui/draw.h"

#define order_rlx std::memory_order_relaxed

#define USE_SPINLOCKS 0
#if USE_SPINLOCKS
# include "sync/spinlock.h"
#else
# include <mutex>
#endif

struct DrawBuf {    // TODO: Better name
    moodycamel::ReaderWriterQueue<Shape> shape_queue;
    GtkWidget *show_btn;     // Button to show drawing area that
    GtkWidget *drawing_area; // appears when the drawing area is hidden
    GdkPixbuf *pixbuf;
#if USE_SPINLOCKS
    Spinlock
#else
    std::mutex
#endif
    lock;
    bool track_clicks, is_visible;
};

bool is_draw_buf_visible(DrawBuf *db) noexcept {
    db->lock.lock();
    const bool b = db->is_visible;
    db->lock.unlock();
    return b;
}

#ifndef NDEBUG
#include <thread>
#include <mutex>
#include <unordered_map>
#endif
bool draw_shape(DrawBuf *draw_buf, Shape shape) noexcept {

#ifndef NDEBUG
    check_ret(draw_buf, false);

    static std::unordered_map<DrawBuf*, std::thread::id> last_producer;
    static std::mutex map_mtx;
    const auto t = std::this_thread::get_id();
    map_mtx.lock();
    const auto p = last_producer.find(draw_buf);
    if (p == last_producer.cend()) { // If not initialized,
        last_producer[draw_buf] = t; // initialize with this thread's ID.
        map_mtx.unlock();
    } else {
        map_mtx.unlock();
        // Make sure only one thread uses this draw buf.
        check_ret(p->second == t, false);
    }
#endif

    // TODO: Should we check if `draw_buf` is visible here?
    return draw_buf->shape_queue.enqueue(shape);
}

bool begin_updating_draw_buf(DrawBuf *db) noexcept {
    db->lock.lock();
    if (!db->is_visible) {
        db->lock.unlock();
        return false; // Don't update.
    }
    return true;
}

static gboolean invalidate_draw_buf(gpointer user_data) noexcept {
    DrawBuf *db = static_cast<DrawBuf*>(user_data);
    db->lock.lock();

    // Here is the line we actually want to execute. Well, actually it
    // would be nice if we could just schedule the draw callback directly,
    // but GTK likes using events and stuff.
    gtk_widget_queue_draw(db->drawing_area);

    db->lock.unlock();
    return G_SOURCE_REMOVE;
}

void finish_updating_draw_buf(DrawBuf *db) noexcept {
    db->lock.unlock();
    // Hopefully `invalidate_draw_buf` will be called more-or-less immediately.
    gdk_threads_add_idle_full(G_PRIORITY_HIGH, invalidate_draw_buf, db, nullptr);
}

static void toggle_hide_draw_buf(DrawBuf *db) noexcept {
    db->lock.lock();
    if (db->is_visible) {
        db->is_visible = false;
        gtk_widget_hide(db->drawing_area);
        gtk_widget_show(db->show_btn);
        shrink_window_to_fit(db->show_btn);
    } else {
        gtk_widget_hide(db->show_btn);
        gtk_widget_show(db->drawing_area);
        db->is_visible = true;
    }
    db->lock.unlock();
}

static gboolean on_show_btn_press(GtkWidget *ignore, GdkEvent *ignore_e,
                                  gpointer draw_buf) noexcept {
    toggle_hide_draw_buf(static_cast<DrawBuf*>(draw_buf));
    return TRUE;
}

static gboolean on_draw_event(GtkWidget *ignore, cairo_t *cr,
                              gpointer draw_buf) noexcept {
    DrawBuf *db = static_cast<DrawBuf*>(draw_buf);

    db->lock.lock();
    gdk_cairo_set_source_pixbuf(cr, static_cast<GdkPixbuf*>(db->pixbuf), 0.0, 0.0);
    cairo_paint(cr);
    db->lock.unlock();

    while (true) {
        Shape shape;
        if (!db->shape_queue.try_dequeue(shape)) break; // No shapes left to draw
        cairo_set_source_rgb(cr, shape.r, shape.g, shape.b);
        switch (shape.type) {
        case Shape::Type::rect:
            {
                const auto r = shape.rect;
                cairo_rectangle(cr, r.x, r.y, r.width, r.height);
            }
            break;
        case Shape::Type::circle:
            {
                const auto c = shape.circle;
                cairo_arc(cr, c.x, c.y, c.radius, 0.0, angle_per_circle);
            }
            break;
        default:
            check(false && "Attempted to draw unknown type of shape.");
        }
        (shape.do_fill ? cairo_fill : cairo_stroke)(cr);
    }
    return TRUE;
}

static gboolean on_click(        GtkWidget*, GdkEvent *, gpointer) noexcept;
static gboolean on_mouse_release(GtkWidget*, GdkEvent *, gpointer) noexcept;
static gboolean on_mouse_motion( GtkWidget*, GdkEvent *, gpointer) noexcept;

static bool on_click_helper(DrawBufClick &last_click, DrawBuf *db,
                            GdkEventButton *eb) noexcept {
    report_click_to_user_input();

    DrawBufClick c;
    c.left = (eb->button == 1);
    c.ctrl  = !!(eb->state & GDK_CONTROL_MASK);
    if (c.ctrl && !c.left) {      // Ctrl + RMB click to hide buf
        toggle_hide_draw_buf(db);
        return false;
    } else if (!db->track_clicks) {
        return false;
    }
    c.alt   = !!(eb->state & GDK_MOD1_MASK);
    c.shift = !!(eb->state & GDK_SHIFT_MASK);
    c.x = static_cast<int>(eb->x);
    c.y = static_cast<int>(eb->y);
    c.draw_buf = db;

    last_click = c;
    return true;
}

static bool setup_draw_buf(DrawBuf *db, GtkWidget *box,
                           const unsigned char *buf, int width, int height,
                           const char *buf_name, bool track_clicks) noexcept {
    db->lock.lock(); // Just in case

    if (unlikely(!db->shape_queue.construct(15))) {
        db->lock.unlock();
        return false;
    }

    db->pixbuf = gdk_pixbuf_new_from_data(buf, GDK_COLORSPACE_RGB, FALSE, 8,
                                          width, height, width * 3, NULL, NULL);
    if (unlikely(!db->pixbuf)) {
        log_warning("gdk_pixbuf_new_from_data failed.");
        return false;
    }

    db->drawing_area = gtk_drawing_area_new();
    if (unlikely(!db->drawing_area)) {
        g_object_unref(static_cast<gpointer>(db->pixbuf));
        log_warning("gdk_drawing_area_new failed.");
        return false;
    }

    db->show_btn = gtk_button_new_with_label(buf_name);
    if (unlikely(!db->show_btn)) {
        g_object_unref(static_cast<gpointer>(db->pixbuf));
        gtk_widget_destroy(db->drawing_area);
        log_warning("gtk_button_new_with_label failed.");
        return false;
    }

    gtk_box_pack_start(GTK_BOX(box), db->drawing_area, FALSE, FALSE, 1);
    gtk_widget_set_size_request(db->drawing_area, width, height);
    gtk_widget_show(db->drawing_area);
    g_signal_connect_data(db->drawing_area, "draw",
                          G_CALLBACK(on_draw_event), db,
                          nullptr, static_cast<GConnectFlags>(0));

    gtk_box_pack_start(GTK_BOX(box), db->show_btn, FALSE, FALSE, 1);
    gtk_widget_show(db->show_btn);
    g_signal_connect(db->show_btn, "button-press-event",
                     G_CALLBACK(on_show_btn_press), db);

    gtk_widget_add_events(db->drawing_area, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(db->drawing_area, "button-press-event",
                     G_CALLBACK(on_click), db);

    if (track_clicks) {
        gtk_widget_add_events(db->drawing_area, GDK_BUTTON_RELEASE_MASK);
        g_signal_connect(db->drawing_area, "button-release-event",
                         G_CALLBACK(on_mouse_release), nullptr);
        gtk_widget_add_events(db->drawing_area, GDK_POINTER_MOTION_MASK);
        g_signal_connect(db->drawing_area, "motion-notify-event",
                         G_CALLBACK(on_mouse_motion), nullptr);
    }

    gtk_widget_hide(db->drawing_area);
    db->is_visible = false;

    db->track_clicks = track_clicks;

    db->lock.unlock();
    return true;
}

static DrawBuf s_draw_bufs[max_draw_buf_num];
static int s_draw_buf_num = 0;

DrawBuf * add_buf_to_draw(Gui *gui, const unsigned char *buf, int width, int height,
                          const char *buf_name, bool track_clicks) noexcept {
    if (!check_gui(gui) || unlikely(s_draw_buf_num >= max_draw_buf_num)) return nullptr;
    auto db = &s_draw_bufs[s_draw_buf_num];
    if (unlikely(!setup_draw_buf(db, gui->box, buf, width, height,
                                 buf_name, track_clicks))) return nullptr;
    ++s_draw_buf_num;
    return db;
}

void stop_drawing(void) noexcept {
    for (int i = 0; i < s_draw_buf_num; ++i) {
#ifndef NDEBUG
        check(s_draw_bufs[i].lock.try_lock()); // Make sure the buf is unlocked.
        s_draw_bufs[i].lock.unlock();
#endif
        s_draw_bufs[i].shape_queue.destruct();
    }
    s_draw_buf_num = 0;
}

struct AtomicDrawBufClick {
    std::atomic<DrawBuf*> draw_buf;
    std::atomic<int> x, y;
    std::atomic<bool> left, ctrl, shift, alt;
};

static void read_click(DrawBufClick &c, const AtomicDrawBufClick &a) noexcept {
    c.draw_buf = a.draw_buf.load(order_rlx);
    if (likely(!c.draw_buf)) return;
    c.x = a.x.load(order_rlx);
    c.y = a.y.load(order_rlx);
    c.left = a.left.load(order_rlx);
    c.ctrl = a.ctrl.load(order_rlx);
    c.shift = a.shift.load(order_rlx);
    c.alt = a.alt.load(order_rlx);
}

static void write_click(AtomicDrawBufClick &a, const DrawBufClick &c) noexcept {
    a.draw_buf.store(c.draw_buf, order_rlx);
    a.x.store(c.x, order_rlx);
    a.y.store(c.y, order_rlx);
    a.left.store(c.left, order_rlx);
    a.ctrl.store(c.ctrl, order_rlx);
    a.shift.store(c.shift, order_rlx);
    a.alt.store(c.alt, order_rlx);
}

static AtomicDrawBufClick s_last_click;
static Seqlock s_last_click_lock;

static gboolean on_click(GtkWidget *ignore, GdkEvent *event,
                         gpointer draw_buf) noexcept {
    DrawBufClick c;
    if (!on_click_helper(c, static_cast<DrawBuf*>(draw_buf),
                         reinterpret_cast<GdkEventButton*>(event))) return TRUE;
    s_last_click_lock.begin_write();
    write_click(s_last_click, c);
    s_last_click_lock.finish_write();
    return TRUE;
}

static void reset_last_click() noexcept {
    s_last_click_lock.begin_write();
    s_last_click.draw_buf.store(nullptr, order_rlx);
    s_last_click_lock.finish_write();
}

static gboolean on_mouse_release(GtkWidget *ignore_w, GdkEvent *ignore_e,
                                 gpointer ignore) noexcept {
    reset_last_click();
    return TRUE;
}

static gboolean on_mouse_motion(GtkWidget *widget, GdkEvent *event,
                                gpointer ignore) noexcept {

    if (likely(!s_last_click.draw_buf.load(order_rlx))) return TRUE;

    const GdkEventMotion *e = reinterpret_cast<GdkEventMotion*>(event);
    const int x = static_cast<int>(e->x), y = static_cast<int>(e->y);

    gint width, height;
    gtk_widget_get_size_request(widget, &width, &height);

    s_last_click_lock.begin_write();
    if (likely(x >= 0 && x < width )) s_last_click.x.store(x, order_rlx);
    if (likely(y >= 0 && y < height)) s_last_click.y.store(y, order_rlx);
    s_last_click_lock.finish_write();

    return TRUE;
}

DrawBufClick get_last_click(void) noexcept {
    DrawBufClick c;
    unsigned read_seq;
    do {
        read_seq = s_last_click_lock.begin_read();
        read_click(c, s_last_click);
    } while (!s_last_click_lock.finish_read(read_seq));
    return c;
}
