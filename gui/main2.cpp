#include "common/check.h"
#include "gui/draw.h"
#include "gui/user_input.h"
#include "gui/gui.h"

static void stop(Gui *gui) {
    stop_drawing();
    stop_gui(gui);
}

int main_old2(int argc, char **argv) {
    Gui *gui = init_gui(&argc, &argv);
    if (!gui) return 1;
    if (!init_user_input(gui)) {
        stop(gui);
        return 2;
    }
    run_gui();
    stop(gui);
    return did_check_fail() ? 7 : 0;
}
