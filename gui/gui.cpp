#include <iostream>
#include "common/keep_going.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/log.h"
#include "gui/internal.h"
#include "gui/gui.h"

void run_gui(void) noexcept { gtk_main(); }

static GtkWidget * setup_box(GtkWidget *window) noexcept {
    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
    if (unlikely(!box)) {
        log_warning("gtk_box_new failed.");
        return nullptr;
    }
    gtk_container_add(GTK_CONTAINER(window), box);
    gtk_widget_show(box);
    return box;
}

static gboolean begin_exit(GtkWidget *ignore_w, GdkEvent *ignore_e,
                           gpointer ignore) noexcept {
    set_keep_going_false();
    gtk_main_quit();
    return FALSE;
}

static GtkWidget * setup_window(void) noexcept {
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    if (unlikely(!window)) {
        log_warning("gtk_window_new failed.");
        return nullptr;
    }
    gtk_window_set_title(GTK_WINDOW(window), "Score: Gtk 1 - Qt 0");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER_ALWAYS);
    gtk_widget_show(window);
    g_signal_connect(window, "delete-event", G_CALLBACK(begin_exit), nullptr);
    return window;
}

bool is_gui_initialized(const Gui *gui) noexcept {
    return gui && !!gui->window;
}

void stop_gui(Gui *gui) noexcept {
    if (!is_gui_initialized(gui)) return; // TODO: Is this redundant?
    std::cout << "GUI!\n";
    gtk_widget_destroy(gui->window); // This will also destroy the drawing areas and box.
    std::cout << "GUI!\n";
    
    gui->window = nullptr;
}

static Gui * init_gui_helper(Gui *gui, int *p_argc, char ***p_argv) noexcept {
    if (unlikely(!gtk_init_check(p_argc, p_argv))) {
        log_warning("gtk_init failed.");
        return nullptr;
    }
    if (unlikely(!(gui->window = setup_window()))) return nullptr;
    if (unlikely(!(gui->box = setup_box(gui->window)))) {
        stop_gui(gui);
        return nullptr;
    }
    return gui;
}

static Gui s_gui = {nullptr, nullptr};

Gui * init_gui(int *p_argc, char ***p_argv) noexcept {
    return is_gui_initialized(&s_gui) ? &s_gui :
                                        init_gui_helper(&s_gui, p_argc, p_argv);
}
