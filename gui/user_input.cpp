#include <atomic>
#include "common/log.h"
#include "common/macros.h"
#include "common/check.h"
#include "common/keep_going.h"
#include "gui/internal.h"
#include "gui/user_input.h"

static const std::memory_order order_rlx = std::memory_order_relaxed;

// "s_" for static
static std::atomic<bool> s_is_undo_request{false}, s_is_paused{false},
     s_keep_going{true}, s_use_wide_brush{true}, s_is_reset_request{false},
     s_is_sample_mode{false};
static std::atomic<enum Colors> s_selected_color{color_orange};

bool is_tracking_ball_region(void) noexcept { return false; } // TODO

bool is_sample_mode(void) noexcept { return s_is_sample_mode.load(order_rlx); }

static bool take_request(std::atomic<bool> &req) noexcept {
    return req.load(std::memory_order_acquire) &&
           req.exchange(false, std::memory_order_release);
}
bool take_reset_request(void) noexcept { return take_request(s_is_reset_request); }
bool  take_undo_request(void) noexcept { return take_request(s_is_undo_request);  }

bool is_paused(void) noexcept {
    return s_is_paused.load(std::memory_order_acquire);
}
bool use_wide_brush(void) noexcept {
    return s_use_wide_brush.load(std::memory_order_acquire);
}
enum Colors selected_color(void) noexcept {
    return s_selected_color.load(std::memory_order_acquire);
}

static GtkWidget *s_reset_btn = nullptr;
static const unsigned reset_confirm_press_num = 2;
static unsigned s_reset_press_num = 0;

static void reset_reset_btn(void) noexcept {
    if (s_reset_press_num == 0) return;
    s_reset_press_num = 0;
    gtk_button_set_label(GTK_BUTTON(s_reset_btn), "Reset");
}
void report_click_to_user_input(void) noexcept { reset_reset_btn(); }

static GtkWidget *s_btn_box = nullptr, *s_brush_width_toggle_btn = nullptr,
                 *s_sample_mode_toggle_btn = nullptr;

// This should only be called from one thread (at a time).
static void handle_input(guint key) noexcept {

    check_ret(gdk_keyval_is_lower(key),);

#define KY(v) case GDK_KEY_ ## v :
#define SL(c) s_selected_color.store(color_ ## c); log_info(#c); break;

    const unsigned old_reset_press_num = s_reset_press_num;
    switch (key) {
    KY(h)
        if (gtk_widget_is_visible(s_btn_box)) {
            gtk_widget_hide(s_btn_box);
            shrink_window_to_fit(s_btn_box);
        } else {
            gtk_widget_show(s_btn_box);
        }
        break;
    KY(m)     // TODO: Abstract toggle button.
        // Use `memory_order_acquire` to avoid speculative branch execution,
        // since C++ compilers will ignore control dependencies.
        if (s_is_sample_mode.load(std::memory_order_acquire)) {
            s_is_sample_mode.store(false, std::memory_order_release);
            gtk_button_set_label(GTK_BUTTON(s_sample_mode_toggle_btn),
                                 "Sample mode off");
        } else {
            s_is_sample_mode.store(true, std::memory_order_release);
            gtk_button_set_label(GTK_BUTTON(s_sample_mode_toggle_btn),
                                 "Sample mode on");
        }
        break;
    KY(r)
        if (++s_reset_press_num == reset_confirm_press_num) {
            s_is_reset_request.store(true, std::memory_order_release);
            reset_reset_btn();
        } else {
            gtk_button_set_label(GTK_BUTTON(s_reset_btn), "Really reset?");
        }
        break;
    KY(z)
        s_is_undo_request.store(true, std::memory_order_release);
        break;
    KY(Escape)
        set_keep_going_false();
        gtk_main_quit();
        return;
    KY(p) {
        const bool is_paused = !s_is_paused.load(std::memory_order_acquire);
        s_is_paused.store(is_paused, std::memory_order_release);
        log_info(is_paused ? "Paused" : "Unpaused");
        break;
    }
    KY(w) {
        const bool use_wide_brush = !s_use_wide_brush.load(std::memory_order_acquire);
        s_use_wide_brush.store(use_wide_brush, std::memory_order_release);
        gtk_button_set_label(GTK_BUTTON(s_brush_width_toggle_btn),
                             use_wide_brush ? "Wide" : "Narrow");
        break;
    }
    KY(o)       SL(orange)
    KY(k) KY(y) SL(yellow)
    KY(s) KY(b) SL(blue)
    KY(v) KY(f) SL(green)
    KY(j) KY(l) SL(white)
    KY(c)       SL(black)
    }
    // If any other keys or buttons are pressed before reset is pressed
    // enough times to actually reset, start counting the number of times
    // it was pressed from 0 again.
    if (s_reset_press_num == old_reset_press_num) reset_reset_btn();

    shrink_window_to_fit(s_btn_box);
#undef SL
#undef KY
}

static gboolean key_press_callback(GtkWidget *ignore_w, GdkEvent *event,
                                   gpointer ignore) noexcept {
    handle_input(gdk_keyval_to_lower(reinterpret_cast<GdkEventKey*>(event)->keyval));
    return TRUE;
}

static gboolean btn_press_callback(GtkWidget *ignore_w, GdkEvent *ignore,
                                   gpointer key) noexcept {
    handle_input(GPOINTER_TO_UINT(key));
    return TRUE;
}

struct Shortcut {
    const gchar *label;
    guint key;
};

#define SC(s, c) {#s, GDK_KEY_ ## c}

static const Shortcut shortcuts[] = {
    SC(Toggle sample mode, m),
    SC(Pause, p),
    SC(Undo, z),
    SC(Toggle brush width, w),
    SC(Yellow, y),
    SC(Blue, b),
    SC(Orange, o),
    SC(Field green, f),
    SC(White line, l),
    SC(Reset, r)
};

#undef SC

bool is_user_input_initialized(void) noexcept { return !!s_btn_box; }

void stop_user_input(Gui *gui) noexcept {
    check_gui(gui);
    gtk_widget_destroy(s_btn_box);
    s_btn_box = nullptr;
}

bool init_user_input(Gui *gui) noexcept {

    if (is_user_input_initialized()) return true;
    if (!check_gui(gui)) return false;

    s_btn_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    if (unlikely(!s_btn_box)) return false;
    gtk_box_pack_end(GTK_BOX(gui->box), s_btn_box, FALSE, FALSE, 1);
    gtk_widget_show(s_btn_box);

    for (size_t i = 0; i < G_N_ELEMENTS(shortcuts); ++i) {

        GtkWidget *btn = gtk_button_new_with_label(shortcuts[i].label);
        if (unlikely(!btn)) return false; // `btn_box` will be destroyed automatically.
        gtk_box_pack_start(GTK_BOX(s_btn_box), btn, FALSE, FALSE, 3);
        gtk_widget_show(btn);
        g_signal_connect(btn, "button-press-event", G_CALLBACK(btn_press_callback),
                         GUINT_TO_POINTER(shortcuts[i].key));

#define CS(k, b) case GDK_KEY_ ## k: b = btn; break;
        switch(shortcuts[i].key) {
            CS(w, s_brush_width_toggle_btn)
            CS(r, s_reset_btn)
            CS(m, s_sample_mode_toggle_btn)
            default: ; // Do nothing.
        }
#undef CS

    }

    g_signal_connect(gui->window, "key-press-event",
                     G_CALLBACK(key_press_callback), nullptr);
    return true;
}
