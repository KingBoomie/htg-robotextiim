#pragma once

#include "common/color_enum.h"

struct Gui;

bool init_user_input(Gui *gui) noexcept;
bool is_user_input_initialized(void) noexcept;

// Note: user input is stopped automatically when the gui it was initialized
// with is stopped, so this function doesn't have to be called explicitly.
// Calling this function after the gui has been stopped is undefined behaviour.
void stop_user_input(Gui *gui) noexcept;

// The following functions use `memory_order_acquire` loads, so they can
// be used in `if` statements without worrying about speculative execution.

bool is_sample_mode(void) noexcept;
bool is_tracking_ball_region(void) noexcept;
bool is_paused(void) noexcept;
bool use_wide_brush(void) noexcept;
enum Colors selected_color(void) noexcept;

// `*_request` functions return whether there is a request and set the
// request to false (atomically using `exchange`), assuming that whatever
// is calling them is going to handle the request if there is one.
bool  take_undo_request(void) noexcept;
bool take_reset_request(void) noexcept;
