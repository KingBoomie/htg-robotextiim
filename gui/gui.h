#pragma once

struct Gui;

Gui * init_gui(int *p_argc, char ***p_argv) noexcept;
bool is_gui_initialized(const Gui *gui) noexcept;
void run_gui(void) noexcept;
void stop_gui(Gui *gui) noexcept;

