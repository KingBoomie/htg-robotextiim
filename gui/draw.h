#pragma once

#define max_draw_buf_num 5   // Trying to add more buffers than this will fail.

struct Gui;
struct DrawBuf;

struct Shape {
    struct Rect {
        int x, y;   // Top left corner
        int width, height;
    };
    struct Circle {
        int x, y;   // Center
        int radius;
    };
    union {
        Rect rect;
        Circle circle;
    };
    double r, g, b;
    enum class Type {circle, rect} type;
    bool do_fill; // Fill the shape, rather than drawing just the border.
};

// Add an RGB buffer to be displayed. Returns -1 on failure, else a buffer handle.
// This should only be called from the main thread during initialization.
DrawBuf * add_buf_to_draw(Gui *gui, const unsigned char *buf, int width, int height,
                          const char *buf_name, bool track_clicks) noexcept;

bool is_draw_buf_visible(DrawBuf *db) noexcept;

// Call this before changing the contents of the RGB buffer the DrawBuf was
// created with. Returns false if the DrawBuf can't be updated (for example,
// if it isn't visible).
bool  begin_updating_draw_buf(DrawBuf *db) noexcept;

// Call this to let the DrawBuf be drawn.
void finish_updating_draw_buf(DrawBuf *db) noexcept;

// Draw shape on buffer with some handle. Only one thread should draw on a given
// buffer, though different threads may draw on different buffers and one thread
// may draw on more than one buffer.
bool draw_shape(DrawBuf *draw_buf, Shape shape) noexcept;

void stop_drawing(void) noexcept;

struct DrawBufClick {
    DrawBufClick() { draw_buf = nullptr; }
    DrawBuf *draw_buf; // DrawBuf that was last clicked on or nullptr
    int x, y;          // Coordinates of the pixel that was clicked on
                       // relative to the upper-left corner of the buffer
    bool left,         // Was the left mouse button pressed or
         ctrl,         // ctrl, shift or alt held down while clicking?
         shift,
         alt;
};

DrawBufClick get_last_click(void) noexcept;
