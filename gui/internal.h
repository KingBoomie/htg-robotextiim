#pragma once

#if defined(__clang__)
# pragma clang system_header
#elif defined(__GNUC__)      // The compiler is GCC.
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-pedantic"
# pragma GCC diagnostic ignored "-Weverything"
# pragma GCC diagnostic ignored "-Wall"
#endif

#include <gtk/gtk.h>

#ifdef __GNUC__
# pragma GCC diagnostic pop
#endif // __GNUC__

struct Gui { GtkWidget *window, *box; };

#define check_gui(gui) check(gui && gui->window && gui->box)

inline void shrink_window_to_fit(GtkWidget *widget) noexcept {
    GtkWidget *window = gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW);
    gtk_window_resize(GTK_WINDOW(window), 1, 1);
}

void report_click_to_user_input(void) noexcept;
