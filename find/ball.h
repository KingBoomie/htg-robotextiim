#pragma once

#include "common/vec2.h"

struct TrackedData {
    struct Ball {
        Vec2d pos, vel;
        double pr;
    };
    Ball balls[max_tracked_ball_num];
    int num;
};

struct RelPos;

void track_balls(const RelPos *rels, const double *prs, int ball_num) noexcept;

void get_tracked_balls(TrackedData *d) noexcept;
