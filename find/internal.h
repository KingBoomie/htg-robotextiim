#pragma once

#include <math.h> // sqrt
#include "common/macros.h"
#include "common/constants.h"

#define cam_focal_len 1000.0 // TODO: Measure this with the new camera.

enum {
    front_cam_idx
    ,back_cam_idx
};
// Reordering the indices WILL break code because C++ syntax sucks.
// (Try initializing a static const array so that specific elements
//  are at specific enumerated indices in C++ (as opposed to, say, C99).)

// TODO: deg_from_pixel_x
// Clang 3.5 compiles this to one addition and one multiplication on x86.
static inline double dir_from_pixel_x(double x) noexcept {
    const double inv_diag_len = 1.0 / sqrt(static_cast<double>(
        frame_width * frame_width + frame_height * frame_height));
    return (x + (0.5 - static_cast<double>(frame_width/2))) * (cam_fov * inv_diag_len);
}
