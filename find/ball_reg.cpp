#include <cstdlib>   // integer abs
#include <cmath>     // cos, sin
#include <algorithm> // max
#include <utility>   // swap
#include "common/color_enum.h"
#include "common/log.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/vec2.h"
#include "sync/seqlock.h"
#include "loc/loc.h"
#include "stats/stats.h"
#include "find/internal.h"
#include "find/ball_reg.h"

// What were the people making the C++ standard thinking when
// they chose the ridiculously verbose memory order names? Why
// not just m_o_*?
#define order_rlx std::memory_order_relaxed

using std::swap;
using std::max;
using std::abs;

static int calc_min_ball_area() noexcept {
    const Vec2d r = get_robot_pos(), u = get_robot_dir_vec();
    const double hy = u.y < 0.0 ? 0.0 : field_len,
                 vx = u.x < 0.0 ? 0.0 : field_width;
    Vec2d hd, vd; // Delta between robot and horizontal/vertical border
    hd.y = hy - r.y;
    vd.x = vx - r.x;
    hd.x = hd.y * u.x / u.y;
    vd.y = vd.x * u.y / u.x;
    const double sqr_border_dist = fmin(hd.sqr_len(), vd.sqr_len());
    if (unlikely(sqr_border_dist == 0.0)) { // TODO: Should this be a `check`?
        log_warning("Calculated border distance == 0.");
        return 60; // A ball's area should be about 63.3 pixels if it's as
                   // far from the robot as possible.
    } // return ball area if ball was on border
    return static_cast<int>(ball_diam * ball_diam *
                            cam_focal_len * cam_focal_len / sqr_border_dist) - 40;
}

// Make sure most pixels around the edge of a ball are green.
static bool has_field_background(const uchar *cmap, int cmap_width, int cmap_height,
                                 const Region &reg) noexcept {

    // Done: optimize this by calculating only one sine and cosine of the
    // angle `angle_per_circle / dbl(max_test_num)` and then rotating a
    // vector by that angle `max_test_num` times. If the compiler is clever
    // enough, the sine and cosine can even be calculated at compile time.

    constexpr int max_test_num = 20; // TWEAK
    constexpr double rot_angle = angle_per_circle / static_cast<double>(max_test_num);
    const double rot_cos = cos(rot_angle), rot_sin = sin(rot_angle);

    // `rad` = a bit more than the region's radius (assuming it's similar to a circle)
    const double rad = static_cast<double>(max(reg.width, reg.height) + 4) * 0.5;
    int test_num = 0, color_count[color_num];
    color_count[color_blue]  = color_count[color_yellow] =
    color_count[color_white] = color_count[color_green]  = 0;

    Vec2d delta = {0.0, rad};
    for (int i = 0; i < max_test_num; ++i, delta.rot(rot_cos, rot_sin)) {

        const unsigned x = static_cast<unsigned>(reg.cen_x + delta.x),
                       y = static_cast<unsigned>(reg.cen_y + delta.y);
        // Using unsigned integers lets us avoid
        // checking whether `x` or `y` is negative.
        if (unlikely(x >= static_cast<unsigned>(cmap_width) ||
                     y >= static_cast<unsigned>(cmap_height))) continue;

        // TODO: Is using an array of length `color_num` to avoid a `switch` faster?
        ++test_num;
        ++color_count[cmap[y * static_cast<unsigned>(cmap_width) + x]];
    }

    check_ret(test_num != 0, false);

    // A ball can only have one gate behind it, so ignore pixels
    // of the other gate's color, which are probably just noise.
    const int gate_count = max(color_count[color_yellow], color_count[color_blue]);

    const int r =
        color_count[color_green] * 12 +
        // The ball may be on or next to a white line.
        color_count[color_white] * 5 -
        // We definitely don't want to go after a ball
        // that's in a goal already.
        gate_count * 3;
    return r > 7 * test_num; // TODO: Measure constants.
}

// Make sure a line from the pixel in front of the robot to the ball
// doesn't cross a border.
static bool is_within_field_borders(const uchar *cmap, int cmap_width, int cmap_height,
                                    int cen_x, int cen_y, int bottom_y) noexcept {
    // TWEAK: Adjust `in_front_y` for resolution.
    const int in_front_x = cmap_width / 2, in_front_y = cmap_height - 40;
    if (bottom_y >= in_front_y) return true; // The ball is right next to the robot.

    int ax = cen_x, ay = cen_y, bx = in_front_x, by = in_front_y;
    const bool is_steep = abs(by - ay) > abs(bx - ax);
    if (is_steep) {
        swap(ax, ay);
        swap(bx, by);
    }
    if (ax > bx) {
        swap(ax, bx);
        swap(ay, by);
    }
    const int dx = bx - ax, dy = abs(by - ay);
    const int y_step = (ay < by) ? 1 : -1;

    // TODO: Take the white border line into account.
    int black_count = 0;
    for (int px = ax, py = ay, eps = dx / 2; px <= bx; ++px) {

        int cx = px, cy = py;
        if (is_steep) swap(cx, cy);
        const uchar c = cmap[cy * cmap_width + cx];
        black_count += (c == color_black);

        eps -= dy;
        if (eps < 0) {
            py += y_step;
            eps += dx;
        }
    }
    return black_count < 6; // TWEAK: Adjust for resolution.
}

static void store_reg(AtomicBallRegs *b, const Region &r, double stat_dist) noexcept {
    const int n = b->num.load(order_rlx);
    b->regs[n].stat_dist.store(stat_dist, order_rlx);
    b->regs[n].cen_x.store( r.cen_x,  order_rlx);
    b->regs[n].cen_y.store( r.cen_y,  order_rlx);
    // Skip cen_y, x1, y1, x2 and y2, in that order.
    b->regs[n].width.store( r.width,  order_rlx);
    b->regs[n].height.store(r.height, order_rlx);
    b->regs[n].area.store(  r.area,   order_rlx);
    b->num.store(n + 1, order_rlx);
}

static SeqlockBuf<AtomicBallRegs, 2> s_reg_bufs[cam_num];

void filter_ball_regs(int cam_idx, double dir_offset, long timestamp_usec,
                      const Region *regs, int reg_num, const uchar *cmap,
                      int cmap_width, int cmap_height) noexcept {

    const int min_area = calc_min_ball_area();

    unsigned write_idx;
    AtomicBallRegs *b = s_reg_bufs[cam_idx].begin_write(write_idx);

    b->timestamp.store(timestamp_usec, order_rlx);
    b->dir_offset.store(dir_offset, order_rlx);
    b->num.store(0, order_rlx);

    for (int i = 0; i < reg_num; ++i) {

        if (regs[i].area < min_area) continue;

        const double stat_dist = calc_ball_stat_dist(regs[i]);
        if (stat_dist > 2.0) continue; // pr_from_stat_dist(2.0) = ~0.36

        // TODO: Uncomment this.
     //   if (!has_field_background(cmap, cmap_width, cmap_height, regs[i])) continue;

        const int cen_x_i = static_cast<int>(regs[i].cen_x),
                  cen_y_i = static_cast<int>(regs[i].cen_y);
        if (!is_within_field_borders(cmap, cmap_width, cmap_height,
                                     cen_x_i, cen_y_i, regs[i].y2)) continue;

        store_reg(b, regs[i], stat_dist);
        if (unlikely(b->num.load(order_rlx) == max_tracked_ball_num)) {
            log_warning("Exceeded ball region capacity.");
            break;
        }
    }

    s_reg_bufs[cam_idx].finish_write(write_idx);
}

unsigned get_ball_read_idx(int cam_idx) noexcept {
    return s_reg_bufs[cam_idx].get_cur_idx();
}

const AtomicBallRegs *begin_reading_ball_regs(int cam_idx, unsigned &read_idx,
                                              unsigned &read_seq) noexcept {
    return s_reg_bufs[cam_idx].begin_read(read_idx, read_seq);
}

bool finish_reading_ball_regs(int cam_idx, unsigned read_idx,
                              unsigned read_seq) noexcept {
    return s_reg_bufs[cam_idx].finish_read(read_idx, read_seq);
}
