#include "common/log.h"
#include "common/check.h"
#include "common/color_enum.h"
#include "common/rel_pos.h"
#include "sync/seqlock.h"
#include "find/internal.h"
#include "find/gate.h"

#define order_rlx std::memory_order_relaxed

static RelPos calc_gate_pos(const Region &reg, const uchar *cmap,
                            int cmap_width, int cmap_height) noexcept {
    // TODO: Take gate angle, width into account.
    const double h = static_cast<double>(reg.height);
    const double//dist_by_width = gate_width  * cam_focal_len / reg->width,
                 dist_by_height = gate_height * cam_focal_len / h;
    RelPos p;
    p.dist = dist_by_height * 0.5; // `(dist_by_width + dist_by_height) * 0.25` is worse.
    p.dir = dir_from_pixel_x(reg.cen_x);
    return p;
}

static bool is_gate_shape(const Region &reg) noexcept {
    if (reg.area < 200 || reg.width < reg.height / 2) return false;
    const int bbox_area = reg.width * reg.height;
    return reg.area > bbox_area / 2;
}

static double calc_aim_dir(const uchar *cmap, int cmap_width,
                           const Region &reg) noexcept {

    const int step = 10, icen_x = static_cast<int>(reg.cen_x); // TWEAK: step
    int left = 0, right = 0;

    for (int y = reg.y1; y < reg.y2; y += step) {
        const int row_start = y * cmap_width;
#define SC(a, b, v) \
    for (int x = a; x < b; x += step) \
        v += (cmap[row_start + x] == color_green);
        SC(reg.x1, icen_x, left);
        SC(icen_x, reg.x2, right);
#undef SC
    }

    const int diff = right - left, eps = 7; // TWEAK: eps
    double aim_x;
    if (diff > eps) {
        aim_x = (icen_x + reg.x2) * 0.5;
    } else if (diff < -eps) {
        aim_x = (icen_x + reg.x1) * 0.5;
    } else {
        aim_x = icen_x;
    }
    return dir_from_pixel_x(aim_x);
};

static int opnt_gate_color; // Only changed by `set_opnt_gate_color`.
static int opnt_band_color; // Color of bottom band on opnt's robot

static void set_opnt_gate_color(char opnt_gate_color_ch) noexcept {
    if (opnt_gate_color_ch == 'y' || opnt_gate_color_ch == 'k') {
        log_info("Opponent's gate is yellow.");
        opnt_gate_color = color_yellow;
    } else {
        if (opnt_gate_color_ch == 'b' || opnt_gate_color_ch == 's') {
            log_info("Opponent's gate is blue.");
        } else {
            log_warning("Gate color unknown; assuming that opponent's gate is blue.");
        }
        opnt_gate_color = color_blue;
    }
}

int get_opnt_gate_color(void) noexcept { return opnt_gate_color; }

void init_gate_info(char opnt_gate_color_ch) noexcept {
    set_opnt_gate_color(opnt_gate_color_ch);
    opnt_band_color = color_yellow; // TODO
}

static bool is_opnt_band(const Region &reg) noexcept {
    return false; // TODO
}

struct OurGate {
    std::atomic<double> dist, dir;
    std::atomic<long> timestamp;
};

struct OpntGate {
    std::atomic<double> dist, dir, aim_dir;
    std::atomic<long> timestamp;
};

static SeqlockBuf<OpntGate, 2> s_opnt_buf;
static SeqlockBuf< OurGate, 2> s_our_buf;

// TODO: Figure out whether the robot is to the left or right of the gate or
// more or less right in front of it and find the gate's corners.
// TODO: If a gate's region is "split" into two (the opponent or several balls
// are right in front of it), find the corners of the two regions and merge them
// into a single gate's dimensions. If the opponent's gate is split, let the rest
// of the program know that aiming is difficult and simply aim for the center of
// the larger gate region instead of counting green pixels.
void try_find_gate(double dir_offset, long timestamp_usec, Shape::Rect &bbox,
                   const Region *regs, int reg_num, int color, const uchar *cmap,
                   int cmap_width, int cmap_height) noexcept {

    int best = -1; // Index of region that is most similar to a gate
    for (int i = 0; i < reg_num; ++i) {
        if (is_opnt_band(regs[i])) {
            continue; // TODO
        }
        if (!is_gate_shape(regs[i])) continue;
        if (best < 0 || regs[i].area > regs[best].area) best = i;
    }
    if (best < 0) {
        bbox.x = bbox.y = bbox.width = bbox.height = 0;
        return;
    }

    RelPos gate = calc_gate_pos(regs[best], cmap, cmap_width, cmap_height);
    gate.dir += dir_offset;

    bbox.x = regs[best].x1;
    bbox.y = regs[best].y1;
    bbox.width  = regs[best].width;
    bbox.height = regs[best].height;

#define WRITE_GATE(T, buf) /* Erase this backslash for syntax highlighting --> */ \
    /* Contention here is unlikely, because generally only one \
       camera should see a given gate at a given point in time. */ \
    T *g = buf.begin_write(write_idx); \
    /* Cancel write if the gate was already found on a frame from the \
       other camera and that frame was captured at the same time or is newer. */ \
    if (buf.raw_read()->timestamp.load(order_rlx) >= timestamp_usec) { \
        buf.cancel_write(write_idx); \
        return; \
    } \
    g->timestamp.store(timestamp_usec, order_rlx); \
    g->dir.store(      gate.dir,       order_rlx); \
    g->dist.store(     gate.dist,      order_rlx)

    unsigned write_idx;
    if (color == get_opnt_gate_color()) {
        const double aim_dir = calc_aim_dir(cmap, cmap_width, regs[best]) + dir_offset;
        WRITE_GATE(OpntGate, s_opnt_buf);
        g->aim_dir.store(aim_dir, order_rlx);
        s_opnt_buf.finish_write(write_idx);
    } else {
        WRITE_GATE(OurGate, s_our_buf);
        s_our_buf.finish_write(write_idx);
    }

#undef WRITE_GATE
}

// TODO: Use read function instead.
#define READ_BUF(T, buf, code) do { \
    unsigned read_idx, read_seq; \
    do { \
        const T *g = buf.begin_read(read_idx, read_seq); \
        { code } \
    } while (!likely(buf.finish_read(read_idx, read_seq))); \
} while (false)

#define GET_POS(T, buf) \
    RelPos r; \
    READ_BUF(T, buf, \
             r.dist = g->dist.load(order_rlx); \
             r.dir  = g->dir.load( order_rlx); \
             if (timestamp_usec) *timestamp_usec = g->timestamp.load(order_rlx); \
    ); \
    return r

RelPos get_opnt_gate_pos(long *timestamp_usec) noexcept { GET_POS(OpntGate, s_opnt_buf); }
RelPos  get_our_gate_pos(long *timestamp_usec) noexcept { GET_POS( OurGate,  s_our_buf); }

#undef GET_POS

RelPos get_aim(long *timestamp_usec) noexcept {
    RelPos r;
    READ_BUF(OpntGate, s_opnt_buf,
             r.dist = g->dist.load(order_rlx);
             r.dir  = g->aim_dir.load(order_rlx);
             if (timestamp_usec) *timestamp_usec = g->timestamp.load(order_rlx);
    );
    return r;
}

bool is_aim_on_left(long *timestamp_usec) noexcept {

    long ts;
    bool on_left;

    READ_BUF(OpntGate, s_opnt_buf,
             on_left = g->aim_dir.load(order_rlx) < 0.0;
             ts = g->timestamp.load(order_rlx);
    );

    constexpr long eps = 30000; // About 2/60 ms
    READ_BUF(OurGate, s_our_buf,
             const long our_ts = g->timestamp.load(order_rlx);
             if (our_ts - ts > eps) { // If the frame with the opnt's gate is
                                      // much older than the one with our gate...
                 // Most of the time is_aim_on_left == is_our_gate_on_right
                 on_left = g->dir.load(order_rlx) > 0.0;
                 ts = our_ts;
             }
    );

    if (timestamp_usec) *timestamp_usec = ts;
    return on_left;
}
