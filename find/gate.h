#pragma once

#include "cmvision/types.h"
#include "gui/draw.h"

struct RelPos;

void init_gate_info(char opnt_gate_color_ch) noexcept;

void try_find_gate(double dir_offset, long timestamp_usec, Shape::Rect &bbox,
                   const Region *regs, int reg_num, int color, const uchar *cmap,
                   int cmap_width, int cmap_height) noexcept;

int get_opnt_gate_color(void) noexcept;

// These functions return a gate's position (when it was last seen) relative to
// the robot and write when the gate was last seen into `timestamp_usec` if
// it isn't a null pointer.
RelPos  get_our_gate_pos(long *timestamp_usec) noexcept;
RelPos get_opnt_gate_pos(long *timestamp_usec) noexcept;

// Returns where to aim. This may or may not be equal to the opnt's gate's position.
// `timestamp_usec` may be a null pointer.
RelPos get_aim(long *timestamp_usec) noexcept;

// Useful for deciding which way to turn when aiming or looking for the opnt's gate.
// If the opnt's gate isn't visible, the return value will be based on our gate's
// position and the timestamp will be when our gate was last seen.
// `timestamp_usec` may be a null pointer.
bool is_aim_on_left(long *timestamp_usec) noexcept;
