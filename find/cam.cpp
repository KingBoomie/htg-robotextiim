#include <thread> // sleep_for
#include <atomic>
#include "sync/semaphore.h"
#include "common/log.h"
#include "common/keep_going.h"
#include "cmvision/capture.h"
#include "cmvision/threshold.h"
#include "cmvision/region.h"
#include "gui/user_input.h" // is_paused
#include "tune/tune.h"
#include "find/internal.h"
#include "find/ball_reg.h"
#include "find/gate.h"
#include "find/cam.h"

#define check_cam_idx_ret(r) \
    check_ret(0 <= cam_idx, r); \
    check_ret(cam_idx < cam_num, r)

static constexpr int cmap_size = frame_size, max_run_num = 50000, max_reg_num = 9000,
                     process_color_num = 3; // Runs and regions are only created for
                                            // the first `process_color_num` colors.

static const char * const tmap_file_path = "../CMVision/config/test.tmap";

static const char * const cam_dev_paths[cam_num] = {
    "/dev/video0"
   ,"/dev/video1"
};

static const double dir_offsets[cam_num] = {
    0.0,
    angle_per_circle * 0.5
};

static bool s_is_cam_inited[cam_num];
static Capture s_cap[cam_num];
static uchar s_tmaps[cam_num][tmap_size], s_cmaps[cam_num][cmap_size];
static Run    s_runs[cam_num][process_color_num][max_run_num];
static Region s_regs[cam_num][process_color_num][max_reg_num];
static int s_run_num[cam_num][process_color_num], s_reg_num[cam_num][process_color_num];

static Semaphore s_sem;
void      wait_for_frame() noexcept { s_sem.wait(); }
static void notify_frame() noexcept { s_sem.notify(); }

bool is_cam_inited(int cam_idx) noexcept { return s_is_cam_inited[cam_idx]; }

bool init_cam(int cam_idx) noexcept {

    check_cam_idx_ret(false);

    if (is_cam_inited(cam_idx)) return true;
    if (!likely(s_cap[cam_idx].init(cam_dev_paths[cam_idx], frame_width, frame_height,
                                    V4L2_PIX_FMT_YUYV, cam_fps))) {
        return false;
    }

    for (int i = 0; i < cam_num; ++i) {
        if (is_cam_inited(i)) {
            if (!check(i != cam_idx)) {
                s_cap[cam_idx].close();
                return false;
            }
            memcpy(s_tmaps[cam_idx], s_tmaps[i], tmap_size);
            return s_is_cam_inited[cam_idx] = true;
        }
    }

    if (!likely(CMVision::LoadThresholdFile(s_tmaps[cam_idx], tmap_file_path))) {
        log_critical("Couldn't load tmap file for camera " + tostr(cam_idx) + ".");
        s_cap[cam_idx].close();
        return false;
    }
    CMVision::CheckTMapColors(s_tmaps[cam_idx], color_num, color_background);
    return s_is_cam_inited[cam_idx] = true;
}

void stop_cam(int cam_idx) noexcept {

    check_cam_idx_ret( );

    if (!likely(is_cam_inited(cam_idx))) return;

    s_cap[cam_idx].close();
    s_is_cam_inited[cam_idx] = false;

    // If someone is still using their copy of the tmap, return
    // and let them save the tmap later.
    for (int i = 0; i < cam_num; ++i) {
        if (is_cam_inited(i)) return;
    }

    if (!likely(CMVision::SaveThresholdFile(s_tmaps[cam_idx], tmap_file_path))) {
        log_warning("Couldn't save tmap file for camera " + tostr(cam_idx) + ".");
    }
}

static const Capture::Frame *s_frames[cam_num];

void run_cam(int cam_idx) noexcept {

    check_cam_idx_ret( );
    check_ret(is_cam_inited(cam_idx),);

    uchar  *cmap = &s_cmaps[cam_idx][0], *tmap = &s_tmaps[cam_idx][0];
    Capture &cap = s_cap[cam_idx];
    const Capture::Frame *&frame = s_frames[cam_idx];
    // `frame` is a non-`const` reference to a non-`const`
    // pointer to a `const Capture::Frame`.

    Run    *runs[process_color_num];
    Region *regs[process_color_num];
    for (int i = 0; i < process_color_num; ++i) {
        runs[i] = &s_runs[cam_idx][i][0];
        regs[i] = &s_regs[cam_idx][i][0];
    }
    int *run_num = &s_run_num[cam_idx][0], *reg_num = &s_reg_num[cam_idx][0];

    while (keep_going()) {

        if (!frame) {
            frame = cap.capture_frame();
            if (!likely(frame)) continue;
            draw_raw_frame(cam_idx, frame->get_raw_data()); // Queue draw raw frame.
        }

        // Wait until cmap is drawn.

        CMVision::ThresholdImage(cmap, tmap, frame->get_raw_data(), frame_size);
        draw_cmap(cam_idx, cmap); // Queue draw cmap.

        // TODO: Could this loop be done in parallel efficiently?
        for (int i = 0; i < process_color_num; ++i) {
            CMVision::EncodeRuns(&runs[i][0], &run_num[i], max_run_num, i,
                                 cmap, frame_width, frame_height);
            CMVision::ConnectComponents(runs[i], run_num[i], frame_height);
            reg_num[i] = CMVision::ExtractRegions(regs[i], max_reg_num,
                                                  runs[i], run_num[i]);
        }

        const long ts = frame->get_timestamp_usec();
        const double dir_offset = dir_offsets[cam_idx];
        Shape::Rect gate_bboxs[2]; // 2 == number of gates
        // These three function calls could be done in parallel:
        try_find_gate(dir_offset, ts, gate_bboxs[0],
                      regs[color_blue], reg_num[color_blue], color_blue,
                      cmap, frame_width, frame_height);
        try_find_gate(dir_offset, ts, gate_bboxs[1],
                      regs[color_yellow], reg_num[color_yellow], color_yellow,
                      cmap, frame_width, frame_height);
        filter_ball_regs(cam_idx, dir_offset, ts,
                         regs[color_orange], reg_num[color_orange],
                         cmap, frame_width, frame_height);
        notify_frame();

        unsigned read_idx, read_seq;
        const auto b = begin_reading_ball_regs(cam_idx, read_idx, read_seq);
        draw_found_balls(cam_idx, b);
        const bool no_data_race = finish_reading_ball_regs(cam_idx, read_idx, read_seq);
        if (!check(no_data_race)) break;

        for (int i = 0; i < 2; ++i) { // 2 == number of gates
            if (gate_bboxs[i].width > 0) draw_found_gate(cam_idx, gate_bboxs[i]);
        }

        tune_stuff(cam_idx, tmap);

        if (is_paused()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(40));
        } else {
            // Wait until raw frame is drawn.
            cap.release_frame(frame);
            frame = nullptr;
        }
    }

    notify_frame(); // Avoid deadlock at exit.
}

const Region * find_region_of_color(int cam_idx, int x, int y, int color) noexcept {

    check_cam_idx_ret(nullptr);

    const int run_idx = CMVision::FindRun(s_runs[cam_idx][color], 0,
                                          s_run_num[cam_idx][color] - 1, x, y);
    if (run_idx < 0) return nullptr;

    const int reg_idx = s_runs[cam_idx][color][run_idx].parent;
    check_ret(reg_idx < s_reg_num[cam_idx][color], nullptr);
    return &s_regs[cam_idx][color][reg_idx];
}

const Region * find_region(int cam_idx, int x, int y, int &color) noexcept {
    for (int i = 0; i < process_color_num; ++i) {
        const Region *p = find_region_of_color(cam_idx, x, y, i);
        if (p) {
            color = i;
            return p;
        }
    }
    return nullptr;
}

#define check_args(r) \
    check_cam_idx_ret(r); \
    check_ret(0 <= x, r); \
    check_ret(x < frame_width, r); \
    check_ret(0 <= y, r); \
    check_ret(y < frame_height, r)

uchar get_cmap(int cam_idx, int x, int y) noexcept {
    check_args(0);
    return s_cmaps[cam_idx][y * frame_width + x];
}

yuv get_frame_pixel(int cam_idx, int x, int y) noexcept {
    {
        yuv f = {0, 0, 0};
        check_args(f);
    }
    const int odd = x & 1;
    x -= odd;
    const uchar *a = s_frames[cam_idx]->get_raw_data() + ((y * frame_width + x) * 2);
    yuv p;
    p.y = odd ? a[2] : a[0]; // y2 or y1
    p.u = a[1];
    p.v = a[3];
    return p;
}
