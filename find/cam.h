#pragma once

#include "cmvision/types.h"

bool init_cam(int cam_idx) noexcept;
bool is_cam_inited(int cam_idx) noexcept;
void run_cam( int cam_idx) noexcept;
void stop_cam(int cam_idx) noexcept; // This should only be called when
                                     // `run_cam` has returned.

void wait_for_frame() noexcept;

const Region * find_region_of_color(int cam_idx, int x, int y, int  color) noexcept;
const Region *          find_region(int cam_idx, int x, int y, int &color) noexcept;

uchar        get_cmap(int cam_idx, int x, int y) noexcept;
yuv   get_frame_pixel(int cam_idx, int x, int y) noexcept;
