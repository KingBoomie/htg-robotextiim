#include "common/rel_pos.h"
#include "sync/seqlock.h"
#include "loc/loc.h"
#include "visualize/field.h"
#include "find/ball.h"

#define order_rlx std::memory_order_relaxed

static Vec2d last_abs_pos[max_tracked_ball_num];
static int last_num = 0;

// Since even benign data races cause undefined behaviour (and
// thus can cause unexpected compiler optimizations), we need
// a struct with atomic aligned variables of builtin types which
// will all be loaded and stored using std::memory_order_relaxed,
// even though the machine code generated without optimizations
// would be exactly the same.
struct AtomicTrackedData { // TODO: Make num the first member?
    struct AtomicBall { std::atomic<double> pos_x, pos_y, vel_x, vel_y, pr; };
    AtomicBall balls[max_tracked_ball_num];
    std::atomic<int> num;
};
// Note that the data layout of `TrackedData` and `AtomicTrackedData` is identical.

static SeqlockBuf<AtomicTrackedData, 4> s_tracked_buf;

void track_balls(const RelPos *rels, const double *prs, int ball_num) noexcept {

    const Vec2d  robot_pos = get_robot_pos();
    const double robot_dir = get_robot_dir();
    Vec2d abs_pos[max_tracked_ball_num];
    for (int i = 0; i < ball_num; ++i) {
        RelPos e;
        e.dist = rels[i].dist;
        e.dir  = robot_dir - rels[i].dir;
        abs_pos[i] = robot_pos + to_vec2d(e);
    }

    // TODO: This function call doesn't really belong here.
    visualize_field(abs_pos, prs, ball_num, robot_pos, robot_dir);

    unsigned write_idx;
    AtomicTrackedData * d = s_tracked_buf.begin_write(write_idx);
    for (int i = 0; i < ball_num; ++i) {
        d->balls[i].pos_x.store(abs_pos[i].x, order_rlx);
        d->balls[i].pos_y.store(abs_pos[i].y, order_rlx);
        d->balls[i].vel_x.store(0.0, order_rlx);
        d->balls[i].vel_y.store(0.0, order_rlx);
        d->balls[i].pr.store(prs[i], order_rlx);
    }
    d->num.store(ball_num, order_rlx);
    s_tracked_buf.finish_write(write_idx);
}

void get_tracked_balls(TrackedData *d) noexcept {
    unsigned read_idx, read_seq;
    do {
        const AtomicTrackedData *a = s_tracked_buf.begin_read(read_idx, read_seq);
        d->num = a->num.load(order_rlx);
        for (int i = 0; i < d->num; ++i) {
            d->balls[i].pos.x = a->balls[i].pos_x.load(order_rlx);
            d->balls[i].pos.y = a->balls[i].pos_y.load(order_rlx);
            d->balls[i].vel.x = a->balls[i].vel_x.load(order_rlx);
            d->balls[i].vel.y = a->balls[i].vel_y.load(order_rlx);
            d->balls[i].pr    = a->balls[i].pr.load(order_rlx);
        }
    } while (!likely(s_tracked_buf.finish_read(read_idx, read_seq)));
}