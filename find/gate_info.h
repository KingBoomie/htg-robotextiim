#pragma once

#include "common/rel_pos.h"

struct GateInfo {
    RelPos aim, opnt, our;
    bool opnt_found, our_found, opnt_on_left, our_on_left;
};
