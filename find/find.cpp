#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "common/chrono_util.h"
#include "common/log.h"
#include "common/keep_going.h"
#include "common/check.h"
#include "common/macros.h"
#include "common/constants.h"
#include "stats/stats.h"
#include "tune/tune.h"
#include "find/internal.h"
#include "find/ball_reg.h"
#include "find/ball.h"
#include "find/gate.h"
#include "find/cam.h"
#include "find/find.h"

#define order_rlx std::memory_order_relaxed
#define for_each_cam() for (int cam_idx = 0; cam_idx < cam_num; ++cam_idx)

static RelPos calc_ball_pos(double reg_cen_x, int reg_width, int reg_height) noexcept {
    RelPos p;
    p.dir = dir_from_pixel_x(reg_cen_x);
    const double ball_diam_in_pixels = static_cast<double>(reg_width + reg_height) * 0.5;
    if (!check(ball_diam_in_pixels != 0.0)) {
        p.dist = 1.0;
        return p;
    }
    p.dist = ball_diam * cam_focal_len / ball_diam_in_pixels;
    return p;
}

static int read_ball_regs(int cam_idx, RelPos *rels, double *prs,
                          unsigned &last_read_idx) noexcept {
    unsigned ri, seq;
    int ball_num;
    do {
        const auto *b = begin_reading_ball_regs(cam_idx, ri, seq);
        ball_num = b->num.load(order_rlx);
        for (int i = 0; i < ball_num; ++i) {
            rels[i] = calc_ball_pos(b->regs[i].cen_x.load(order_rlx),
                                    b->regs[i].width.load(order_rlx),
                                    b->regs[i].height.load(order_rlx));
            prs[i] = ball_pr_from_stat_dist(b->regs[i].stat_dist.load(order_rlx));
        }
    } while (!likely(finish_reading_ball_regs(cam_idx, ri, seq)));
    last_read_idx = ri;
    return ball_num;
}

static unsigned s_frame_count{0};
static std::condition_variable s_cond_var;
static std::mutex s_cv_mtx;

void wait_for_found_stuff(void) noexcept {
    std::unique_lock<std::mutex> lk(s_cv_mtx);
    const unsigned cnt = s_frame_count;
    // Use `wait_for` instead of `wait` to avoid deadlock at exit.
    s_cond_var.wait_for(lk, 300ms, [cnt] { return s_frame_count != cnt; });
}

static void wake_up_waiters(void) noexcept {
    s_cv_mtx.lock();
    ++s_frame_count;
    s_cv_mtx.unlock();
    s_cond_var.notify_all();
}

void stop_finding(void) noexcept { for_each_cam() stop_cam(cam_idx); }

bool init_finding(char opnt_gate_color_ch) noexcept {
    
    init_gate_info(opnt_gate_color_ch);

    if (!likely(init_cam(front_cam_idx))) {
        log_critical("Front camera doesn't work.");
        return false;
    }

    for_each_cam() {
        if (cam_idx == front_cam_idx) continue;
        if (!likely(init_cam(cam_idx))) {
            log_warning("Back camera (camera number " + tostr(cam_idx) +
                        ") doesn't work.");
        }
    }

    
    return true;
}

void find_stuff(void) noexcept {

    unsigned last_read_idx[cam_num];  // TODO: Use timestamp instead?
    std::thread cam_threads[cam_num];
    for_each_cam() {
        last_read_idx[cam_idx] = 0;
        if (likely(is_cam_inited(cam_idx))) {
            cam_threads[cam_idx] = std::thread(run_cam, cam_idx);
        }
    }

    while (keep_going()) {

        wait_for_frame();

        for_each_cam() {

            if (get_ball_read_idx(cam_idx) == last_read_idx[cam_idx]) continue;

            RelPos rels[max_tracked_ball_num];
            double prs[max_tracked_ball_num];
            const int ball_num = read_ball_regs(cam_idx, rels, prs, last_read_idx[cam_idx]);
            check_ret(ball_num <= max_tracked_ball_num,);
            track_balls(rels, prs, ball_num);

            wake_up_waiters();
        }
    }

    for_each_cam() if (likely(is_cam_inited(cam_idx))) cam_threads[cam_idx].join();
}

void find_and_draw(void) noexcept {
    // TODO
}

// Pausing is surprisingly hard to implement.
void find_and_tune(void) noexcept {
#if 0
    const Capture::Frame *frame = nullptr; // Note that the pointer itself isn't
                                           // constant, only the frame it points to.
    while (keep_going()) {

        // `is_paused()` can change in the middle of a frame, but `paused` can't.
        const bool paused = is_paused();

        if (paused) {
            if (!likely(frame)) {
                if (!likely(capture_and_draw_frame(&frame))) continue;
            } else {
                // Sleeping for 20ms results in about 23ms per loop iteration, 40 in 44.
                std::this_thread::sleep_for(40ms);
            }
        } else {
            if (unlikely(frame)) s_cap.release_frame(frame);
            if (!likely(capture_and_draw_frame(&frame))) continue;
        }

        const Image img = img_from_frame(frame);
        s_vision.process_image(img);
        draw_thresholded_frame(s_vision);
        try_find_and_draw_stuff(s_vision);
        tune_vision(s_vision);
        notify_frame();

        if (!paused) {
            s_cap.release_frame(frame);
            frame = nullptr;
        }
    }

    notify_frame(); // Avoid leaving other threads waiting for next frame.

    // If `paused` was true during the last loop iteration, the frame
    // wasn't released, so release it before exiting.
    if (frame) s_cap.release_frame(frame);
#endif
}
