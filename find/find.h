#pragma once

#include "find/gate_info.h"

struct Gui;
struct RelPos;

// `opnt_gate_color_ch` should be 'b' or 's' if the opponent's gate is blue,
// 'y' or 'k' if it's yellow and any other character if its color is unknown.
bool init_finding(char opnt_gate_color_ch) noexcept;

// These three functions will loop until `keep_going` returns false:
void find_stuff(void) noexcept;
void find_and_draw(void) noexcept;
void find_and_tune(void) noexcept;

void stop_finding(void) noexcept; // Call this only after `keep_going` is false.

void wait_for_found_stuff(void) noexcept; // Sleep until new data comes.
