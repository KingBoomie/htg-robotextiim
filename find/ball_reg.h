#pragma once

#include "cmvision/types.h"

void filter_ball_regs(int cam_idx, double dir_offset, long timestamp_usec,
                      const Region *regs, int reg_num, const uchar *cmap,
                      int cmap_width, int cmap_height) noexcept;

struct AtomicBallRegs {
    struct AtomicRegion {
        std::atomic<double> stat_dist, cen_x, cen_y;
        std::atomic<int> area, width, height;
    };
    AtomicRegion regs[max_tracked_ball_num];
    std::atomic<double> dir_offset;
    std::atomic<long> timestamp; // In microseconds
    std::atomic<int> num;
};

unsigned get_ball_read_idx(int cam_idx) noexcept;

// Reading uses seqlocks internally. That means the data being read
// can change at random to anything at all, so don't do e.g.
// `if (cen_x.load(m_o_relaxed) != 0.0) foo = bar / cen_x;`,
// because that can still divide by zero.

// `read_idx` is an index into an internal buffer. This can be saved and
// compared to `get_ball_read_idx()` to see if there's any new data, as
// long as the reader keeps up and reads all data - indices will wrap.
// `read_seq` is basically just something to be passed to `finish_reading_ball_regs`.
const AtomicBallRegs *begin_reading_ball_regs(int cam_idx, unsigned &read_idx,
                                              unsigned &read_seq) noexcept;

// If this returns false, the pointer `begin_reading_ball_regs` points to
// garbage now and the caller needs to try reading again.
bool finish_reading_ball_regs(int cam_idx, unsigned read_idx, unsigned read_seq) noexcept;
